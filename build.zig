const Builder = @import("std").build.Builder;

pub fn build(b: *std.build.Builder) void {
	const target = b.standardTargetOptions(.{});
	const mode = b.standardReleaseOptions();

	const unicode_lib = b.addStaticLibrary("unicode", null);
	unicode_lib.setTarget(target);
	unicode_lib.setBuildMode(mode);
	unicode_lib.linkLibCpp();
	unicode_lib.addCSourceFiles(&.{
		"src/props.cpp",
	}, &.{
		"-std=c++20",
		"-Wall",
	});
	
	const unicode_lib_tests = b.addExecutable("unicode-tests", null);
	unicode_lib_tests.setTarget(target);
	unicode_lib_tests.setBuildMode(mode);
	unicode_lib_tests.linkLibCpp();
	unicode_lib_tests.linkLibrary(unicode_lib);
	unicode_lib_tests.addCSourceFiles(&.{
		"tests/main.cpp",
		"tests/props.cpp",
	}, &.{
		"-std=c++20",
		"-Wall",
		"-Isrc/",
	});
}