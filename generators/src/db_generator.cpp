#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "httplib/httplib.h"
#include <filesystem>
#include <vector>

#include "downloader.h"
#include "xml.h"
#include "file_generators/flags.h"
#include "file_generators/string_props.h"
#include "file_generators/aliases.h"
#include "file_generators/enums.h"
#include "file_generators/script_extensions.h"
#include "file_generators/num_props.h"
#include "file_generators/blocks.h"

const auto zipFile = (std::filesystem::current_path() / "ucd.all.flat.zip");
const auto xmlFile = (std::filesystem::current_path() / "ucd.all.flat.xml");

int main(int argc, const char** argv) {
    const auto *const usageStr = R"(Usage: ./unicode-generator file_output_path [--regenerate|-r] [--download|-d] [--host <host>|-h <host>] [--uri <uri>|-u <uri>]

Options:
    --regenerate, -r        Regenerate output files. Useful if generator code changes
    --download, -d          Force download of unicode files and regenerates output files. Useful for new unicode versions
    --missing, -m           Do a deep scan for missing files and fill them in
    --host, -h              [String] Host and schema to download zipped Unicode XML files from. Defaults to 'https://www.unicode.org'
    --uri, -u               [String] URI to zipped Unicode XML files on host. Defaults to '/Public/15.0.0/ucdxml/ucd.all.flat.zip'
)";

    if (argc < 2) {
        std::cerr << usageStr;
        return 1;
    }

    const auto fileOutput = std::filesystem::absolute(argv[1]);

    bool regenerate = false;
    bool redownload = false;
    bool missing = false;

    std::string host = "https://www.unicode.org";
    std::string uri = "/Public/15.0.0/ucdxml/ucd.all.flat.zip";

    for (auto i = 2; i < argc; ++i) {
        const auto* str = argv[i];
        if (strcmp("--regenerate", str) == 0 || strcmp("-r", str) == 0) {
            regenerate = true;
        }
        else if (strcmp("--download", str) == 0 || strcmp("-d", str) == 0) {
            redownload = true;
        }
        else if (strcmp("--missing", str) == 0 || strcmp("-m", str) == 0) {
            missing = true;
        }
        else if (strcmp("--host", str) == 0 || strcmp("-h", str) == 0) {
            if (i + 1 >= argc) {
                std::cerr << "Missing host param!\n"
                          << usageStr;
                return 1;
            }
            host = argv[i + 1];
            i += 1;
        }
        else if (strcmp("--uri", str) == 0 || strcmp("-u", str) == 0) {
            if (i + 1 >= argc) {
                std::cerr << "Missing uri param!\n"
                          << usageStr;
                return 1;
            }
            uri = argv[i + 1];
            i += 1;
        }
    }

    if (!std::filesystem::exists(xmlFile) || redownload) {
        if (!std::filesystem::exists(zipFile) || redownload) {
            download_file(host, uri, zipFile);
        }
        unzip(zipFile, std::filesystem::current_path());
    }

    if (!std::filesystem::exists(fileOutput / "db_flags.cpp") || regenerate || redownload || missing) {
        std::cout <<  "Generating unicode data..\n";
        if (!std::filesystem::exists(fileOutput) || !std::filesystem::exists(fileOutput / "strings")) {
            std::filesystem::create_directories(fileOutput / "strings");
        }

        if (!std::filesystem::exists(fileOutput / "enums")) {
            std::filesystem::create_directories(fileOutput / "enums");
        }

        auto data = read_xml(xmlFile);
        generate_db_flag_file(data, fileOutput, !missing || (regenerate || redownload));
        generate_db_string_prop_files(data, fileOutput, !missing || (regenerate || redownload));
        generate_db_alias_file(data, fileOutput, !missing || (regenerate || redownload));
        generate_db_script_extension_file(data, fileOutput, !missing || (regenerate || redownload));
        generate_db_num_prop_files(data, fileOutput, !missing || (regenerate || redownload));
        generate_enums(data, fileOutput, !missing || (regenerate || redownload));
        generate_block_file(data, fileOutput, !missing || (regenerate || redownload));
        std::cout <<  "Finished generating!\n";
    }
}

