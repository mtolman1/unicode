#include "utils.h"
#include <sstream>

auto join(const std::vector<const char*>& vec, const char *join) -> std::string {
    std::stringstream ss;
    bool first = true;
    for (const auto& str : vec) {
        if (!first) {
            ss << join;
        }
        ss << str;
        first = false;
    }
    return ss.str();
}

auto join(const std::vector<std::string>& vec, const char *join) -> std::string {
    std::stringstream ss;
    bool first = true;
    for (const auto& str : vec) {
        if (!first) {
            ss << join;
        }
        ss << str;
        first = false;
    }
    return ss.str();
}

auto upper(std::string s) -> std::string {
    for (char & i : s) {
        i = static_cast<char>(std::toupper(i));
    }
    return s;
}
