#pragma once

#include <filesystem>

extern auto download_file(const std::string& host, const std::string& uri, const std::filesystem::path& downloadLocation) -> void;
extern auto unzip(const std::filesystem::path& zipFile, const std::filesystem::path& extractLocation) -> void;
