#pragma once

#include "../xml.h"

extern auto generate_db_num_prop_files(const XmlData& data, const std::filesystem::path& outFolder, bool overwriteIfExists) -> void;
