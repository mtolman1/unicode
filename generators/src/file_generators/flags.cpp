#include "flags.h"
#include "../utils.h"
#include <iostream>
#include <fstream>

static auto print_flag_fns(std::ostream &out, const char *fnName, const std::vector<const char *> &matching) {
    // Print bool check function
    out << "\nauto unicode::db::ch::is_" << fnName << R"((char32_t ch) -> bool {
    switch(ch){
)";
    for (const auto &c: matching) {
        out << "\tcase 0x" << c << ":\n";
    }
    out << "\t\treturn true;\n\tdefault:\n\t\treturn false;\n\t}\n}";
}

static auto print_lookup_fns(std::ostream &out, const char *fnName, const std::vector<const char *> &matching) {
    // Print list function
    out << "\nauto unicode::db::ch::all_of_" << fnName << R"T(() -> std::set<char32_t> {
        return {)T" << join(prepend_all(matching, "0x"), ",") << "};\n}";
}

auto
generate_db_flag_file(const XmlData &data, const std::filesystem::path &outFolder, bool overwriteIfExists) -> void {
    auto flagsPath = outFolder / "db_flags.cpp";
    auto lookupPath = outFolder / "db_lookup.cpp";
    bool writeFlags = overwriteIfExists || !std::filesystem::exists(flagsPath);
    bool writeLookups = overwriteIfExists || !std::filesystem::exists(lookupPath);
    if (!writeFlags && !writeLookups) {
        std::cout << "db_flags.cpp exists, skipping.\n";
        std::cout << "db_lookup.cpp exists, skipping.\n";
        return;
    }

    std::cout << "Generating db_flags.cpp ...\n";
    std::vector<const char *> deprecated{};
    std::vector<const char *> bidiMirrored{};
    std::vector<const char *> bidiControl{};
    std::vector<const char *> compositionExclusion{};
    std::vector<const char *> fullCompositionExclusion{};
    std::vector<const char *> expandOnNfc{};
    std::vector<const char *> expandOnNfd{};
    std::vector<const char *> expandOnNfkc{};
    std::vector<const char *> expandOnNfkd{};
    std::vector<const char *> joinControl{};
    std::vector<const char *> upper{};
    std::vector<const char *> lower{};
    std::vector<const char *> otherUpper{};
    std::vector<const char *> otherLower{};
    std::vector<const char *> caseIgnorable{};
    std::vector<const char *> cased{};
    std::vector<const char *> changesWhenCaseFolded{};
    std::vector<const char *> changesWhenCaseMapped{};
    std::vector<const char *> changesWhenLowerCased{};
    std::vector<const char *> changesWhenNfkcCaseFolded{};
    std::vector<const char *> changesWhenTitleCased{};
    std::vector<const char *> changesWhenUpperCased{};
    std::vector<const char *> identifierStart{};
    std::vector<const char *> otherIdentifierStart{};
    std::vector<const char *> extendedIdentifierStart{};
    std::vector<const char *> identifierContinue{};
    std::vector<const char *> otherIdentifierContinue{};
    std::vector<const char *> extendedIdentifierContinue{};
    std::vector<const char *> patternSyntax{};
    std::vector<const char *> patternWhiteSpace{};
    std::vector<const char *> dash{};
    std::vector<const char *> hyphen{};
    std::vector<const char *> quotationMark{};
    std::vector<const char *> terminalPunctuation{};
    std::vector<const char *> sentenceTerminal{};
    std::vector<const char *> diacritic{};
    std::vector<const char *> extender{};
    std::vector<const char *> prependedConcatenationMark{};
    std::vector<const char *> softDotted{};
    std::vector<const char *> alphabetic{};
    std::vector<const char *> otherAlphabetic{};
    std::vector<const char *> math{};
    std::vector<const char *> otherMath{};
    std::vector<const char *> hex{};
    std::vector<const char *> asciiHex{};
    std::vector<const char *> defaultIgnorableCodePoint{};
    std::vector<const char *> otherDefaultIgnorableCodePoint{};
    std::vector<const char *> logicalOrderException{};
    std::vector<const char *> whiteSpace{};
    std::vector<const char *> regionalIndicator{};
    std::vector<const char *> graphemeBase{};
    std::vector<const char *> graphemeExtend{};
    std::vector<const char *> otherGraphemeExtend{};
    std::vector<const char *> graphemeLink{};
    std::vector<const char *> ideographic{};
    std::vector<const char *> unifiedIdeograph{};
    std::vector<const char *> idsBinaryOperator{};
    std::vector<const char *> idsTrinaryOperator{};
    std::vector<const char *> radical{};
    std::vector<const char *> variationSelector{};
    std::vector<const char *> nonCharacterCodePoint{};
    std::vector<const char *> emoji{};
    std::vector<const char *> emojiPresentation{};
    std::vector<const char *> emojiModifier{};
    std::vector<const char *> emojiModifierBase{};
    std::vector<const char *> emojiComponent{};
    std::vector<const char *> extendedPictograph{};

    for (const auto &entry: data.charData) {
        const auto *ch = entry.first.c_str();
        if (has_flag_set(entry.second.attrs, "Dep")) { deprecated.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Bidi_M")) {
            bidiMirrored.emplace_back(ch);
        }
        if (has_flag_set(entry.second.attrs, "Bidi_C")) { bidiControl.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "CE")) { compositionExclusion.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Comp_Ex")) { fullCompositionExclusion.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "XO_NFC")) { expandOnNfc.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "XO_NFD")) { expandOnNfd.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "XO_NFKC")) { expandOnNfkc.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "XO_NFKD")) { expandOnNfkd.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Join_C")) { joinControl.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Upper")) { upper.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Lower")) { lower.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "OUpper")) { otherUpper.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "OLower")) { otherLower.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "CI")) { caseIgnorable.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Cased")) { cased.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "CWCF")) { changesWhenCaseFolded.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "CWCM")) { changesWhenCaseMapped.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "CWL")) { changesWhenLowerCased.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "CWKCF")) { changesWhenNfkcCaseFolded.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "CWT")) { changesWhenTitleCased.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "CWU")) { changesWhenUpperCased.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "IDS")) { identifierStart.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "OIDS")) { otherIdentifierStart.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "XIDS")) { extendedIdentifierStart.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "IDC")) { identifierContinue.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "OIDC")) { otherIdentifierContinue.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "XIDC")) { extendedIdentifierContinue.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Pat_Syn")) { patternSyntax.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Pat_WS")) { patternWhiteSpace.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Dash")) { dash.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Hyphen")) { hyphen.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "QMark")) { quotationMark.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Term")) { terminalPunctuation.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "STerm")) { sentenceTerminal.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Dia")) { diacritic.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Ext")) { extender.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "PCM")) { prependedConcatenationMark.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "SD")) { softDotted.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Alpha")) { alphabetic.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "OAlpha")) { otherAlphabetic.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Math")) { math.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "OMath")) { otherMath.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Hex")) { hex.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "AHex")) { asciiHex.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "DI")) { defaultIgnorableCodePoint.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "ODI")) { otherDefaultIgnorableCodePoint.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "LOE")) { logicalOrderException.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "WSpace")) { whiteSpace.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "RI")) { regionalIndicator.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Gr_Base")) { graphemeBase.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Gr_Ext")) { graphemeExtend.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "OGr_Ext")) { otherGraphemeExtend.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Gr_Link")) { graphemeLink.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Ideo")) { ideographic.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "UIdeo")) { unifiedIdeograph.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "IDSB")) { idsBinaryOperator.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "IDST")) { idsTrinaryOperator.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "Radical")) { radical.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "VS")) { variationSelector.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "NChar")) { nonCharacterCodePoint.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "")) { emoji.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "")) { emojiPresentation.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "")) { emojiModifier.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "")) { emojiModifierBase.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "")) { emojiComponent.emplace_back(ch); }
        if (has_flag_set(entry.second.attrs, "")) { extendedPictograph.emplace_back(ch); }
    }

    if (writeFlags) {

        std::ofstream out{flagsPath};
        out << R"(
////////////////////////////////////////////
//  AUTOGENERATED! DO NOT EDIT MANUALLY!  //
////////////////////////////////////////////

#include "db/ch/basic.h"

)";

        print_flag_fns(out, "deprecated", deprecated);
        print_flag_fns(out, "bidi_mirrored", bidiMirrored);
        print_flag_fns(out, "bidi_control", bidiControl);
        print_flag_fns(out, "composition_exclusion", compositionExclusion);
        print_flag_fns(out, "full_composition_exclusion", fullCompositionExclusion);
        print_flag_fns(out, "expand_on_nfc", expandOnNfc);
        print_flag_fns(out, "expand_on_nfd", expandOnNfd);
        print_flag_fns(out, "expand_on_nfkc", expandOnNfkc);
        print_flag_fns(out, "expand_on_nfkd", expandOnNfkd);
        print_flag_fns(out, "join_control", joinControl);
        print_flag_fns(out, "upper", upper);
        print_flag_fns(out, "lower", lower);
        print_flag_fns(out, "other_upper", otherUpper);
        print_flag_fns(out, "other_lower", otherLower);
        print_flag_fns(out, "case_ignorable", caseIgnorable);
        print_flag_fns(out, "cased", cased);
        print_flag_fns(out, "changed_when_case_folded", changesWhenCaseFolded);
        print_flag_fns(out, "changed_when_case_mapped", changesWhenCaseMapped);
        print_flag_fns(out, "changed_when_lower_cased", changesWhenLowerCased);
        print_flag_fns(out, "changed_when_nfkc_case_folded", changesWhenNfkcCaseFolded);
        print_flag_fns(out, "changed_when_title_cased", changesWhenTitleCased);
        print_flag_fns(out, "changed_when_upper_cased", changesWhenUpperCased);
        print_flag_fns(out, "identifier_start", identifierStart);
        print_flag_fns(out, "other_identifier_start", otherIdentifierStart);
        print_flag_fns(out, "extended_identifier_start", extendedIdentifierStart);
        print_flag_fns(out, "identifier_continue", identifierContinue);
        print_flag_fns(out, "other_identifier_continue", otherIdentifierContinue);
        print_flag_fns(out, "extended_identifier_continue", extendedIdentifierContinue);
        print_flag_fns(out, "pattern_syntax", patternSyntax);
        print_flag_fns(out, "pattern_whitespace", patternWhiteSpace);
        print_flag_fns(out, "dash", dash);
        print_flag_fns(out, "hyphen", hyphen);
        print_flag_fns(out, "quotation_mark", quotationMark);
        print_flag_fns(out, "terminal_punctuation", terminalPunctuation);
        print_flag_fns(out, "sentence_terminal", sentenceTerminal);
        print_flag_fns(out, "diacritic", diacritic);
        print_flag_fns(out, "extender", extender);
        print_flag_fns(out, "prepended_concatenation_mark", prependedConcatenationMark);
        print_flag_fns(out, "soft_dotted", softDotted);
        print_flag_fns(out, "alphabetic", alphabetic);
        print_flag_fns(out, "other_alphabetic", otherAlphabetic);
        print_flag_fns(out, "math", math);
        print_flag_fns(out, "other_math", otherMath);
        print_flag_fns(out, "hex", hex);
        print_flag_fns(out, "ascii_hex", asciiHex);
        print_flag_fns(out, "default_ignorable_code_point", defaultIgnorableCodePoint);
        print_flag_fns(out, "other_default_ignorable_code_point", otherDefaultIgnorableCodePoint);
        print_flag_fns(out, "logical_order_exception", logicalOrderException);
        print_flag_fns(out, "white_space", whiteSpace);
        print_flag_fns(out, "regional_indicator", regionalIndicator);
        print_flag_fns(out, "grapheme_base", graphemeBase);
        print_flag_fns(out, "grapheme_extend", graphemeExtend);
        print_flag_fns(out, "other_grapheme_extend", otherGraphemeExtend);
        print_flag_fns(out, "grapheme_link", graphemeLink);
        print_flag_fns(out, "ideographic", ideographic);
        print_flag_fns(out, "unified_ideograph", unifiedIdeograph);
        print_flag_fns(out, "ids_binary_operator", idsBinaryOperator);
        print_flag_fns(out, "ids_trinary_operator", idsTrinaryOperator);
        print_flag_fns(out, "radical", radical);
        print_flag_fns(out, "variation_selector", variationSelector);
        print_flag_fns(out, "non_character_code_point", nonCharacterCodePoint);
        print_flag_fns(out, "emoji", emoji);
        print_flag_fns(out, "emoji_presentation", emojiPresentation);
        print_flag_fns(out, "emoji_modifier", emojiModifier);
        print_flag_fns(out, "emoji_modifier_base", emojiModifierBase);
        print_flag_fns(out, "emoji_component", emojiComponent);
        print_flag_fns(out, "extended_pictograph", extendedPictograph);

        out.close();
        std::cout << "Wrote db_flags.cpp file!\n";
    }
    else {
        std::cout << "db_flags.cpp exists, skipping.\n";
    }

    if (writeLookups) {
        std::cout << "Writing db_lookup.cpp...\n";
        std::ofstream out{lookupPath};
        out << R"(
////////////////////////////////////////////
//  AUTOGENERATED! DO NOT EDIT MANUALLY!  //
////////////////////////////////////////////

#include "db/ch/lookup.h"

)";

        print_lookup_fns(out, "deprecated", deprecated);
        print_lookup_fns(out, "bidi_mirrored", bidiMirrored);
        print_lookup_fns(out, "bidi_control", bidiControl);
        print_lookup_fns(out, "composition_exclusion", compositionExclusion);
        print_lookup_fns(out, "full_composition_exclusion", fullCompositionExclusion);
        print_lookup_fns(out, "expand_on_nfc", expandOnNfc);
        print_lookup_fns(out, "expand_on_nfd", expandOnNfd);
        print_lookup_fns(out, "expand_on_nfkc", expandOnNfkc);
        print_lookup_fns(out, "expand_on_nfkd", expandOnNfkd);
        print_lookup_fns(out, "join_control", joinControl);
        print_lookup_fns(out, "upper", upper);
        print_lookup_fns(out, "lower", lower);
        print_lookup_fns(out, "other_upper", otherUpper);
        print_lookup_fns(out, "other_lower", otherLower);
        print_lookup_fns(out, "case_ignorable", caseIgnorable);
        print_lookup_fns(out, "cased", cased);
        print_lookup_fns(out, "changed_when_case_folded", changesWhenCaseFolded);
        print_lookup_fns(out, "changed_when_case_mapped", changesWhenCaseMapped);
        print_lookup_fns(out, "changed_when_lower_cased", changesWhenLowerCased);
        print_lookup_fns(out, "changed_when_nfkc_case_folded", changesWhenNfkcCaseFolded);
        print_lookup_fns(out, "changed_when_title_cased", changesWhenTitleCased);
        print_lookup_fns(out, "changed_when_upper_cased", changesWhenUpperCased);
        print_lookup_fns(out, "identifier_start", identifierStart);
        print_lookup_fns(out, "other_identifier_start", otherIdentifierStart);
        print_lookup_fns(out, "extended_identifier_start", extendedIdentifierStart);
        print_lookup_fns(out, "identifier_continue", identifierContinue);
        print_lookup_fns(out, "other_identifier_continue", otherIdentifierContinue);
        print_lookup_fns(out, "extended_identifier_continue", extendedIdentifierContinue);
        print_lookup_fns(out, "pattern_syntax", patternSyntax);
        print_lookup_fns(out, "pattern_whitespace", patternWhiteSpace);
        print_lookup_fns(out, "dash", dash);
        print_lookup_fns(out, "hyphen", hyphen);
        print_lookup_fns(out, "quotation_mark", quotationMark);
        print_lookup_fns(out, "terminal_punctuation", terminalPunctuation);
        print_lookup_fns(out, "sentence_terminal", sentenceTerminal);
        print_lookup_fns(out, "diacritic", diacritic);
        print_lookup_fns(out, "extender", extender);
        print_lookup_fns(out, "prepended_concatenation_mark", prependedConcatenationMark);
        print_lookup_fns(out, "soft_dotted", softDotted);
        print_lookup_fns(out, "alphabetic", alphabetic);
        print_lookup_fns(out, "other_alphabetic", otherAlphabetic);
        print_lookup_fns(out, "math", math);
        print_lookup_fns(out, "other_math", otherMath);
        print_lookup_fns(out, "hex", hex);
        print_lookup_fns(out, "ascii_hex", asciiHex);
        print_lookup_fns(out, "default_ignorable_code_point", defaultIgnorableCodePoint);
        print_lookup_fns(out, "other_default_ignorable_code_point", otherDefaultIgnorableCodePoint);
        print_lookup_fns(out, "logical_order_exception", logicalOrderException);
        print_lookup_fns(out, "white_space", whiteSpace);
        print_lookup_fns(out, "regional_indicator", regionalIndicator);
        print_lookup_fns(out, "grapheme_base", graphemeBase);
        print_lookup_fns(out, "grapheme_extend", graphemeExtend);
        print_lookup_fns(out, "other_grapheme_extend", otherGraphemeExtend);
        print_lookup_fns(out, "grapheme_link", graphemeLink);
        print_lookup_fns(out, "ideographic", ideographic);
        print_lookup_fns(out, "unified_ideograph", unifiedIdeograph);
        print_lookup_fns(out, "ids_binary_operator", idsBinaryOperator);
        print_lookup_fns(out, "ids_trinary_operator", idsTrinaryOperator);
        print_lookup_fns(out, "radical", radical);
        print_lookup_fns(out, "variation_selector", variationSelector);
        print_lookup_fns(out, "non_character_code_point", nonCharacterCodePoint);
        print_lookup_fns(out, "emoji", emoji);
        print_lookup_fns(out, "emoji_presentation", emojiPresentation);
        print_lookup_fns(out, "emoji_modifier", emojiModifier);
        print_lookup_fns(out, "emoji_modifier_base", emojiModifierBase);
        print_lookup_fns(out, "emoji_component", emojiComponent);
        print_lookup_fns(out, "extended_pictograph", extendedPictograph);

        out.close();
        std::cout << "Wrote db_lookup.cpp file!\n";
    }
    else {
        std::cout << "db_lookup.cpp exists, skipping.\n";
    }
}
