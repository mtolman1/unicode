#pragma once

#include "../xml.h"

extern auto generate_enums(const XmlData&, const std::filesystem::path&, bool overwriteIfExists = true) -> void;
