#pragma once

#include "../xml.h"

extern auto generate_db_script_extension_file(const XmlData&, const std::filesystem::path&, bool overwriteIfExists) -> void;