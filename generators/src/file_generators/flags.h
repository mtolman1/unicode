#pragma once

#include "../xml.h"

extern auto generate_db_flag_file(const XmlData&, const std::filesystem::path&, bool overwriteIfExists = true) -> void;