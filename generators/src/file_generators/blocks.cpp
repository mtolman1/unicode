#include "blocks.h"

#include <iostream>
#include <fstream>

auto generate_block_file(const XmlData& xml, const std::filesystem::path& outDir, bool overwriteIfExists) -> void {
    auto outPath = outDir / "db_blocks.cpp";

    if (!overwriteIfExists && std::filesystem::exists(outPath)) {
        std::cout << "Skipping " << outPath << ", exists!\n";
        return;
    }

    std::cout << "Writing " << outPath << "...\n";

    auto out = std::ofstream{outPath};

    out << R"(#include "db/blocks.h"

#include <array>

using Block = unicode::db::block::Block;

static constexpr auto defined_blocks = std::array{
)";
    auto blocks = std::vector<XmlNode>{};
    for (const auto& [_, block] : xml.blockData) {
        blocks.emplace_back(block);
    }

    std::sort(blocks.begin(), blocks.end(), [](const XmlNode& left, const XmlNode& right) {
        const auto& leftStr = left.attrs.at("first-cp");
        const auto& rightStr = right.attrs.at("first-cp");
        auto leftLl = strtoll(leftStr.c_str(), nullptr, 16);
        auto rightLl = strtoll(rightStr.c_str(), nullptr, 16);
        return leftLl < rightLl;
    });

    for (const auto& block : blocks) {
        out << "Block{ 0x" << block.attrs.at("first-cp") << ", 0x" << block.attrs.at("last-cp") << ", \""
            << block.attrs.at("name") << "\"},\n";
    }
    out << R"(};

static constexpr auto blocks_sorted() noexcept -> bool {
    int64_t lastStart = -1;
    int64_t lastEnd = -1;
    for(const auto& block : defined_blocks) {
        if (lastStart >= block.firstCodePoint || lastEnd >= block.lastCodePoint) {
            return false;
        }
        lastStart = block.firstCodePoint;
        lastEnd = block.lastCodePoint;
    }
    return true;
}

static constexpr auto blocks_are_ranges() noexcept -> bool {
    for(const auto& block : defined_blocks) {
        if (block.firstCodePoint > block.lastCodePoint) {
            return false;
        }
    }
    return true;
}

static_assert(blocks_sorted(), "Block data is not sorted properly, it must be in ascending range order");
static_assert(blocks_are_ranges(), "Block data is not made of ranges; all lastCodePoint's must be after the firstCodePoint");

static constexpr auto search_char_blocks(char32_t ch) noexcept -> std::optional<Block> {
    auto res = std::lower_bound(defined_blocks.begin(), defined_blocks.end(), ch, [](const Block& blk, char32_t ch) {
        return blk.lastCodePoint < ch;
    });
    if (res == defined_blocks.end()) {
        return std::nullopt;
    }
    return *res;
}

static constexpr auto str_equals_const(const char* left, const char* right) noexcept -> bool {
    if (!left) {
        return !right;
    }
    if (!right) {
        return false;
    }
    size_t leftIndex = 0, rightIndex = 0;
    for(; left[leftIndex] && right[rightIndex] && left[leftIndex] == right[rightIndex]; ++leftIndex, ++rightIndex) {}
    return leftIndex == rightIndex && left[leftIndex] == right[rightIndex];
}

static_assert(str_equals_const(search_char_blocks(U'\u0000')->name, "Basic Latin"));
static_assert(str_equals_const(search_char_blocks(U'\u007F')->name, "Basic Latin"));
static_assert(str_equals_const(search_char_blocks(U'\u004F')->name, "Basic Latin"));
static_assert(str_equals_const(search_char_blocks(U'\u0080')->name, "Latin-1 Supplement"));
static_assert(str_equals_const(search_char_blocks(U'\u17FE')->name, "Khmer"));
static_assert(str_equals_const(search_char_blocks(U'\U000119A0')->name, "Nandinagari"));
static_assert(str_equals_const(search_char_blocks(U'\U0010FFFF')->name, "Supplementary Private Use Area-B"));
static_assert(!search_char_blocks(0x0011FFFF));

auto unicode::db::block::blocks() -> std::vector<Block> { return {defined_blocks.begin(), defined_blocks.end()}; }

auto unicode::db::block::block_for(char32_t ch) noexcept -> std::optional<Block> { return search_char_blocks(ch); }

auto unicode::db::block::block_by_id(size_t blockId) noexcept -> std::optional<Block> {
    return blockId < defined_blocks.size() ? std::optional{defined_blocks.at(blockId)} : std::nullopt;
}
)";

    std::cout << "Wrote " << outPath << "!\n";
}