#include "string_props.h"
#include <iostream>
#include <set>
#include <fstream>
#include <array>
#include <thread>
#include <sstream>

static auto padded(const std::string& in) -> std::string {
    std::string out{};
    out.reserve(8);
    while (out.size() + in.size() < 8) {
        out.push_back('0');
    }
    out += in;
    return out;
}

static auto split_and_pad(const std::string& in) -> std::string {
    std::stringstream ss;

    std::istringstream iss(in);
    std::string cp;
    while ( getline( iss, cp, ' ' ) ) {
        ss << "\\U" << padded(cp);
    }

    return ss.str();
}

static auto save_string_fns(const std::filesystem::path &outFolder,
                            const std::map<std::string, std::map<std::string, std::vector<std::string>>> &props,
                            bool overwriteIfExists) {
    std::map<std::string, std::string> fnOverride{
            {"na",  "name"},
            {"na1", "legacy_name"},
            {"nv",  "numeric_value"},
            {"JSN", "jamo_short_name"},
            {"isc", "iso10646_comment"},
    };

    std::set<std::string> unicodeStr{
            "kHangul",
            "kHDZRadBreak",
            "kHanyuPinlu",
            "kHanyuPinyin",
            "kMandarin",
            "kTang",
            "kVietnamese",
            "kXHC1983",
            "kTGHZ2013",
    };

    std::map<std::string, std::string> includeMap{
            {"age",                         "meta"},
            {"na",                          "meta"},
            {"na1",                         "metax"},
            {"isc",                         "metax"},
            {"JSN",                         "misc"},
            {"kAccountingNumeric",          "unihan"},
            {"kAlternateHanYu",             "unihan"},
            {"kAlternateJEF",               "unihan"},
            {"kAlternateKangXi",             "unihan"},
            {"kAlternateMorohashi",         "unihan"},
            {"kAlternateTotalStrokes",      "unihan"},
            {"kBigFive",                    "unihan"},
            {"kCCCII",                      "unihan"},
            {"kCNS1986",                    "unihan"},
            {"kCNS1992",                    "unihan"},
            {"kCangjie",                     "unihan"},
            {"kCantonese",                  "unihan"},
            {"kCheungBauer",                "unihan"},
            {"kCheungBauerIndex",           "unihan"},
            {"kCihaiT",                     "unihan"},
            {"kCompatibilityVariant",       "unihan"},
            {"kCowles",                     "unihan"},
            {"kDaeJaweon",                  "unihan"},
            {"kDefinition",                 "unihan"},
            {"kEACC",                       "unihan"},
            {"kFenn",                       "unihan"},
            {"kFennIndex",                  "unihan"},
            {"kFourCornerCode",             "unihan"},
            {"kFrequency",                  "unihan"},
            {"kGB0",                        "unihan"},
            {"kGB1",                        "unihan"},
            {"kGB3",                        "unihan"},
            {"kGB5",                        "unihan"},
            {"kGB7",                        "unihan"},
            {"kGB8",                        "unihan"},
            {"kGradeLevel",                 "unihan"},
            {"kGSR",                        "unihan"},
            {"kHangul",                     "unihan"},
            {"kHanYu",                      "unihan"},
            {"kHanyuPinlu",                 "unihan"},
            {"kHanyuPinyin",                "unihan"},
            {"kHDZRadBreak",                "unihan"},
            {"kHKGlyph",                    "unihan"},
            {"kHKSCS",                      "unihan"},
            {"kIBMJapan",                   "unihan"},
            {"kIICore",                     "unihan"},
            {"kIRGDaeJaweon",               "unihan"},
            {"kIRGDaiKanwaZiten",           "unihan"},
            {"kIRGHanyuDaZidian",           "unihan"},
            {"kIRGKangXi",                  "unihan"},
            {"kIRG_GSource",                "unihan"},
            {"kIRG_HSource",                "unihan"},
            {"kIRG_JSource",                "unihan"},
            {"kIRG_KPSource",               "unihan"},
            {"kIRG_KSource",                "unihan"},
            {"kIRG_MSource",                "unihan"},
            {"kIRG_SSource",                "unihan"},
            {"kIRG_TSource",                "unihan"},
            {"kIRG_USource",                "unihan"},
            {"kIRG_UKSource",               "unihan"},
            {"kIRG_VSource",                "unihan"},
            {"kJa",                         "unihan"},
            {"kJHJ",                        "unihan"},
            {"kJinmeiyoKanji",              "unihan"},
            {"kJoyoKanji",                  "unihan"},
            {"kKoreanEducationHanja",       "unihan"},
            {"kKoreanName",                 "unihan"},
            {"kTGH",                        "unihan"},
            {"kJIS0213",                    "unihan"},
            {"kJapaneseKun",                "unihan"},
            {"kJapaneseOn",                 "unihan"},
            {"kJis0",                       "unihan"},
            {"kJis1",                       "unihan"},
            {"kKPS0",                       "unihan"},
            {"kKPS1",                       "unihan"},
            {"kKSC0",                       "unihan"},
            {"kKangXi",                     "unihan"},
            {"kKarlgren",                   "unihan"},
            {"kKorean",                     "unihan"},
            {"kLau",                        "unihan"},
            {"kMainlandTelegraph",          "unihan"},
            {"kMandarin",                   "unihan"},
            {"kMatthews",                   "unihan"},
            {"kMeyerWempe",                 "unihan"},
            {"kMorohashi",                  "unihan"},
            {"kNelson",                     "unihan"},
            {"kOtherNumeric",               "unihan"},
            {"kPhonetic",                   "unihan"},
            {"kPrimaryNumeric",             "unihan"},
            {"kPseudoGB1",                  "unihan"},
            {"kRSAdobe_Japan1_6",           "unihan"},
            {"kRSJapanese",                 "unihan"},
            {"kRSKanWa",                    "unihan"},
            {"kRSKangXi",                   "unihan"},
            {"kRSKorean",                   "unihan"},
            {"kRSMerged",                   "unihan"},
            {"kRSUnicode",                  "unihan"},
            {"kSBGY",                       "unihan"},
            {"kSemanticVariant",            "unihan"},
            {"kSimplifiedVariant",          "unihan"},
            {"kSpecializedSemanticVariant", "unihan"},
            {"kSpoofingVariant",            "unihan"},
            {"kTaiwanTelegraph",            "unihan"},
            {"kTang",                       "unihan"},
            {"kTGHZ2013",                   "unihan"},
            {"kTotalStrokes",               "unihan"},
            {"kTraditionalVariant",         "unihan"},
            {"kUnihanCore2020",             "unihan"},
            {"kVietnamese",                 "unihan"},
            {"kXHC1983",                    "unihan"},
            {"kXerox",                      "unihan"},
            {"kZVariant",                   "unihan"},
            {"kStrange",                    "unihan"},
            {"kRSTUnicode",                 "tangut"},
            {"kTGT_MergedSrc",              "tangut"},
            {"kSrc_NushuDuben",             "nushu"},
            {"kReading",                    "nushu"},
            {"nv",                          "numeric"},
    };
    std::set<std::string> charByStr{"na", "na1"};
    std::set<std::string> charsByStr{"nv"};
    std::set<std::string> charsByDouble{"nv"};
    std::map<std::string, std::string> emptyVals = {{"nv",                     "NaN"},
                                                    {"kAlternateTotalStrokes", "-"}};

    std::set<std::string> missing{};

    for (const auto &prop: props) {
        // Print bool check function
        auto fnName = fnOverride.contains(prop.first) ? fnOverride.at(prop.first) : prop.first;
        auto fileName = "db_" + fnName + ".cpp";

        if (!includeMap.contains(prop.first)) {
            missing.insert(prop.first);
        }

        auto outputPath = outFolder / "strings" / fileName;
        if (!overwriteIfExists && std::filesystem::exists(outputPath)) {
            std::cout << "strings/" << fileName << " exists, skipping.\n";
            continue;
        }
        std::cout << "Writing strings/" << fileName << " ...\n";

        std::ofstream out{outputPath};
        out << R"(
////////////////////////////////////////////
//  AUTOGENERATED! DO NOT EDIT MANUALLY!  //
////////////////////////////////////////////
#include "db/ch/)" << includeMap[prop.first] << R"(.h"
)";

        if (unicodeStr.contains(prop.first)) {
            out << "\nauto unicode::db::ch::" << fnName << R"((char32_t ch) -> const char8_t* {
)";
        }
        else {
            out << "\nauto unicode::db::ch::" << fnName << R"((char32_t ch) -> const char* {
)";
        }
        auto empty = emptyVals.contains(prop.first) ? emptyVals[prop.first] : "";
        out << "switch (ch) {\n";
        for (const auto &pair: prop.second) {
            if (pair.first == empty) {
                continue;
            }
            for (const auto &ch: pair.second) {
                out << "\tcase 0x" << ch << ":\n";
            }
            if (unicodeStr.contains(prop.first)) {
                out << "\t\treturn u8\"" << pair.first << "\";\n";
            }
            else {
                out << "\t\treturn \"" << pair.first << "\";\n";
            }
        }
        out << "\tdefault:\n\t\treturn nullptr;\n\t}\n}";

        if (charByStr.contains(prop.first)) {
            out << "\nauto unicode::db::ch::char_by_" << fnName
                << "(const std::string_view& sv) -> std::optional<char32_t> {\n";
            for (const auto &pair: prop.second) {
                if (pair.first.empty() || pair.first == empty) {
                    continue;
                }
                for (const auto &ch: pair.second) {
                    out << "\tif (sv == \"" << pair.first << "\") return 0x" << ch << ";\n";
                    break;
                }
            }
            out << "\treturn std::nullopt;\n}";
        }

        if (charsByStr.contains(prop.first)) {
            out << "\n#include \"encoding/convert.h\"\n";

            out << "\n\nauto unicode::db::ch::chars_by_" << fnName
                << "(const std::string_view& sv) -> std::set<char32_t> {\n";
            for (const auto &pair: prop.second) {
                if (pair.first.empty() || pair.first == empty) {
                    continue;
                }
                out << "\tif (sv == \"" << pair.first << "\") {\n\t\treturn {";
                bool first = true;
                for (const auto &ch: pair.second) {
                    if (!first) {
                        out << ", ";
                    }
                    first = false;
                    out << "0x" << ch;
                }
                out << "};\n\t}\n";
            }
            out << "\treturn {};\n}";

            auto fnPrefixes = std::array{"u8", "u16", "u32"};
            auto convFuncs = std::array{"utf8_to_str", "utf16_to_str", "utf32_to_str"};
            for (size_t i = 0; i < fnPrefixes.size(); ++i) {
                auto fnPrefix = fnPrefixes[i];
                auto convFunc = convFuncs[i];

                out << "\n\nauto unicode::db::ch::chars_by_" << fnName
                    << "(const std::" << fnPrefix
                    << "string_view& sv) -> std::set<char32_t> {\n\tauto str = unicode::encoding::convert::"
                    << convFunc << "(sv);\n\tauto res = chars_by_" << fnName << "(str);\n\treturn res;\n}";
            }
        }

        if (charsByDouble.contains(prop.first)) {
            out << "\n\nauto unicode::db::ch::chars_by_" << fnName
                << "(double val) -> std::set<char32_t> {\n";
            for (const auto &pair: prop.second) {
                if (pair.first.empty() || pair.first == empty) {
                    continue;
                }
                out << "if (val + 0.000001 <= " << pair.first << " || val - 0.000001 >= " << pair.first
                    << ") {\n\t\treturn {";
                bool first = true;
                for (const auto &ch: pair.second) {
                    if (!first) {
                        out << ", ";
                    }
                    first = false;
                    out << "0x" << ch;
                }
                out << "};\n\t}";
            }
            out << "\treturn {};\n}";
        }

        std::cout << "Wrote " << fileName << "!\n";

        out.close();
    }

    if (!missing.empty()) {
        std::cerr << "Missing the following save_string_fns:\n";
        for (const auto& m : missing) {
            std::cerr << "\t" << m << "\n";
        }
    }
}

static auto save_string_map_fns(const std::filesystem::path &outFolder,
                                const std::map<std::string, std::map<std::string, std::vector<std::string>>> &props,
                                bool overwriteIfExists) {
    std::map<std::string, std::string> fnOverride{
            {"dm",      "decomposition_mapping"},
            {"FC_NFKC", "fc_nfkc_closure"},
            {"uc",      "upper_case"},
            {"lc",      "lower_case"},
            {"tc",      "title_case"},
            {"cf",      "case_folding"},
            {"NFKC_CF", "nfkc_case_fold"},
    };
    std::map<std::string, std::string> includeMap{
            {"dm", "normalize"},
            {"FC_NFKC", "normalize"},
            {"uc", "casing"},
            {"lc", "casing"},
            {"tc", "casing"},
            {"cf", "casing"},
            {"NFKC_CF", "normalize"},
    };

    std::set<std::string> missing{};

    for (const auto &prop: props) {
        // Print bool check function
        auto fnName = fnOverride.contains(prop.first) ? fnOverride.at(prop.first) : prop.first;
        auto fileName = "db_" + fnName + ".cpp";
        std::cout << "Writing " << fileName << " ...\n";

        if (!includeMap.contains(prop.first)) {
            missing.insert(prop.first);
        }

        auto outputPath = outFolder / "strings" / fileName;
        if (!overwriteIfExists && std::filesystem::exists(outputPath)) {
            std::cout << "strings/" << fileName << " exists, skipping.\n";
            continue;
        }

        std::ofstream out{outputPath};
        out << R"(
////////////////////////////////////////////
//  AUTOGENERATED! DO NOT EDIT MANUALLY!  //
////////////////////////////////////////////
#include "db/ch/)" << includeMap[prop.first] << R"(.h"

)";

        out << "\nauto unicode::db::ch::" << fnName << R"((char32_t ch) -> const char32_t* {
)";
        out << "switch (ch) {\n";
        for (const auto &pair: prop.second) {
            if (pair.first == "#") {
                for (const auto &ch: pair.second) {
                    out << "\tcase 0x" << ch << ":\n\t\treturn U\"" << split_and_pad(ch) << "\";\n";
                }
            } else if (!pair.first.empty()) {
                for (const auto &ch: pair.second) {
                    out << "\tcase 0x" << ch << ":\n";
                }
                out << "\t\treturn U\"" << split_and_pad(pair.first) << "\";\n";
            }
        }
        out << "\tdefault:\n\t\treturn nullptr;\n\t}\n}";

        std::cout << "Wrote " << fileName << "!\n";

        out.close();
    }

    if (!missing.empty()) {
        std::cerr << "Missing the following save_string_map_fns:\n";
        for (const auto& m : missing) {
            std::cerr << "\t" << m << "\n";
        }
    }
}

static auto save_char_map_fns(const std::filesystem::path &outFolder,
                              const std::map<std::string, std::map<std::string, std::vector<std::string>>> &props,
                              bool overwriteIfExists) {
    std::map<std::string, std::string> fnOverride{
            {"bpb", "bidi_paired_bracket"},
            {"suc", "simple_upper_case"},
            {"slc", "simple_lower_case"},
            {"stc", "simple_title_case"},
            {"scf", "simple_case_folding"},
    };
    std::map<std::string, std::string> includeMap{
            {"bpb", "bidi"},
            {"suc", "casing"},
            {"slc", "casing"},
            {"stc", "casing"},
            {"scf", "casing"},
    };

    std::set<std::string> missing{};

    for (const auto &prop: props) {
        // Print bool check function
        auto fnName = fnOverride.contains(prop.first) ? fnOverride.at(prop.first) : prop.first;
        auto fileName = "db_" + fnName + ".cpp";
        std::cout << "Writing " << fileName << " ...\n";

        if (!includeMap.contains(prop.first)) {
            missing.insert(prop.first);
        }

        auto outputPath = outFolder / "strings" / fileName;
        if (!overwriteIfExists && std::filesystem::exists(outputPath)) {
            std::cout << "strings/" << fileName << " exists, skipping.\n";
            continue;
        }

        std::ofstream out{outputPath};
        out << R"(
////////////////////////////////////////////
//  AUTOGENERATED! DO NOT EDIT MANUALLY!  //
////////////////////////////////////////////
#include "db/ch/)" << includeMap[prop.first] << R"(.h"

)";

        out << "\nauto unicode::db::ch::" << fnName << R"((char32_t ch) -> char32_t {
)";
        out << "switch (ch) {\n";
        for (const auto &pair: prop.second) {
            if (pair.first == "#") {
                continue;
            } else {
                for (const auto &ch: pair.second) {
                    out << "\tcase 0x" << ch << ":\n";
                }
                out << "\t\treturn 0x" << pair.first << ";\n";
            }
        }
        out << "\tdefault:\n\t\treturn ch;\n\t}\n}";

        std::cout << "Wrote " << fileName << "!\n";

        out.close();
    }

    if (!missing.empty()) {
        std::cerr << "Missing the following save_char_map_fns:\n";
        for (const auto& m : missing) {
            std::cerr << "\t" << m << "\n";
        }
    }
}

static auto save_char_fns(const std::filesystem::path &outFolder,
                          const std::map<std::string, std::map<std::string, std::vector<std::string>>> &props,
                          bool overwriteIfExists) {
    std::map<std::string, std::string> includeMap{
            {"bmg", "bidi"},
            {"EqUIdeo", "misc"},
    };

    std::set<std::string> missing{};
    auto fnOverride = std::map<std::string, std::string, std::less<>>{
            {"bmg",     "bidi_mirrored_glyph"},
            {"EqUIdeo", "equivalent_unified_ideograph"},
    };
    for (const auto &prop: props) {
        // Print bool check function
        auto fnName = fnOverride.contains(prop.first) ? fnOverride.at(prop.first) : prop.first;
        auto fileName = "db_" + fnName + ".cpp";
        auto path = outFolder / "strings" / fileName;

        if (!includeMap.contains(prop.first)) {
            missing.insert(prop.first);
        }

        if (!overwriteIfExists && std::filesystem::exists(path)) {
            std::cout << "Skipping " << path << ", file exists!\n";
            continue;
        }
        std::cout << "Writing " << path << " ...\n";
        auto out = std::ofstream{path};

        out << R"(
////////////////////////////////////////////
//  AUTOGENERATED! DO NOT EDIT MANUALLY!  //
////////////////////////////////////////////
#include "db/ch/)" << includeMap[prop.first] << R"(.h"

)";
        out << "\nauto unicode::db::ch::" << fnName << R"((char32_t ch) -> char32_t {
)";
        out << "switch (ch) {\n";
        for (const auto &pair: prop.second) {
            if (pair.first.empty()) {
                continue;
            }
            for (const auto &ch: pair.second) {
                out << "\tcase 0x" << ch << ":\n";
            }
            out << "\t\treturn 0x" << pair.first << ";\n";
        }
        out << "\tdefault:\n\t\treturn 0;\n\t}\n}";

        std::cout << "Wrote " << fileName << "!\n";

        out.close();
    }
}

static auto save_char_fns_empty(const std::filesystem::path &outFolder,
                                const std::map<std::string, std::map<std::string, std::vector<std::string>>> &props,
                                bool overwriteIfExists) {
    auto fnOverride = std::map<std::string, std::string, std::less<>>{
            {"bc", "bidi_class"},
    };
    std::map<std::string, std::string> includeMap{
            {"bmg", "bidi"},
            {"EqUIdeo", "misc"},
    };

    std::set<std::string> missing{};
    for (const auto &prop: props) {
        // Print bool check function
        auto fnName = fnOverride.contains(prop.first) ? fnOverride.at(prop.first) : prop.first;
        auto fileName = "db_" + fnName + ".cpp";
        auto path = outFolder / "strings" / fileName;

        if (!includeMap.contains(prop.first)) {
            missing.insert(prop.first);
        }
        if (!overwriteIfExists && std::filesystem::exists(path)) {
            std::cout << "Skipping " << path << ", file exists!\n";
            continue;
        }
        std::cout << "Writing " << path << " ...\n";
        auto out = std::ofstream{path};

        out << R"(
////////////////////////////////////////////
//  AUTOGENERATED! DO NOT EDIT MANUALLY!  //
////////////////////////////////////////////
#include "db/ch/)" << includeMap[prop.first] << R"(.h"

)";
        out << "\nauto unicode::db::ch::" << fnName << R"((char32_t ch) -> char32_t { return 0; })";

        std::cout << "Wrote " << fileName << "!\n";

        out.close();
    }
}

auto generate_db_string_prop_files(const XmlData &data, const std::filesystem::path &outFolder,
                                   bool overwriteIfExists) -> void {
    std::cout << "Extracting string attributes ...\n";
    std::map<std::string, std::map<std::string, std::vector<std::string>>> stringProps{
            {"age",                         {}},
            {"na",                          {}},
            {"na1",                         {}},
            {"isc",                         {}},
            {"JSN",                         {}},
            {"kAccountingNumeric",          {}},
            {"kAlternateHanYu",             {}},
            {"kAlternateJEF",               {}},
            {"kAlternateKangXi",             {}},
            {"kAlternateMorohashi",         {}},
            {"kAlternateTotalStrokes",      {}},
            {"kBigFive",                    {}},
            {"kCCCII",                      {}},
            {"kCNS1986",                    {}},
            {"kCNS1992",                    {}},
            {"kCangjie",                     {}},
            {"kCantonese",                  {}},
            {"kCheungBauer",                {}},
            {"kCheungBauerIndex",           {}},
            {"kCihaiT",                     {}},
            {"kCompatibilityVariant",       {}},
            {"kCowles",                     {}},
            {"kDaeJaweon",                  {}},
            {"kDefinition",                 {}},
            {"kEACC",                       {}},
            {"kFenn",                       {}},
            {"kFennIndex",                  {}},
            {"kFourCornerCode",             {}},
            {"kFrequency",                  {}},
            {"kGB0",                        {}},
            {"kGB1",                        {}},
            {"kGB3",                        {}},
            {"kGB5",                        {}},
            {"kGB7",                        {}},
            {"kGB8",                        {}},
            {"kGradeLevel",                 {}},
            {"kGSR",                        {}},
            {"kHangul",                     {}},
            {"kHanYu",                      {}},
            {"kHanyuPinlu",                 {}},
            {"kHanyuPinyin",                {}},
            {"kHDZRadBreak",                {}},
            {"kHKGlyph",                    {}},
            {"kHKSCS",                      {}},
            {"kIBMJapan",                   {}},
            {"kIICore",                     {}},
            {"kIRGDaeJaweon",               {}},
            {"kIRGDaiKanwaZiten",           {}},
            {"kIRGHanyuDaZidian",           {}},
            {"kIRGKangXi",                  {}},
            {"kIRG_GSource",                {}},
            {"kIRG_HSource",                {}},
            {"kIRG_JSource",                {}},
            {"kIRG_KPSource",               {}},
            {"kIRG_KSource",                {}},
            {"kIRG_MSource",                {}},
            {"kIRG_SSource",                {}},
            {"kIRG_TSource",                {}},
            {"kIRG_USource",                {}},
            {"kIRG_UKSource",               {}},
            {"kIRG_VSource",                {}},
            {"kJa",                         {}},
            {"kJHJ",                        {}},
            {"kJinmeiyoKanji",              {}},
            {"kJoyoKanji",                  {}},
            {"kKoreanEducationHanja",       {}},
            {"kKoreanName",                 {}},
            {"kTGH",                        {}},
            {"kJIS0213",                    {}},
            {"kJapaneseKun",                {}},
            {"kJapaneseOn",                 {}},
            {"kJis0",                       {}},
            {"kJis1",                       {}},
            {"kKPS0",                       {}},
            {"kKPS1",                       {}},
            {"kKSC0",                       {}},
            {"kKangXi",                     {}},
            {"kKarlgren",                   {}},
            {"kKorean",                     {}},
            {"kLau",                        {}},
            {"kMainlandTelegraph",          {}},
            {"kMandarin",                   {}},
            {"kMatthews",                   {}},
            {"kMeyerWempe",                 {}},
            {"kMorohashi",                  {}},
            {"kNelson",                     {}},
            {"kOtherNumeric",               {}},
            {"kPhonetic",                   {}},
            {"kPrimaryNumeric",             {}},
            {"kPseudoGB1",                  {}},
            {"kRSAdobe_Japan1_6",           {}},
            {"kRSJapanese",                 {}},
            {"kRSKanWa",                    {}},
            {"kRSKangXi",                   {}},
            {"kRSKorean",                   {}},
            {"kRSMerged",                   {}},
            {"kRSUnicode",                  {}},
            {"kSBGY",                       {}},
            {"kSemanticVariant",            {}},
            {"kSimplifiedVariant",          {}},
            {"kSpecializedSemanticVariant", {}},
            {"kSpoofingVariant",            {}},
            {"kTaiwanTelegraph",            {}},
            {"kTang",                       {}},
            {"kTGHZ2013",                   {}},
            {"kTotalStrokes",               {}},
            {"kTraditionalVariant",         {}},
            {"kUnihanCore2020",             {}},
            {"kVietnamese",                 {}},
            {"kXHC1983",                    {}},
            {"kXerox",                      {}},
            {"kZVariant",                   {}},
            {"kStrange",                    {}},
            {"kRSTUnicode",                 {}},
            {"kTGT_MergedSrc",              {}},
            {"kSrc_NushuDuben",             {}},
            {"kReading",                    {}},
            {"nv",                          {}},
    };

    std::map<std::string, std::map<std::string, std::vector<std::string>>> charProps{
            {"bmg",     {}},
            {"EqUIdeo", {}},
    };

    std::map<std::string, std::map<std::string, std::vector<std::string>>> strMappingProps{
            {"dm",      {}},
            {"FC_NFKC", {}},
            {"uc",      {}},
            {"lc",      {}},
            {"tc",      {}},
            {"cf",      {}},
            {"NFKC_CF", {}},
    };

    std::map<std::string, std::map<std::string, std::vector<std::string>>> charMappingProps{
            {"bpb", {}},
            {"suc", {}},
            {"slc", {}},
            {"stc", {}},
            {"scf", {}},
    };

    for (const auto &entry: data.charData) {
        for (auto &prop: stringProps) {
            if (entry.second.attrs.contains(prop.first)) {
                prop.second[entry.second.attrs.at(prop.first)].emplace_back(entry.first);
            }
        }
        for (auto &prop: charProps) {
            if (entry.second.attrs.contains(prop.first)) {
                prop.second[entry.second.attrs.at(prop.first)].emplace_back(entry.first);
            }
        }
        for (auto &prop: strMappingProps) {
            if (entry.second.attrs.contains(prop.first)) {
                prop.second[entry.second.attrs.at(prop.first)].emplace_back(entry.first);
            }
        }

        for (auto &prop: charMappingProps) {
            if (entry.second.attrs.contains(prop.first)) {
                prop.second[entry.second.attrs.at(prop.first)].emplace_back(entry.first);
            }
        }
    }

    save_string_fns(outFolder, stringProps, overwriteIfExists);
    save_string_map_fns(outFolder, strMappingProps, overwriteIfExists);
    save_char_map_fns(outFolder, charMappingProps, overwriteIfExists);
    save_char_fns(outFolder, charProps, overwriteIfExists);
}
