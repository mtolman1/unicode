#pragma once

#include "../xml.h"

extern auto generate_block_file(const XmlData&, const std::filesystem::path&, bool overwriteIfExists) -> void;
