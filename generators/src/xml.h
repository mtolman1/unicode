#pragma once

#include <map>
#include <vector>
#include "pugixml.hpp"
#include <filesystem>

using XmlAttributes = std::map<std::string, std::string, std::less<>>;
using XmlChildren = std::vector<XmlAttributes>;
struct XmlNode {
    XmlAttributes attrs;
    XmlChildren children;
};
using XmlMap = std::map<std::string, XmlNode, std::less<>>;

struct XmlData {
    XmlMap charData;
    XmlMap blockData;
};

extern auto has_flag_set(const XmlAttributes& attrs, const char* flag) -> bool;

extern auto read_xml(const std::filesystem::path& xmlFile) -> XmlData;
