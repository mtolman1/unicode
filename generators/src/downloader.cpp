#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "httplib/httplib.h"
#include "11Zip/include/elzip/elzip.hpp"
#include "downloader.h"
#include <iostream>

auto download_file(const std::string& host, const std::string& uri, const std::filesystem::path& downloadLocation) -> void {
    httplib::Client client(host.c_str());

    std::cout << "Downloading \"" << host << uri << "\" ...";
    if (auto res = client.Get(uri.c_str())) {
        if (res->status == 200) {
            std::cout << "Saving file to " << downloadLocation << " ...\n";
            std::ofstream out{downloadLocation};
            out << res->body;
            out.close();
            std::cout << "File saved.\n";
        }
    } else {
        auto err = res.error();
        std::cout << "Download failed. HTTP error: " << httplib::to_string(err) << std::endl;
        throw err;
    }
}
auto unzip(const std::filesystem::path& zipFile, const std::filesystem::path& extractLocation) -> void {
    std::cout << "Extracting zip file...\n";
    elz::extractZip(zipFile, extractLocation.string());
    std::cout << "Extracted zip file.\n";
}