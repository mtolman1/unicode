#pragma once

#include <vector>
#include <string>

template<typename T>
auto prepend_all(const std::vector<T>& vec, const std::string& prepend) -> std::vector<std::string> {
    std::vector<std::string> res;
    res.reserve(vec.size());
    for (const auto& elem : vec) {
        res.emplace_back(prepend + elem);
    }
    return res;
}

extern auto join(const std::vector<const char*>& vec, const char *join) -> std::string;

extern auto join(const std::vector<std::string>& vec, const char *join) -> std::string;

extern auto upper(std::string s) -> std::string;
