#include "xml.h"
#include <iostream>
#include <memory>

auto read_xml(const std::filesystem::path &xmlFile) -> XmlData {
    std::cout << "Reading xml file...\n";
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_file(xmlFile.c_str());
    if (!result) {
        std::cerr << "Could not read xml file";
        throw 1;
    }

    std::cout << "Extracting character data...\n";
    auto repertoire = doc.child("ucd").child("repertoire");
    auto charData = XmlMap{};
    for (const auto &elem: repertoire) {
        auto attrs = XmlAttributes{};
        for (auto attr: elem.attributes()) {
            attrs[attr.name()] = attr.value();
        }

        if (attrs.contains("cp")) {
            auto aliases = XmlChildren{};
            for (auto childElem: elem.children()) {
                auto childAttrs = XmlAttributes{};
                for (auto childAttr: childElem.attributes()) {
                    childAttrs[childAttr.name()] = childAttr.value();
                }
                aliases.emplace_back(std::move(childAttrs));
            }
            std::string cp = attrs["cp"];
            charData[cp] = {std::move(attrs), std::move(aliases)};
        }
    }

    std::cout << "Extracting block data...\n";
    auto blocks = doc.child("ucd").child("blocks");
    auto blockData = XmlMap{};
    for (auto elem: blocks.children()) {
        auto attrs = XmlAttributes{};
        for (auto attr: elem.attributes()) {
            attrs[attr.name()] = attr.value();
        }

        if (attrs.contains("name")) {
            auto aliases = XmlChildren{};
            std::string name = attrs["name"];
            blockData[name] = {std::move(attrs), XmlChildren{}};
        }
    }

    return {charData, blockData};
}

auto has_flag_set(const XmlAttributes &attrs, const char *flag) -> bool {
    return attrs.contains(flag) && attrs.at(flag) == "Y";
}
