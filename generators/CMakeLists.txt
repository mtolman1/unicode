
find_package(OpenSSL COMPONENTS SSL Crypto REQUIRED)
find_package(Threads REQUIRED)

add_subdirectory(deps)
add_executable(unicode-generator
        src/db_generator.cpp
        src/xml.cpp
        src/xml.h
        src/downloader.cpp
        src/downloader.h
        src/file_generators/flags.cpp
        src/file_generators/flags.h
        src/file_generators/string_props.cpp
        src/file_generators/string_props.h
        src/file_generators/aliases.cpp
        src/file_generators/aliases.h
        src/utils.cpp
        src/utils.h
        src/file_generators/enums.cpp
        src/file_generators/enums.h
        src/file_generators/num_props.cpp
        src/file_generators/num_props.h
        src/file_generators/script_extensions.cpp
        src/file_generators/script_extensions.h
        src/file_generators/blocks.cpp src/file_generators/blocks.h)
target_link_libraries(unicode-generator PUBLIC ${OPENSSL_LIBRARIES} elzip pugixml Threads::Threads)
target_include_directories(unicode-generator PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/../deps deps ${OPENSSL_INCLUDE_DIR})
