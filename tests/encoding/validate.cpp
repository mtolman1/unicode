#include "doctest.h"
#include <encoding/validate.h>

TEST_SUITE("[encoding][validate]") {
    TEST_CASE("[is_valid][utf8]") {
        std::u8string str = u8"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::string s = "¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE(unicode::encoding::validate::is_valid_utf8(str));
        REQUIRE(unicode::encoding::validate::is_valid_utf8(s));
    }

    TEST_CASE("[is_valid][utf8][corrupted]") {
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf8(u8"\x9F"));
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf8("\x9F"));
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf8(u8"\xF0\x9F\xF0\x9F\x92\xA9"));
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf8(u8"\xF0\x9F\xF0\x9F\x92\xA9"));
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf8(u8"\xF0\x9F"));
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf8(u8"\xF0\x9F"));
    }

    TEST_CASE("[is_valid][utf16]") {
        std::u16string str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        REQUIRE(unicode::encoding::validate::is_valid_utf16(str));
    }

    TEST_CASE("[is_valid][utf16][corrupted]") {
        std::u16string str = u"hello";
        str[1] = 0xDFFF;
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf16(str));
        str[2] = 0xD9FF;
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf16(str));
        str = u"hello";
        REQUIRE(unicode::encoding::validate::is_valid_utf16(str));
        str[1] = 0x04F;
        str[2] = 0xDCFF;
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf16(str));
        str = u"hello";
        str[str.size() - 1] = 0xD9FF;
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf16(str));
    }

    TEST_CASE("[is_valid][utf32]") {
        std::u32string str = U"¡héllo!💩\U000FFFFF 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        REQUIRE(unicode::encoding::validate::is_valid_utf32(str));
    }

    TEST_CASE("[is_valid][utf32][corrupted]") {
        std::u32string str = U"hello";
        str[1] = 0xFFFFFFFF;
        REQUIRE_FALSE(unicode::encoding::validate::is_valid_utf32(str));
    }
}
