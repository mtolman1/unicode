#include "doctest.h"
#include <encoding/props.h>

TEST_SUITE("[encoding][props]") {
    TEST_CASE("[is_high_surrogate]") {
        REQUIRE(unicode::encoding::props::is_high_surrogate(0xD800));
        REQUIRE(unicode::encoding::props::is_high_surrogate(0xDBFF));

        REQUIRE(!unicode::encoding::props::is_high_surrogate(0xD800 - 1));
        REQUIRE(!unicode::encoding::props::is_high_surrogate(0xDBFF + 1));
    }

    TEST_CASE("[is_low_surrogate]") {
        REQUIRE(unicode::encoding::props::is_low_surrogate(0xDC00));
        REQUIRE(unicode::encoding::props::is_low_surrogate(0xDFFF));

        REQUIRE(!unicode::encoding::props::is_low_surrogate(0xDC00 - 1));
        REQUIRE(!unicode::encoding::props::is_low_surrogate(0xDFFF + 1));
    }

    TEST_CASE("[is_surrogate]") {
        REQUIRE(unicode::encoding::props::is_surrogate(0xD800));
        REQUIRE(unicode::encoding::props::is_surrogate(0xDBFF));
        REQUIRE(unicode::encoding::props::is_surrogate(0xDC00));
        REQUIRE(unicode::encoding::props::is_surrogate(0xDFFF));

        REQUIRE(!unicode::encoding::props::is_surrogate(0xD800 - 1));
        REQUIRE(!unicode::encoding::props::is_surrogate(0xDFFF + 1));
    }
}
