#include "doctest.h"
#include <encoding/byte_order.h>

TEST_SUITE("[encoding][byte_order]") {
    TEST_CASE("[is_bom]") {
        REQUIRE(unicode::encoding::byte_order::is_bom(U'\uFEFF'));
        REQUIRE(unicode::encoding::byte_order::is_bom_rev_16_bit(0xFFFE));
        REQUIRE(unicode::encoding::byte_order::is_bom_rev_32_bit(0xFFFE0000));
    }

    TEST_CASE("[flip_endian][utf16]") {
        std::u16string s1 = u"\ufeff";
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
        unicode::encoding::byte_order::flip_endian(s1);
        REQUIRE(unicode::encoding::byte_order::is_bom_rev_16_bit(s1[0]));
        unicode::encoding::byte_order::flip_endian(s1);
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
    }

    TEST_CASE("[flip_endian][utf32]") {
        std::u32string s1 = U"\ufeff";
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
        unicode::encoding::byte_order::flip_endian(s1);
        REQUIRE(unicode::encoding::byte_order::is_bom_rev_32_bit(s1[0]));
        unicode::encoding::byte_order::flip_endian(s1);
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
    }

    TEST_CASE("[flip_endian_ch][utf16]") {
        std::u16string s1 = u"\ufeff";
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
        unicode::encoding::byte_order::flip_endian(s1);
        REQUIRE(unicode::encoding::byte_order::is_bom_rev_16_bit(s1[0]));
        unicode::encoding::byte_order::flip_endian(s1);
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
    }

    TEST_CASE("[flip_endian_ch][utf16]") {
        char16_t ch = u'\ufeff';
        REQUIRE(unicode::encoding::byte_order::is_bom(ch));
        ch = unicode::encoding::byte_order::flip_endian_ch16(ch);
        REQUIRE(unicode::encoding::byte_order::is_bom_rev_16_bit(ch));
        ch = unicode::encoding::byte_order::flip_endian_ch16(ch);
        REQUIRE(unicode::encoding::byte_order::is_bom(ch));
    }

    TEST_CASE("[flip_endian_ch][utf32]") {
        char32_t ch = u'\ufeff';
        REQUIRE(unicode::encoding::byte_order::is_bom(ch));
        ch = unicode::encoding::byte_order::flip_endian_ch32(ch);
        REQUIRE(unicode::encoding::byte_order::is_bom_rev_32_bit(ch));
        ch = unicode::encoding::byte_order::flip_endian_ch32(ch);
        REQUIRE(unicode::encoding::byte_order::is_bom(ch));
    }

    TEST_CASE("[add_bom][utf16]") {
        std::u16string s1 = u"hello";
        REQUIRE(!unicode::encoding::byte_order::is_bom(s1[0]));
        unicode::encoding::byte_order::add_bom(s1);
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
        REQUIRE_EQ(s1, u"\ufeffhello");
    }

    TEST_CASE("[add_bom][utf32]") {
        std::u32string s1 = U"hello";
        REQUIRE(!unicode::encoding::byte_order::is_bom(s1[0]));
        unicode::encoding::byte_order::add_bom(s1);
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
        REQUIRE_EQ(s1, U"\ufeffhello");
    }

    TEST_CASE("[remove_bom][utf16]") {
        std::u16string s1 = u"\ufeffhello";
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
        unicode::encoding::byte_order::remove_bom(s1);
        REQUIRE(!unicode::encoding::byte_order::is_bom(s1[0]));
        REQUIRE_EQ(s1, u"hello");
    }

    TEST_CASE("[remove_bom][utf32]") {
        std::u32string s1 = U"\ufeffhello";
        REQUIRE(unicode::encoding::byte_order::is_bom(s1[0]));
        unicode::encoding::byte_order::remove_bom(s1);
        REQUIRE(!unicode::encoding::byte_order::is_bom(s1[0]));
        REQUIRE_EQ(s1, U"hello");
    }
}