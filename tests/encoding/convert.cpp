#include "doctest.h"
#include <encoding/convert.h>
#include <encoding/byte_order.h>
#include <bit>

// Ignoring unreachable code for big-endian vs little-endian machine tests
#pragma clang diagnostic push
#pragma ide diagnostic ignored "UnreachableCode"
TEST_SUITE("[encoding][convert]") {
    TEST_CASE("[conversion][utf8][utf32]") {
        std::u8string str = u8"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u32string s = U"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf8_to_utf32(str));
    }

    TEST_CASE("[conversion][utf32][utf8]") {
        std::u32string str = U"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u8string s = u8"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf8(str));
    }

    TEST_CASE("[conversion][utf32][str]") {
        std::u32string str = U"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::string s = "¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_str(str));
    }

    TEST_CASE("[conversion][utf32][str][detect_bom]") {
        std::u32string str = U"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::encoding::byte_order::flip_endian(str);
        std::string s = "\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_str(str));
    }

    TEST_CASE("[conversion][utf32][utf8][detect_bom]") {
        std::u32string str = U"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::encoding::byte_order::flip_endian(str);
        std::u8string s = u8"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf8(str));
    }

    TEST_CASE("[conversion][utf32][utf8][remove_bom]") {
        std::u32string str = U"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u8string s = u8"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf8(str, true));
        unicode::encoding::byte_order::flip_endian(str);
        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf8(str, true));
    }

    TEST_CASE("[conversion][utf8][str]") {
        std::u8string str = u8"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::string s = "¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf8_to_str(str));
    }

    TEST_CASE("[conversion][str][utf8]") {
        std::string str = "¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u8string s = u8"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::str_to_utf8(str));
    }

    TEST_CASE("[conversion][str][utf32]") {
        std::string str = "¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u32string s = U"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::str_to_utf32(str));
    }

    TEST_CASE("[conversion][utf16][utf32]") {
        std::u16string str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u32string s = U"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf32(str));
    }

    TEST_CASE("[conversion][utf16][utf32][detect_bom]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::encoding::byte_order::flip_endian(str);

        std::u32string s = U"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        auto res = unicode::encoding::convert::utf16_to_utf32(str);

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf32(str));
    }

    TEST_CASE("[conversion][utf32][utf16][detect_bom]") {
        std::u32string str = U"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::encoding::byte_order::flip_endian(str);

        std::u16string s = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf16(str));
    }

    TEST_CASE("[conversion][utf32][utf16][simple]1") {
        std::u32string str = U"\U00010437";
        std::u16string s = u"\U00010437";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf16(str));
    }

    TEST_CASE("[conversion][utf32][utf16][simple]2") {
        std::u32string str = U"\U00000024";
        std::u16string s = u"\U00000024";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf16(str));
    }

    TEST_CASE("[conversion][utf32][utf16][simple]3") {
        std::u32string str = U"\U000020AC";
        std::u16string s = u"\U000020AC";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf16(str));
    }

    TEST_CASE("[conversion][utf32][utf16][simple]4") {
        std::u32string str = U"\U00024B62";
        std::u16string s = u"\U00024B62";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf16(str));
    }

    TEST_CASE("[conversion][utf16][utf32][simple]1") {
        std::u16string str = u"\U00010437";
        std::u32string s = U"\U00010437";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf32(str));
    }

    TEST_CASE("[conversion][utf16][utf32][simple]2") {
        std::u16string str = u"\U00000024";
        std::u32string s = U"\U00000024";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf32(str));
    }

    TEST_CASE("[conversion][utf16][utf32][simple]3") {
        std::u16string str = u"\U000020AC";
        std::u32string s = U"\U000020AC";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf32(str));
    }

    TEST_CASE("[conversion][utf16][utf32][simple]4") {
        std::u16string str = u"\U00024B62";
        std::u32string s = U"\U00024B62";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf32(str));
    }

    TEST_CASE("[conversion][utf32][utf16]") {
        std::u32string str = U"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u16string s = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf32_to_utf16(str));
    }

    TEST_CASE("[conversion][utf16][str]") {
        std::u16string str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::string s = "¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_str(str));
    }

    TEST_CASE("[conversion][utf16][utf8]") {
        std::u16string str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u8string s = u8"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf8(str));
    }

    TEST_CASE("[conversion][utf16][utf8][detect_bom]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::encoding::byte_order::flip_endian(str);
        std::u8string s = u8"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf8(str));
    }

    TEST_CASE("[conversion][utf16][str][detect_bom]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::string s = "\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_str(str));

        unicode::encoding::byte_order::flip_endian(str);
        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_str(str));
    }

    TEST_CASE("[conversion][utf16le][utf16]") {
        unicode::u16lestring str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::little) {
            unicode::encoding::byte_order::flip_endian(str);
        }
        std::u16string s = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16le_to_utf16(str));
    }

    TEST_CASE("[conversion][utf16le][utf16][addBom]") {
        unicode::u16lestring str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::little) {
            unicode::encoding::byte_order::flip_endian(str);
        }
        std::u16string s = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16le_to_utf16(str, true));
    }

    TEST_CASE("[conversion][utf16be][utf16]") {
        unicode::u16lestring str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::big) {
            unicode::encoding::byte_order::flip_endian(str);
        }
        std::u16string s = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16be_to_utf16(str));
    }

    TEST_CASE("[conversion][utf16be][utf16][addBom]") {
        unicode::u16lestring str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::big) {
            unicode::encoding::byte_order::flip_endian(str);
        }
        std::u16string s = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16be_to_utf16(str, true));
    }

    TEST_CASE("[conversion][utf16][utf16le]") {
        std::u16string str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::u16lestring s = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::little) {
            unicode::encoding::byte_order::flip_endian(s);
        }

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf16le(str));
    }

    TEST_CASE("[conversion][utf16][utf16be]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::u16bestring s = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::big) {
            unicode::encoding::byte_order::flip_endian(s);
        }

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf16be(str));
    }

    TEST_CASE("[conversion][utf16][utf16le][detect_bom]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::encoding::byte_order::flip_endian(str);

        unicode::u16lestring s = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::little) {
            unicode::encoding::byte_order::flip_endian(s);
        }

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf16le(str));
    }

    TEST_CASE("[conversion][utf16][utf16be][detect_bom]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::encoding::byte_order::flip_endian(str);

        unicode::u16bestring s = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::big) {
            unicode::encoding::byte_order::flip_endian(s);
        }

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf16be(str));
    }

    TEST_CASE("[conversion][utf16][utf16le][detect_bom][remove_bom]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::encoding::byte_order::flip_endian(str);

        unicode::u16lestring s = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::little) {
            unicode::encoding::byte_order::flip_endian(s);
        }

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf16le(str, true));
    }

    TEST_CASE("[conversion][utf16][utf16be][detect_bom][remove_bom]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        unicode::encoding::byte_order::flip_endian(str);

        unicode::u16bestring s = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::big) {
            unicode::encoding::byte_order::flip_endian(s);
        }

        REQUIRE_EQ(s, unicode::encoding::convert::utf16_to_utf16be(str, true));
    }

    TEST_CASE("[conversion][utf16be][utf16][addBom]") {
        unicode::u16lestring str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        if constexpr (std::endian::native != std::endian::big) {
            unicode::encoding::byte_order::flip_endian(str);
        }
        std::u16string s = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::utf16be_to_utf16(str, true));
    }

    TEST_CASE("[conversion][str][utf16]") {
        std::string str = "¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u16string s = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        REQUIRE_EQ(s, unicode::encoding::convert::str_to_utf16(str));
    }

    TEST_CASE("[conversion][rune][utf8]") {
        auto rune = U'💩';
        std::u8string expected = u8"💩";

        auto res = unicode::encoding::convert::rune_to_utf8_vector(rune);
        for (size_t i = 0; i < res.size(); ++i) {
            REQUIRE_EQ(res[i], expected[i]);
        }

        auto resNoAlloc = unicode::encoding::convert::rune_to_utf8(rune);
        for (size_t i = 0; i < resNoAlloc.size(); ++i) {
            REQUIRE_EQ(resNoAlloc.at(i), expected[i]);
        }
    }

    TEST_CASE("[conversion][rune][utf16]") {
        auto rune = U'💩';
        std::u16string expected = u"💩";

        auto res = unicode::encoding::convert::rune_to_utf16_vector(rune);
        for (size_t i = 0; i < res.size(); ++i) {
            REQUIRE_EQ(res[i], expected[i]);
        }

        auto resNoAlloc = unicode::encoding::convert::rune_to_utf16(rune);
        for (size_t i = 0; i < resNoAlloc.size(); ++i) {
            REQUIRE_EQ(resNoAlloc.at(i), expected[i]);
        }
    }
}

#pragma clang diagnostic pop