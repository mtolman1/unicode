#include "doctest.h"
#include <encoding/iter.h>
#include <encoding/byte_order.h>
#include <iostream>
#include <vector>

TEST_SUITE("[encoding][iter]") {

    TEST_CASE("[str][rune][iterate]") {
        std::string str = "¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u32string s = U"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        size_t u32Index = 0;
        for (const auto [rune, index]: unicode::encoding::iter::StrRuneIterable{str}) {
            if (rune != s[u32Index]) {
                std::cerr << "index " << u32Index;
            }
            REQUIRE_EQ(rune, s[u32Index++]);
        }
        REQUIRE_EQ(u32Index, s.size());
    }

    TEST_CASE("[str][rune][iterator][compare]") {
        std::string str = "¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        auto iteratorStart = unicode::encoding::iter::impl::StrToUtf32Iterator{str, false};
        auto iteratorMid = unicode::encoding::iter::impl::StrToUtf32Iterator{str, false};
        auto iteratorEnd = unicode::encoding::iter::impl::StrToUtf32Iterator{str, true};
        REQUIRE_EQ(iteratorStart <=> iteratorMid, std::partial_ordering::equivalent);
        REQUIRE_EQ(iteratorStart, iteratorMid);
        REQUIRE_NE(iteratorStart, iteratorEnd);
        iteratorMid++;
        REQUIRE_NE(iteratorStart, iteratorMid);
        REQUIRE_LE(iteratorStart, iteratorMid);
        REQUIRE_LT(iteratorStart, iteratorEnd);
        REQUIRE_GE(iteratorMid, iteratorStart);
        REQUIRE_GT(iteratorEnd, iteratorStart);

        auto otherIter = unicode::encoding::iter::impl::StrToUtf32Iterator{"¡héllo!👦", false};
        REQUIRE_EQ(iteratorStart <=> otherIter, std::partial_ordering::unordered);
        REQUIRE_EQ(iteratorStart <= otherIter, false);
        REQUIRE_EQ(iteratorStart < otherIter, false);
        REQUIRE_EQ(iteratorStart == otherIter, false);
        REQUIRE_EQ(iteratorStart != otherIter, true);
        REQUIRE_EQ(iteratorStart >= otherIter, false);
        REQUIRE_EQ(iteratorStart > otherIter, false);

        REQUIRE_EQ(std::get<0>(*iteratorMid), 'h');
        REQUIRE_EQ(std::get<0>(*(iteratorMid.operator->())), 'h');
    }

    TEST_CASE("[utf8][rune][iterate]") {
        std::u8string str = u8"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u32string s = U"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        size_t u32Index = 0;
        for (const auto [rune, index]: unicode::encoding::iter::Utf8RuneIterable{str}) {
            if (rune != s[u32Index]) {
                std::cerr << "index " << u32Index;
            }
            REQUIRE_EQ(rune, s[u32Index++]);
        }
        REQUIRE_EQ(u32Index, s.size());
    }

    TEST_CASE("[utf8][rune][iterator][compare]") {
        std::u8string str = u8"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        auto iteratorStart = unicode::encoding::iter::impl::Utf8ToUtf32Iterator{str, false};
        auto iteratorMid = unicode::encoding::iter::impl::Utf8ToUtf32Iterator{str, false};
        auto iteratorEnd = unicode::encoding::iter::impl::Utf8ToUtf32Iterator{str, true};
        REQUIRE_EQ(iteratorStart <=> iteratorMid, std::partial_ordering::equivalent);
        REQUIRE_EQ(iteratorStart, iteratorMid);
        REQUIRE_NE(iteratorStart, iteratorEnd);
        iteratorMid++;
        REQUIRE_NE(iteratorStart, iteratorMid);
        REQUIRE_LE(iteratorStart, iteratorMid);
        REQUIRE_LT(iteratorStart, iteratorEnd);
        REQUIRE_GE(iteratorMid, iteratorStart);
        REQUIRE_GT(iteratorEnd, iteratorStart);

        auto otherIter = unicode::encoding::iter::impl::Utf8ToUtf32Iterator{u8"¡héllo!👦", false};
        REQUIRE_EQ(iteratorStart <=> otherIter, std::partial_ordering::unordered);
        REQUIRE_EQ(iteratorStart <= otherIter, false);
        REQUIRE_EQ(iteratorStart < otherIter, false);
        REQUIRE_EQ(iteratorStart == otherIter, false);
        REQUIRE_EQ(iteratorStart != otherIter, true);
        REQUIRE_EQ(iteratorStart >= otherIter, false);
        REQUIRE_EQ(iteratorStart > otherIter, false);

        REQUIRE_EQ(std::get<0>(*iteratorMid), U'h');
        REQUIRE_EQ(std::get<0>(*(iteratorMid.operator->())), U'h');
    }

    TEST_CASE("[utf16][rune][iterate]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u32string s = U"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";

        size_t u32Index = 0;
        for (const auto [rune, index]: unicode::encoding::iter::Utf16RuneIterable{str}) {
            if (rune != s[u32Index]) {
                std::cerr << "index " << u32Index;
            }
            REQUIRE_EQ(rune, s[u32Index++]);
        }
        REQUIRE_EQ(u32Index, s.size());

        u32Index = 0;
        unicode::encoding::byte_order::flip_endian(str);
        for (const auto [rune, index]: unicode::encoding::iter::Utf16RuneIterable{str}) {
            if (rune != s[u32Index]) {
                std::cerr << "index " << u32Index;
            }
            REQUIRE_EQ(rune, s[u32Index++]);
        }
        REQUIRE_EQ(u32Index, s.size());
    }

    TEST_CASE("[utf16][rune][iterator][compare]") {
        std::u16string str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        auto iteratorStart = unicode::encoding::iter::impl::Utf16ToUtf32Iterator{str, false, false};
        auto iteratorMid = unicode::encoding::iter::impl::Utf16ToUtf32Iterator{str, false, false};
        auto iteratorEnd = unicode::encoding::iter::impl::Utf16ToUtf32Iterator{str, true, false};
        REQUIRE_EQ(iteratorStart <=> iteratorMid, std::partial_ordering::equivalent);
        REQUIRE_EQ(iteratorStart, iteratorMid);
        REQUIRE_NE(iteratorStart, iteratorEnd);
        iteratorMid++;
        REQUIRE_NE(iteratorStart, iteratorMid);
        REQUIRE_LE(iteratorStart, iteratorMid);
        REQUIRE_LT(iteratorStart, iteratorEnd);
        REQUIRE_GE(iteratorMid, iteratorStart);
        REQUIRE_GT(iteratorEnd, iteratorStart);

        auto otherIter = unicode::encoding::iter::impl::Utf16ToUtf32Iterator{u"¡héllo!👦", false, false};
        REQUIRE_EQ(iteratorStart <=> otherIter, std::partial_ordering::unordered);
        REQUIRE_EQ(iteratorStart <= otherIter, false);
        REQUIRE_EQ(iteratorStart < otherIter, false);
        REQUIRE_EQ(iteratorStart == otherIter, false);
        REQUIRE_EQ(iteratorStart != otherIter, true);
        REQUIRE_EQ(iteratorStart >= otherIter, false);
        REQUIRE_EQ(iteratorStart > otherIter, false);

        REQUIRE_EQ(std::get<0>(*iteratorMid), U'h');
        REQUIRE_EQ(std::get<0>(*(iteratorMid.operator->())), U'h');
    }

    TEST_CASE("[utf16][endian][iterate]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u16string s = str;

        size_t index = 0;
        for (const auto ch: unicode::encoding::iter::Utf16EndianIterable{str}) {
            REQUIRE_EQ(ch, s[index++]);
        }
        REQUIRE_EQ(index, s.size());

        index = 0;
        unicode::encoding::byte_order::flip_endian(str);
        for (const auto ch: unicode::encoding::iter::Utf16EndianIterable{str}) {
            REQUIRE_EQ(ch, s[index++]);
        }
        REQUIRE_EQ(index, s.size());
    }

    TEST_CASE("[utf16][endian][iterator][compare]") {
        std::u16string str = u"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        auto iteratorStart = unicode::encoding::iter::impl::Utf16EndianIterator{str, false, false};
        auto iteratorMid = unicode::encoding::iter::impl::Utf16EndianIterator{str, false, false};
        auto iteratorEnd = unicode::encoding::iter::impl::Utf16EndianIterator{str, true, false};
        REQUIRE_EQ(iteratorStart <=> iteratorMid, std::partial_ordering::equivalent);
        REQUIRE_EQ(iteratorStart, iteratorMid);
        REQUIRE_NE(iteratorStart, iteratorEnd);
        iteratorMid++;
        REQUIRE_NE(iteratorStart, iteratorMid);
        REQUIRE_LE(iteratorStart, iteratorMid);
        REQUIRE_LT(iteratorStart, iteratorEnd);
        REQUIRE_GE(iteratorMid, iteratorStart);
        REQUIRE_GT(iteratorEnd, iteratorStart);

        auto otherIter = unicode::encoding::iter::impl::Utf16EndianIterator{u"¡héllo!👦", false, false};
        REQUIRE_EQ(iteratorStart <=> otherIter, std::partial_ordering::unordered);
        REQUIRE_EQ(iteratorStart <= otherIter, false);
        REQUIRE_EQ(iteratorStart < otherIter, false);
        REQUIRE_EQ(iteratorStart == otherIter, false);
        REQUIRE_EQ(iteratorStart != otherIter, true);
        REQUIRE_EQ(iteratorStart >= otherIter, false);
        REQUIRE_EQ(iteratorStart > otherIter, false);

        REQUIRE_EQ(*iteratorMid, u'h');
        REQUIRE_EQ(*(iteratorMid.operator->()), u'h');
    }

    TEST_CASE("[utf16][target][endian][iterate]") {
        std::u16string str = u"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u16string s = str;

        size_t index = 0;
        unicode::encoding::byte_order::flip_endian(str);
        for (const auto ch: unicode::encoding::iter::Utf16TargetEndianIterable{unicode::encoding::byte_order::endian::machine, str}) {
            REQUIRE_EQ(ch, s[index++]);
        }
        REQUIRE_EQ(index, s.size());

        index = 0;
        unicode::encoding::byte_order::flip_endian(str);
        unicode::encoding::byte_order::flip_endian(s);
        for (const auto ch: unicode::encoding::iter::Utf16TargetEndianIterable{unicode::encoding::byte_order::endian::opposite, str}) {
            REQUIRE_EQ(ch, s[index++]);
        }
        REQUIRE_EQ(index, s.size());
    }

    TEST_CASE("[utf32][endian][iterate]") {
        std::u32string str = U"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u32string s = str;

        size_t index = 0;
        for (const auto ch: unicode::encoding::iter::Utf32EndianIterable{str}) {
            REQUIRE_EQ(ch, s[index++]);
        }
        REQUIRE_EQ(index, s.size());

        index = 0;
        unicode::encoding::byte_order::flip_endian(str);
        for (const auto ch: unicode::encoding::iter::Utf32EndianIterable{str}) {
            REQUIRE_EQ(ch, s[index++]);
        }
        REQUIRE_EQ(index, s.size());
    }

    TEST_CASE("[utf32][endian][iterator][compare]") {
        std::u32string str = U"¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        auto iteratorStart = unicode::encoding::iter::impl::Utf32EndianIterator{str, false, false};
        auto iteratorMid = unicode::encoding::iter::impl::Utf32EndianIterator{str, false, false};
        auto iteratorEnd = unicode::encoding::iter::impl::Utf32EndianIterator{str, true, false};
        REQUIRE_EQ(iteratorStart <=> iteratorMid, std::partial_ordering::equivalent);
        REQUIRE_EQ(iteratorStart, iteratorMid);
        REQUIRE_NE(iteratorStart, iteratorEnd);
        iteratorMid++;
        REQUIRE_NE(iteratorStart, iteratorMid);
        REQUIRE_LE(iteratorStart, iteratorMid);
        REQUIRE_LT(iteratorStart, iteratorEnd);
        REQUIRE_GE(iteratorMid, iteratorStart);
        REQUIRE_GT(iteratorEnd, iteratorStart);

        auto otherIter = unicode::encoding::iter::impl::Utf32EndianIterator{U"¡héllo!👦", false, false};
        REQUIRE_EQ(iteratorStart <=> otherIter, std::partial_ordering::unordered);
        REQUIRE_EQ(iteratorStart <= otherIter, false);
        REQUIRE_EQ(iteratorStart < otherIter, false);
        REQUIRE_EQ(iteratorStart == otherIter, false);
        REQUIRE_EQ(iteratorStart != otherIter, true);
        REQUIRE_EQ(iteratorStart >= otherIter, false);
        REQUIRE_EQ(iteratorStart > otherIter, false);

        REQUIRE_EQ(*iteratorMid, U'h');
        REQUIRE_EQ(*(iteratorMid.operator->()), U'h');
    }

    TEST_CASE("[utf32][target][endian][iterate]") {
        std::u32string str = U"\ufeff¡héllo!💩 🧟‍♀️¢∂≈˚å∆ñm👨‍👩‍👧‍👦";
        std::u32string s = str;

        size_t index = 0;
        unicode::encoding::byte_order::flip_endian(str);
        for (const auto ch: unicode::encoding::iter::Utf32TargetEndianIterable{unicode::encoding::byte_order::endian::machine, str}) {
            REQUIRE_EQ(ch, s[index++]);
        }
        REQUIRE_EQ(index, s.size());

        index = 0;
        unicode::encoding::byte_order::flip_endian(str);
        unicode::encoding::byte_order::flip_endian(s);
        for (const auto ch: unicode::encoding::iter::Utf32TargetEndianIterable{unicode::encoding::byte_order::endian::opposite, str}) {
            REQUIRE_EQ(ch, s[index++]);
        }
        REQUIRE_EQ(index, s.size());
    }

    TEST_CASE("[corrupted]") {
        std::u8string str = u8"\x9F\xF0\x9F\xF0\x9F\x92\xA9\xF0\x9F";
        auto expected = std::vector<char32_t>{U'\0', U'\0', U'\U0001F4A9', U'\0'};
        auto actual = std::vector<char32_t>{};
        for (const auto [rune, index] : unicode::encoding::iter::Utf8RuneIterable{str}) {
            actual.emplace_back(rune);
        }
        REQUIRE_EQ(expected.size(), actual.size());
        for (size_t i = 0; i < actual.size(); ++i) {
            CHECK_EQ(expected[i], actual[i]);
        }
    }
}
