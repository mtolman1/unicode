#include "doctest.h"
#include <db/ch/join.h>
#include "layers/base.h"

TEST_SUITE("[ch][join][data]") {
    static const auto base = unicode::layers::base::base_unicode_layer(); // NOLINT(cert-err58-cpp)

    TEST_CASE("join_type") {
        REQUIRE_EQ(unicode::db::ch::join_type(U'\u0001'), unicode::db::ch::JoinType::U);
        REQUIRE_EQ(unicode::db::ch::join_type(U'\u0640'), unicode::db::ch::JoinType::C);
        REQUIRE_EQ(base->join_type(U'\u0001'), unicode::db::ch::JoinType::U);
        REQUIRE_EQ(base->join_type(U'\u0640'), unicode::db::ch::JoinType::C);
    }

    TEST_CASE("joining_group") {
        REQUIRE_EQ(unicode::db::ch::joining_group(U'\u0001'), unicode::db::ch::JoiningGroup::NO_JOINING_GROUP);
        REQUIRE_EQ(unicode::db::ch::joining_group(U'\u08BC'), unicode::db::ch::JoiningGroup::AFRICAN_QAF);
        REQUIRE_EQ(base->joining_group(U'\u0001'), unicode::db::ch::JoiningGroup::NO_JOINING_GROUP);
        REQUIRE_EQ(base->joining_group(U'\u08BC'), unicode::db::ch::JoiningGroup::AFRICAN_QAF);
    }

    TEST_CASE("combining_properties") {
        REQUIRE_EQ(unicode::db::ch::combining_properties(U'\u302A'), 218);
        REQUIRE_EQ(base->combining_properties(U'\u302A'), 218);
    }
}