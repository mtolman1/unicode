#include "doctest.h"
#include <db/ch/numeric.h>

TEST_SUITE("[ch][numeric][data]") {
    TEST_CASE("[value]") {
        REQUIRE_EQ(unicode::db::ch::numeric_value(U'9'), std::string{"9"});
        REQUIRE_EQ(unicode::db::ch::numeric_value(U'Z'), nullptr);
    }

    TEST_CASE("[lookup]") {
        REQUIRE_EQ(*unicode::db::ch::chars_by_numeric_value("-1/2").begin(), 0x0F33);
        REQUIRE_EQ(*unicode::db::ch::chars_by_numeric_value(std::u8string_view{u8"-1/2"}).begin(), 0x0F33);
        REQUIRE_EQ(*unicode::db::ch::chars_by_numeric_value(u"-1/2").begin(), 0x0F33);
        REQUIRE_EQ(*unicode::db::ch::chars_by_numeric_value(U"-1/2").begin(), 0x0F33);
        REQUIRE_EQ(*unicode::db::ch::chars_by_numeric_value(-0.5).begin(), 0x0F33);
    }
}
