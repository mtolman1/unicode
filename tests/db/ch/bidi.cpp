#include "doctest.h"
#include <db/ch/bidi.h>
#include "layers/base.h"

TEST_SUITE("[ch][bidi][data]") {
    static const auto base = unicode::layers::base::base_unicode_layer(); // NOLINT(cert-err58-cpp)

    TEST_CASE("bidi_class") {
        REQUIRE_EQ(unicode::db::ch::bidi_class(U'a').value_or(unicode::db::ch::BidiClass::B), unicode::db::ch::BidiClass::L);
        REQUIRE_EQ(unicode::db::ch::bidi_class(U'\U00002066').value_or(unicode::db::ch::BidiClass::B), unicode::db::ch::BidiClass::LRI);
        REQUIRE_EQ(base->bidi_class(U'a').value_or(unicode::db::ch::BidiClass::B), unicode::db::ch::BidiClass::L);
        REQUIRE_EQ(base->bidi_class(U'\U00002066').value_or(unicode::db::ch::BidiClass::B), unicode::db::ch::BidiClass::LRI);
    }

    TEST_CASE("bidi_mirrored_glyph (defined only)") {
        // The XML file has this property be empty since the algorithm should be font-specific
        // Keeping it here though so that if bmg is provided in the future, we will have it available
        REQUIRE_EQ(unicode::db::ch::bidi_mirrored_glyph(U'\U0000FF09'), U'\U0000FF08');
        REQUIRE_EQ(base->bidi_mirrored_glyph(U'\U0000FF09'), U'\U0000FF08');

        auto custom = unicode::layers::CharacterLayer{base};
        custom.override_bidi_mirrored_glyph([](const auto&){ return ')';});
        REQUIRE_EQ(custom.bidi_mirrored_glyph(U'('), U')');
    }

    TEST_CASE("bidi_paired_bracket") {
        REQUIRE_EQ(unicode::db::ch::bidi_paired_bracket(U'\U0000FE59'), U'\U0000FE5A');
        REQUIRE_EQ(base->bidi_paired_bracket(U'\U0000FE59'), U'\U0000FE5A');
    }

    TEST_CASE("bidi_paired_bracket_type") {
        REQUIRE_EQ(unicode::db::ch::bidi_paired_bracket_type(U'\U0000FE59'), unicode::db::ch::BidiPairedBracketType::O);
        REQUIRE_EQ(base->bidi_paired_bracket_type(U'\U0000FE59'), unicode::db::ch::BidiPairedBracketType::O);
    }
}