#include "doctest.h"
#include "db/ch/tangut.h"

using namespace unicode::db::ch;

TEST_SUITE("[ch][tangut][data]") {
    TEST_CASE("kRSTUnicode") {
        REQUIRE_EQ(kRSTUnicode(U'\U00017017'), std::string{"1.10"});
    }

    TEST_CASE("kTGT_MergedSrc") {
        REQUIRE_EQ(kTGT_MergedSrc(U'\U00017462'), std::string{"H2004-A-1015"});
    }
}