#include "doctest.h"
#include <db/ch/metax.h>

TEST_SUITE("[ch][metax][data]") {
    TEST_CASE("char_by_legacy_name") {
        REQUIRE_EQ(unicode::db::ch::char_by_legacy_name("OPENING CURLY BRACKET"), U'\u007B');
    }

    TEST_CASE("legacy_name") {
        REQUIRE_EQ(unicode::db::ch::legacy_name(U'\u007B'), std::string{"OPENING CURLY BRACKET"});
    }

    TEST_CASE("iso10646_comment") {
        // Empty for now
        unicode::db::ch::iso10646_comment(U'\u007B');
    }
}