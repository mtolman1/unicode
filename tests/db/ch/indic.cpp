#include "doctest.h"
#include <db/ch/indic.h>
#include "layers/base.h"

TEST_SUITE("[ch][indic][data]") {
    static const auto base = unicode::layers::base::base_unicode_layer(); // NOLINT(cert-err58-cpp)

    TEST_CASE("indic_mantra_category") {
        // Currently empty in XML file, making sure it's at least present
        unicode::db::ch::indic_mantra_category(U'a');
        REQUIRE(!base->indic_mantra_category(U'a').has_value());
    }

    TEST_CASE("indic_positional_category") {
        REQUIRE_EQ(unicode::db::ch::indic_positional_category(U'\U0000ABED'), unicode::db::ch::IndicPositionalCategory::BOTTOM);
        REQUIRE_EQ(base->indic_positional_category(U'\U0000ABED'), unicode::db::ch::IndicPositionalCategory::BOTTOM);
    }

    TEST_CASE("indic_syllabic_category") {
        REQUIRE_EQ(unicode::db::ch::indic_syllabic_category(U'\U00001BBA'), unicode::db::ch::IndicSyllabicCategory::AVAGRAHA);
        REQUIRE_EQ(base->indic_syllabic_category(U'\U00001BBA'), unicode::db::ch::IndicSyllabicCategory::AVAGRAHA);
    }
}