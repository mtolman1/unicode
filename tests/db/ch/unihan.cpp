#include "doctest.h"
#include "db/ch/unihan.h"

using namespace unicode::db::ch;

TEST_SUITE("[ch][unihan][data]") {
    TEST_CASE("kBigFive") {
        REQUIRE_EQ(kBigFive(U'\u5159'), std::string{"A259"});
    }

    TEST_CASE("kIRGKangXi") {
        REQUIRE_EQ(kIRGKangXi(U'\u4E00'), std::string{"0075.010"});
    }

    TEST_CASE("kRSKorean") {
        // No data yet, but want to make sure function exists
        REQUIRE_EQ(kRSKorean(U'\u4E00'), nullptr);
    }

    TEST_CASE("kAccountingNumeric") {
        REQUIRE_EQ(kAccountingNumeric(U'\u58F1'), std::string{"1"});
    }

    TEST_CASE("kAlternateKangXi") {
        // No data yet, but want to make sure function exists
        REQUIRE_EQ(kAlternateKangXi(U'\u58F1'), nullptr);
    }

    TEST_CASE("kAlternateJEF") {
        // No data yet, but want to make sure function exists
        REQUIRE_EQ(kAlternateJEF(U'\u58F1'), nullptr);
    }

    TEST_CASE("kAlternateMorohashi") {
        // No data yet, but want to make sure function exists
        REQUIRE_EQ(kAlternateMorohashi(U'\u58F1'), nullptr);
    }

    TEST_CASE("kAlternateMorohashi") {
        // No data yet, but want to make sure function exists
        REQUIRE_EQ(kAlternateMorohashi(U'\u58F1'), nullptr);
    }

    TEST_CASE("kAlternateTotalStrokes") {
        REQUIRE_EQ(kAlternateTotalStrokes(U'\u9AA8'), std::string{"10:HJKPV"});
    }

    TEST_CASE("kCCCII") {
        REQUIRE_EQ(kCCCII(U'\u8A01'), std::string{"212D5E"});
    }

    TEST_CASE("kCNS1986") {
        REQUIRE_EQ(kCNS1986(U'\u5159'), std::string{"1-2279"});
    }

    TEST_CASE("kCNS1992") {
        REQUIRE_EQ(kCNS1992(U'\u5159'), std::string{"1-2279"});
    }

    TEST_CASE("kCangjie") {
        REQUIRE_EQ(kCangjie(U'\u5309'), std::string{"PMFJ"});
    }

    TEST_CASE("kCantonese") {
        REQUIRE_EQ(kCantonese(U'\u9E26'), std::string{"aa1"});
    }

    TEST_CASE("kCheungBauer") {
        REQUIRE_EQ(kCheungBauer(U'\U000233B4'), std::string{"001/03;MF;dan2"});
    }

    TEST_CASE("kCheungBauerIndex") {
        REQUIRE_EQ(kCheungBauerIndex(U'\U00025531'), std::string{"137.09"});
    }

    TEST_CASE("kCihaiT") {
        REQUIRE_EQ(kCihaiT(U'\u4E00'), std::string{"1.101"});
    }

    TEST_CASE("kCompatibilityVariant") {
        REQUIRE_EQ(kCompatibilityVariant(U'\U0002F803'), std::string{"U+20122"});
    }

    TEST_CASE("kCowles") {
        REQUIRE_EQ(kCowles(U'\U0002BB28'), std::string{"0045"});
    }

    TEST_CASE("kDaeJaweon") {
        REQUIRE_EQ(kDaeJaweon(U'\u4E00'), std::string{"0129.010"});
    }

    TEST_CASE("kDefinition") {
        REQUIRE_EQ(kDefinition(U'\u543D'), std::string{"'OM'; bellow; (Cant.) dull, stupid"});
    }

    TEST_CASE("kEACC") {
        REQUIRE_EQ(kEACC(U'\u4E00'), std::string{"213021"});
    }

    TEST_CASE("kFenn") {
        REQUIRE_EQ(kFenn(U'\u858A'), std::string{"0G"});
    }

    TEST_CASE("kFennIndex") {
        REQUIRE_EQ(kFennIndex(U'\u554A'), std::string{"1.01"});
    }

    TEST_CASE("kFourCornerCode") {
        REQUIRE_EQ(kFourCornerCode(U'\u4EA0'), std::string{"0000.0"});
    }

    TEST_CASE("kFrequency") {
        REQUIRE_EQ(kFrequency(U'\u9EBC'), std::string{"1"});
    }

    TEST_CASE("kGB0") {
        REQUIRE_EQ(kGB0(U'\u554A'), std::string{"1601"});
    }

    TEST_CASE("kGB1") {
        REQUIRE_EQ(kGB1(U'\u554A'), std::string{"1601"});
    }

    TEST_CASE("kGB3") {
        REQUIRE_EQ(kGB3(U'\u4E0F'), std::string{"1601"});
    }

    TEST_CASE("kGB5") {
        REQUIRE_EQ(kGB5(U'\u4E02'), std::string{"1601"});
    }

    TEST_CASE("kGB7") {
        REQUIRE_EQ(kGB7(U'\u57AF'), std::string{"0101"});
    }

    TEST_CASE("kGB8") {
        REQUIRE_EQ(kGB8(U'\u540B'), std::string{"0883"});
    }

    TEST_CASE("kGSR") {
        REQUIRE_EQ(kGSR(U'\u53EF'), std::string{"0001a"});
    }

    TEST_CASE("kGradeLevel") {
        REQUIRE_EQ(kGradeLevel(U'\u9F4A'), std::string{"1"});
    }

    TEST_CASE("kHDZRadBreak") {
        REQUIRE_EQ(kHDZRadBreak(U'\u4E00'), std::u8string{u8"⼀[U+2F00]:10001.010"});
    }

    TEST_CASE("kHKGlyph") {
        REQUIRE_EQ(kHKGlyph(U'\u4E00'), std::string{"0001"});
    }

    TEST_CASE("kHKSCS") {
        REQUIRE_EQ(kHKSCS(U'\u43F0'), std::string{"8740"});
    }

    TEST_CASE("kHanYu") {
        REQUIRE_EQ(kHanYu(U'\u4E00'), std::string{"10001.010"});
    }

    TEST_CASE("kHangul") {
        REQUIRE_EQ(kHangul(U'\u8857'), std::u8string{u8"가:0E"});
    }

    TEST_CASE("kHanyuPinlu") {
        REQUIRE_EQ(kHanyuPinlu(U'\u4E00'), std::u8string{u8"yī(32747)"});
    }

    TEST_CASE("kHanyuPinyin") {
        REQUIRE_EQ(kHanyuPinyin(U'\u4E09'), std::u8string{u8"10004.030:sān"});
    }

    TEST_CASE("kIBMJapan") {
        REQUIRE_EQ(kIBMJapan(U'\u7E8A'), std::string{"FA5C"});
    }

    TEST_CASE("kIICore") {
        REQUIRE_EQ(kIICore(U'\u9F9F'), std::string{"AG"});
    }

    TEST_CASE("kIRGDaeJaweon") {
        REQUIRE_EQ(kIRGDaeJaweon(U'\u4E00'), std::string{"0129.010"});
    }

    TEST_CASE("kIRGDaiKanwaZiten") {
        REQUIRE_EQ(kIRGDaiKanwaZiten(U'\u4E00'), std::string{"00001"});
    }

    TEST_CASE("kIRGHanyuDaZidian") {
        REQUIRE_EQ(kIRGHanyuDaZidian(U'\u4E00'), std::string{"10001.010"});
    }

    TEST_CASE("kIRG_GSource") {
        REQUIRE_EQ(kIRG_GSource(U'\u4E00'), std::string{"G0-523B"});
    }

    TEST_CASE("kIRG_HSource") {
        REQUIRE_EQ(kIRG_HSource(U'\u43F0'), std::string{"H-8740"});
    }

    TEST_CASE("kIRG_JSource") {
        REQUIRE_EQ(kIRG_JSource(U'\u4EDD'), std::string{"J0-2138"});
    }

    TEST_CASE("kIRG_KPSource") {
        REQUIRE_EQ(kIRG_KPSource(U'\u4F3D'), std::string{"KP0-CDA1"});
    }

    TEST_CASE("kIRG_KSource") {
        REQUIRE_EQ(kIRG_KSource(U'\u4F3D'), std::string{"K0-4A21"});
    }

    TEST_CASE("kIRG_MSource") {
        REQUIRE_EQ(kIRG_MSource(U'\u53D8'), std::string{"MA-895A"});
    }

    TEST_CASE("kIRG_SSource") {
        REQUIRE_EQ(kIRG_SSource(U'\U0002E7E5'), std::string{"SAT-00002"});
    }

    TEST_CASE("kIRG_TSource") {
        REQUIRE_EQ(kIRG_TSource(U'\u5159'), std::string{"T1-2279"});
    }

    TEST_CASE("kIRG_UKSource") {
        REQUIRE_EQ(kIRG_UKSource(U'\U000301B1'), std::string{"UK-01313"});
    }

    TEST_CASE("kIRG_USource") {
        REQUIRE_EQ(kIRG_USource(U'\U0002B88A'), std::string{"UTC-00001"});
    }

    TEST_CASE("kIRG_VSource") {
        REQUIRE_EQ(kIRG_VSource(U'\U00020126'), std::string{"V0-3021"});
    }

    TEST_CASE("kJHJ") {
        REQUIRE_EQ(kJHJ(U'\U00020126'), nullptr);
    }

    TEST_CASE("kJIS0213") {
        REQUIRE_EQ(kJIS0213(U'\U0002000B'), std::string{"1,14,02"});
    }

    TEST_CASE("kJa") {
        REQUIRE_EQ(kJa(U'\u382F'), std::string{"2256"});
    }

    TEST_CASE("kJapaneseKun") {
        REQUIRE_EQ(kJapaneseKun(U'\u5440'), std::string{"A ATSU"});
    }

    TEST_CASE("kJapaneseOn") {
        REQUIRE_EQ(kJapaneseOn(U'\u9D76'), std::string{"A"});
    }

    TEST_CASE("kJinmeiyoKanji") {
        REQUIRE_EQ(kJinmeiyoKanji(U'\u4E11'), std::string{"2010"});
    }

    TEST_CASE("kJis0") {
        REQUIRE_EQ(kJis0(U'\u4EDD'), std::string{"0124"});
    }

    TEST_CASE("kJis1") {
        REQUIRE_EQ(kJis1(U'\u4E02'), std::string{"1601"});
    }

    TEST_CASE("kJoyoKanji") {
        REQUIRE_EQ(kJoyoKanji(U'\u4E00'), std::string{"2010"});
    }

    TEST_CASE("kKPS0") {
        REQUIRE_EQ(kKPS0(U'\u4E00'), std::string{"FCD6"});
    }

    TEST_CASE("kKPS1") {
        REQUIRE_EQ(kKPS1(U'\u4E0F'), std::string{"340A"});
    }

    TEST_CASE("kKSC0") {
        REQUIRE_EQ(kKSC0(U'\u4F3D'), std::string{"4201"});
    }

    TEST_CASE("kKangXi") {
        REQUIRE_EQ(kKangXi(U'\uF900'), std::string{"0000.900"});
    }

    TEST_CASE("kKarlgren") {
        REQUIRE_EQ(kKarlgren(U'\u963F'), std::string{"1"});
    }

    TEST_CASE("kKorean") {
        REQUIRE_EQ(kKorean(U'\u9EEF'), std::string{"AM"});
    }

    TEST_CASE("kKoreanEducationHanja") {
        REQUIRE_EQ(kKoreanEducationHanja(U'\u4E01'), std::string{"2007"});
    }

    TEST_CASE("kKoreanName") {
        REQUIRE_EQ(kKoreanName(U'\u9DA0'), std::string{"2018"});
    }

    TEST_CASE("kLau") {
        REQUIRE_EQ(kLau(U'\u4E2B'), std::string{"1"});
    }

    TEST_CASE("kMainlandTelegraph") {
        REQUIRE_EQ(kMainlandTelegraph(U'\u4E00'), std::string{"0001"});
    }

    TEST_CASE("kMandarin") {
        REQUIRE_EQ(kMandarin(U'\u554A'), std::u8string{u8"a"});
    }

    TEST_CASE("kMatthews") {
        REQUIRE_EQ(kMatthews(U'\u963F'), std::string{"1"});
    }

    TEST_CASE("kMeyerWempe") {
        REQUIRE_EQ(kMeyerWempe(U'\u4E2B'), std::string{"1 1996d 1997"});
    }

    TEST_CASE("kMorohashi") {
        REQUIRE_EQ(kMorohashi(U'\u4E00'), std::string{"00001"});
    }

    TEST_CASE("kNelson") {
        REQUIRE_EQ(kNelson(U'\u4E00'), std::string{"0001"});
    }

    TEST_CASE("kOtherNumeric") {
        REQUIRE_EQ(kOtherNumeric(U'\u5E7A'), std::string{"1"});
    }

    TEST_CASE("kPhonetic") {
        REQUIRE_EQ(kPhonetic(U'\u4E2B'), std::string{"1"});
    }

    TEST_CASE("kPrimaryNumeric") {
        REQUIRE_EQ(kPrimaryNumeric(U'\u96F6'), std::string{"0"});
    }

    TEST_CASE("kPseudoGB1") {
        REQUIRE_EQ(kPseudoGB1(U'\u66F1'), std::string{"9201"});
    }

    TEST_CASE("kRSAdobe_Japan1_6") {
        REQUIRE_EQ(kRSAdobe_Japan1_6(U'\u4E9C'), std::string{"C+1125+7.2.5"});
    }

    TEST_CASE("kRSJapanese") {
        REQUIRE_EQ(kRSJapanese(U'\u4E9C'), nullptr);
    }

    TEST_CASE("kRSKanWa") {
        REQUIRE_EQ(kRSKanWa(U'\u4E9C'), nullptr);
    }

    TEST_CASE("kRSKangXi") {
        REQUIRE_EQ(kRSKangXi(U'\u4E00'), std::string{"1.0"});
    }

    TEST_CASE("kRSMerged") {
        REQUIRE_EQ(kRSMerged(U'\u4E00'), nullptr);
    }

    TEST_CASE("kRSUnicode") {
        REQUIRE_EQ(kRSUnicode(U'\u4E00'), "1.0");
    }

    TEST_CASE("kSBGY") {
        REQUIRE_EQ(kSBGY(U'\u6771'), "022.01");
    }

    TEST_CASE("kSpecializedSemanticVariant") {
        REQUIRE_EQ(kSpecializedSemanticVariant(U'\u534C'), "U+2099C");
    }

    TEST_CASE("kSemanticVariant") {
        REQUIRE_EQ(kSemanticVariant(U'\u4F5C'), "U+20228");
    }

    TEST_CASE("kSimplifiedVariant") {
        REQUIRE_EQ(kSimplifiedVariant(U'\u661C'), "U+200D3");
    }

    TEST_CASE("kSpoofingVariant") {
        REQUIRE_EQ(kSpoofingVariant(U'\u4E86'), "U+20110");
    }

    TEST_CASE("kStrange") {
        REQUIRE_EQ(kStrange(U'\u4E02'), "B:U+310E");
    }

    TEST_CASE("kTGH") {
        REQUIRE_EQ(kTGH(U'\u4E00'), "2013:1");
    }

    TEST_CASE("kTGHZ2013") {
        REQUIRE_EQ(kTGHZ2013(U'\u5416'), std::u8string{u8"001.010:ā"});
    }

    TEST_CASE("kTaiwanTelegraph") {
        REQUIRE_EQ(kTaiwanTelegraph(U'\u4E00'), std::string{"0001"});
    }

    TEST_CASE("kTotalStrokes") {
        REQUIRE_EQ(kTotalStrokes(U'\u4E85'), std::string{"1"});
    }

    TEST_CASE("kTraditionalVariant") {
        REQUIRE_EQ(kTraditionalVariant(U'\U0002BDD8'), std::string{"U+20054"});
    }

    TEST_CASE("kUnihanCore2020") {
        REQUIRE_EQ(kUnihanCore2020(U'\u920E'), std::string{"HJM"});
    }

    TEST_CASE("kVietnamese") {
        REQUIRE_EQ(kVietnamese(U'\u4E2B'), std::u8string{u8"a"});
    }

    TEST_CASE("kXHC1983") {
        REQUIRE_EQ(kXHC1983(U'\u963F'), std::u8string{u8"0001.010:ā 0002.081:a 0282.010:ē"});
    }

    TEST_CASE("kXerox") {
        REQUIRE_EQ(kXerox(U'\uFA20'), std::string{"171:143"});
    }

    TEST_CASE("kZVariant") {
        REQUIRE_EQ(kZVariant(U'\u58EF'), std::string{"U+2125F"});
    }
}