#include "doctest.h"
#include "db/ch/basic.h"
#include "db/ch/lookup.h"
#include "layers/base.h"

using namespace unicode::db::ch;
using namespace unicode::layers;
using namespace unicode::layers::base;

TEST_SUITE("[ch][basic][data]") {
    static const auto base = base_unicode_layer(); // NOLINT(cert-err58-cpp)
    
    TEST_CASE("is_alphabetic") {
        REQUIRE(is_alphabetic(U'a'));
        REQUIRE(base->is_alphabetic(U'a'));
        REQUIRE(base->is_alphabetic(U'b'));

        auto o = CharacterLayer{base}.override_is_alphabetic([](char32_t c){ return c == U'a';});
        REQUIRE(o.is_alphabetic(U'a').value());
        REQUIRE_FALSE(o.is_alphabetic(U'b').value());
    }

    TEST_CASE("is_ascii_hex") {
        REQUIRE(is_ascii_hex(U'0'));
        REQUIRE(base->is_ascii_hex(U'a'));
    }

    TEST_CASE("is_bidi_control") {
        auto o = CharacterLayer{base}.override_is_bidi_control([](char32_t){return false;});
        for (auto ch : all_of_bidi_control()) {
            REQUIRE(is_bidi_control(ch));
            REQUIRE(base->is_bidi_control(ch));
            REQUIRE(o.is_bidi_control(ch));
        }
    }

    TEST_CASE("is_bidi_mirrored") {
        for (auto ch : all_of_bidi_mirrored()) {
            REQUIRE(is_bidi_mirrored(ch));
            REQUIRE(base->is_bidi_mirrored(ch));
        }
    }

    TEST_CASE("is_case_ignorable") {
        for (auto ch : all_of_case_ignorable()) {
            REQUIRE(is_case_ignorable(ch));
            REQUIRE(base->is_case_ignorable(ch));
        }
    }

    TEST_CASE("is_cased") {
        REQUIRE(is_cased(U'a'));
        REQUIRE(base->is_cased(U'a'));
    }

    TEST_CASE("is_changed_when_case_folded") {
        for (auto ch : all_of_changed_when_case_folded()) {
            REQUIRE(is_changed_when_case_folded(ch));
            REQUIRE(base->is_changed_when_case_folded(ch));
        }
    }

    TEST_CASE("is_changed_when_case_mapped") {
        REQUIRE(is_changed_when_case_mapped(U'a'));
        REQUIRE(base->is_changed_when_case_mapped(U'a'));
    }

    TEST_CASE("is_changed_when_lower_cased") {
        for (auto ch : all_of_changed_when_lower_cased()) {
            REQUIRE(is_changed_when_lower_cased(ch));
            REQUIRE(base->is_changed_when_lower_cased(ch));
        }
    }

    TEST_CASE("is_changed_when_nfkc_case_folded") {
        REQUIRE(is_changed_when_nfkc_case_folded(U'\uA640'));
        REQUIRE(base->is_changed_when_nfkc_case_folded(U'\uA640'));
    }

    TEST_CASE("is_changed_when_title_cased") {
        for (auto ch : all_of_changed_when_title_cased()) {
            REQUIRE(is_changed_when_title_cased(ch));
            REQUIRE(base->is_changed_when_title_cased(ch));
        }
    }

    TEST_CASE("is_changed_when_upper_cased") {
        for (auto ch : all_of_changed_when_upper_cased()) {
            REQUIRE(is_changed_when_upper_cased(ch));
            REQUIRE(base->is_changed_when_upper_cased(ch));
        }
    }

    TEST_CASE("is_composition_exclusion") {
        for (auto ch : all_of_composition_exclusion()) {
            REQUIRE(is_composition_exclusion(ch));
            REQUIRE(base->is_composition_exclusion(ch));
        }
    }

    TEST_CASE("is_dash") {
        for (auto ch : all_of_dash()) {
            REQUIRE(is_dash(ch));
            REQUIRE(base->is_dash(ch));
        }
    }

    TEST_CASE("is_default_ignorable_code_point") {
        for (auto ch : all_of_default_ignorable_code_point()) {
            REQUIRE(is_default_ignorable_code_point(ch));
            REQUIRE(base->is_default_ignorable_code_point(ch));
        }
    }

    TEST_CASE("is_deprecated") {
        for (auto ch : all_of_deprecated()) {
            REQUIRE(is_deprecated(ch));
            REQUIRE(base->is_deprecated(ch));
        }
    }

    TEST_CASE("is_diacritic") {
        for (auto ch : all_of_diacritic()) {
            REQUIRE(is_diacritic(ch));
            REQUIRE(base->is_diacritic(ch));
        }
    }

    TEST_CASE("is_emoji") {
        for (auto ch : all_of_emoji()) {
            REQUIRE(is_emoji(ch));
            REQUIRE(base->is_emoji(ch));
        }
    }

    TEST_CASE("is_emoji_component") {
        for (auto ch : all_of_emoji_component()) {
            REQUIRE(is_emoji_component(ch));
            REQUIRE(base->is_emoji_component(ch));
        }
    }

    TEST_CASE("is_emoji_modifier") {
        for (auto ch : all_of_emoji_modifier()) {
            REQUIRE(is_emoji_modifier(ch));
            REQUIRE(base->is_emoji_modifier(ch));
        }
    }

    TEST_CASE("is_emoji_modifier_base") {
        for (auto ch : all_of_emoji_modifier_base()) {
            REQUIRE(is_emoji_modifier_base(ch));
            REQUIRE(base->is_emoji_modifier_base(ch));
        }
    }

    TEST_CASE("is_emoji_presentation") {
        for (auto ch : all_of_emoji_presentation()) {
            REQUIRE(is_emoji_presentation(ch));
            REQUIRE(base->is_emoji_presentation(ch));
        }
    }

    TEST_CASE("is_expand_on_nfc") {
        for (auto ch : all_of_expand_on_nfc()) {
            REQUIRE(is_expand_on_nfc(ch));
            REQUIRE(base->is_expand_on_nfc(ch));
        }
    }

    TEST_CASE("is_expand_on_nfd") {
        for (auto ch : all_of_expand_on_nfd()) {
            REQUIRE(is_expand_on_nfd(ch));
            REQUIRE(base->is_expand_on_nfd(ch));
        }
    }

    TEST_CASE("is_expand_on_nfkc") {
        for (auto ch : all_of_expand_on_nfkc()) {
            REQUIRE(is_expand_on_nfkc(ch));
            REQUIRE(base->is_expand_on_nfkc(ch));
        }
    }

    TEST_CASE("is_expand_on_nfkd") {
        for (auto ch : all_of_expand_on_nfkd()) {
            REQUIRE(is_expand_on_nfkd(ch));
            REQUIRE(base->is_expand_on_nfkd(ch));
        }
    }

    TEST_CASE("is_extended_identifier_continue") {
        REQUIRE(is_extended_identifier_continue(U'\uFA02'));
        REQUIRE(base->is_extended_identifier_continue(U'\uFA02'));
    }

    TEST_CASE("is_extended_identifier_start") {
        REQUIRE(is_extended_identifier_start(U'A'));
        REQUIRE(base->is_extended_identifier_start(U'A'));
    }

    TEST_CASE("is_extended_pictograph") {
        for (auto ch : all_of_extended_pictograph()) {
            REQUIRE(is_extended_pictograph(ch));
            REQUIRE(base->is_extended_pictograph(ch));
        }
    }

    TEST_CASE("is_extender") {
        for (auto ch : all_of_extender()) {
            REQUIRE(is_extender(ch));
            REQUIRE(base->is_extender(ch));
        }
    }

    TEST_CASE("is_full_composition_exclusion") {
        for (auto ch : all_of_full_composition_exclusion()) {
            REQUIRE(is_full_composition_exclusion(ch));
            REQUIRE(base->is_full_composition_exclusion(ch));
        }
    }

    TEST_CASE("is_grapheme_base") {
        REQUIRE(is_grapheme_base(U'\uFA0D'));
        REQUIRE(base->is_grapheme_base(U'\uFA0D'));
    }

    TEST_CASE("is_grapheme_extend") {
        for (auto ch : all_of_grapheme_extend()) {
            REQUIRE(is_grapheme_extend(ch));
            REQUIRE(base->is_grapheme_extend(ch));
        }
    }

    TEST_CASE("is_grapheme_link") {
        for (auto ch : all_of_grapheme_link()) {
            REQUIRE(is_grapheme_link(ch));
            REQUIRE(base->is_grapheme_link(ch));
        }
    }

    TEST_CASE("is_hex") {
        for (auto ch : all_of_hex()) {
            REQUIRE(is_hex(ch));
            REQUIRE(base->is_hex(ch));
        }
    }

    TEST_CASE("is_hyphen") {
        for (auto ch : all_of_hyphen()) {
            REQUIRE(is_hyphen(ch));
            REQUIRE(base->is_hyphen(ch));
        }
    }

    TEST_CASE("is_identifier_continue") {
        REQUIRE(is_identifier_continue(U'\uFA02'));
        REQUIRE(base->is_identifier_continue(U'\uFA02'));
    }

    TEST_CASE("is_identifier_start") {
        REQUIRE(is_identifier_start(U'\uFA02'));
        REQUIRE(base->is_identifier_start(U'\uFA02'));
    }

    TEST_CASE("is_ideographic") {
        REQUIRE(is_ideographic(U'\uFA02'));
        REQUIRE(base->is_ideographic(U'\uFA02'));
    }

    TEST_CASE("is_ids_binary_operator") {
        for (auto ch : all_of_ids_binary_operator()) {
            REQUIRE(is_ids_binary_operator(ch));
            REQUIRE(base->is_ids_binary_operator(ch));
        }
    }

    TEST_CASE("is_ids_trinary_operator") {
        for (auto ch : all_of_ids_trinary_operator()) {
            REQUIRE(is_ids_trinary_operator(ch));
            REQUIRE(base->is_ids_trinary_operator(ch));
        }
    }

    TEST_CASE("is_join_control") {
        for (auto ch : all_of_join_control()) {
            REQUIRE(is_join_control(ch));
            REQUIRE(base->is_join_control(ch));
        }
    }

    TEST_CASE("is_logical_order_exception") {
        for (auto ch : all_of_logical_order_exception()) {
            REQUIRE(is_logical_order_exception(ch));
            REQUIRE(base->is_logical_order_exception(ch));
        }
    }

    TEST_CASE("is_lower") {
        for (auto ch : all_of_lower()) {
            REQUIRE(is_lower(ch));
            REQUIRE(base->is_lower(ch));
        }
    }

    TEST_CASE("is_math") {
        for (auto ch : all_of_math()) {
            REQUIRE(is_math(ch));
            REQUIRE(base->is_math(ch));
        }
    }

    TEST_CASE("is_non_character_code_point") {
        for (auto ch : all_of_non_character_code_point()) {
            REQUIRE(is_non_character_code_point(ch));
            REQUIRE(base->is_non_character_code_point(ch));
        }
    }

    TEST_CASE("is_other_alphabetic") {
        for (auto ch : all_of_other_alphabetic()) {
            REQUIRE(is_other_alphabetic(ch));
            REQUIRE(base->is_other_alphabetic(ch));
        }
    }

    TEST_CASE("is_other_default_ignorable_code_point") {
        for (auto ch : all_of_other_default_ignorable_code_point()) {
            REQUIRE(is_other_default_ignorable_code_point(ch));
            REQUIRE(base->is_other_default_ignorable_code_point(ch));
        }
    }

    TEST_CASE("is_other_grapheme_extend") {
        for (auto ch : all_of_other_grapheme_extend()) {
            REQUIRE(is_other_grapheme_extend(ch));
            REQUIRE(base->is_other_grapheme_extend(ch));
        }
    }

    TEST_CASE("is_other_identifier_continue") {
        for (auto ch : all_of_other_identifier_continue()) {
            REQUIRE(is_other_identifier_continue(ch));
            REQUIRE(base->is_other_identifier_continue(ch));
        }
    }

    TEST_CASE("is_other_identifier_start") {
        for (auto ch : all_of_other_identifier_start()) {
            REQUIRE(is_other_identifier_start(ch));
            REQUIRE(base->is_other_identifier_start(ch));
        }
    }

    TEST_CASE("is_other_lower") {
        for (auto ch : all_of_other_lower()) {
            REQUIRE(is_other_lower(ch));
            REQUIRE(base->is_other_lower(ch));
        }
    }

    TEST_CASE("is_other_math") {
        for (auto ch : all_of_other_math()) {
            REQUIRE(is_other_math(ch));
            REQUIRE(base->is_other_math(ch));
        }
    }

    TEST_CASE("is_other_upper") {
        for (auto ch : all_of_other_upper()) {
            REQUIRE(is_other_upper(ch));
            REQUIRE(base->is_other_upper(ch));
        }
    }

    TEST_CASE("is_pattern_syntax") {
        for (auto ch : all_of_pattern_syntax()) {
            REQUIRE(is_pattern_syntax(ch));
            REQUIRE(base->is_pattern_syntax(ch));
        }
    }

    TEST_CASE("is_pattern_whitespace") {
        for (auto ch : all_of_pattern_whitespace()) {
            REQUIRE(is_pattern_whitespace(ch));
            REQUIRE(base->is_pattern_whitespace(ch));
        }
    }

    TEST_CASE("is_prepended_concatenation_mark") {
        for (auto ch : all_of_prepended_concatenation_mark()) {
            REQUIRE(is_prepended_concatenation_mark(ch));
            REQUIRE(base->is_prepended_concatenation_mark(ch));
        }
    }

    TEST_CASE("is_quotation_mark") {
        for (auto ch : all_of_quotation_mark()) {
            REQUIRE(is_quotation_mark(ch));
            REQUIRE(base->is_quotation_mark(ch));
        }
    }

    TEST_CASE("is_radical") {
        for (auto ch : all_of_radical()) {
            REQUIRE(is_radical(ch));
            REQUIRE(base->is_radical(ch));
        }
    }

    TEST_CASE("is_regional_indicator") {
        for (auto ch : all_of_regional_indicator()) {
            REQUIRE(is_regional_indicator(ch));
            REQUIRE(base->is_regional_indicator(ch));
        }
    }

    TEST_CASE("is_sentence_terminal") {
        for (auto ch : all_of_sentence_terminal()) {
            REQUIRE(is_sentence_terminal(ch));
            REQUIRE(base->is_sentence_terminal(ch));
        }
    }

    TEST_CASE("is_soft_dotted") {
        for (auto ch : all_of_soft_dotted()) {
            REQUIRE(is_soft_dotted(ch));
            REQUIRE(base->is_soft_dotted(ch));
        }
    }

    TEST_CASE("is_terminal_punctuation") {
        for (auto ch : all_of_terminal_punctuation()) {
            REQUIRE(is_terminal_punctuation(ch));
            REQUIRE(base->is_terminal_punctuation(ch));
        }
    }

    TEST_CASE("is_unified_ideograph") {
        REQUIRE(is_unified_ideograph(U'\uFA0E'));
        REQUIRE(base->is_unified_ideograph(U'\uFA0E'));
    }

    TEST_CASE("is_upper") {
        for (auto ch : all_of_upper()) {
            REQUIRE(is_upper(ch));
            REQUIRE(base->is_upper(ch));
        }
    }

    TEST_CASE("is_variation_selector") {
        for (auto ch : all_of_variation_selector()) {
            REQUIRE(is_variation_selector(ch));
            REQUIRE(base->is_variation_selector(ch));
        }
    }

    TEST_CASE("is_white_space") {
        for (auto ch : all_of_white_space()) {
            REQUIRE(is_white_space(ch));
            REQUIRE(base->is_white_space(ch));
        }
    }
}
