#include "doctest.h"
#include <db/ch/misc.h>

TEST_SUITE("[ch][misc][data]") {
    TEST_CASE("east_asian_width") {
        REQUIRE_EQ(unicode::db::ch::east_asian_width(U'\uFFFD'), unicode::db::ch::EastAsianWidth::A);
    }

    TEST_CASE("hangul_syllable_type") {
        REQUIRE_EQ(unicode::db::ch::hangul_syllable_type(U'\uA97B'), unicode::db::ch::HangulSyllableType::L);
    }

    TEST_CASE("jamo_short_name") {
        REQUIRE_EQ(unicode::db::ch::jamo_short_name(U'\u1100'), std::string{"G"});
    }

    TEST_CASE("equivalent_unified_ideograph") {
        REQUIRE_EQ(unicode::db::ch::equivalent_unified_ideograph(U'\u2E82'), U'\u4E5B');
    }
}