#include "doctest.h"
#include "db/ch/nushu.h"

using namespace unicode::db::ch;

TEST_SUITE("[ch][nushu][data]") {
    TEST_CASE("kSrc_NushuDuben") {
        REQUIRE_EQ(unicode::db::ch::kSrc_NushuDuben(U'\U0001B170'), std::string{"36.01"});
    }

    TEST_CASE("kReading") {
        REQUIRE_EQ(unicode::db::ch::kReading(U'\U0001B170'), std::string{"i5"});
    }
}