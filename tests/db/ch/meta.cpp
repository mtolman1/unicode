#include "doctest.h"
#include <db/ch/meta.h>
#include "layers/base.h"

TEST_SUITE("[ch][meta][data]") {
    static const auto base = unicode::layers::base::base_unicode_layer(); // NOLINT(cert-err58-cpp)

    TEST_CASE("age") {
        REQUIRE_EQ(unicode::db::ch::age(U'a'), std::string{"1.1"});
        REQUIRE_EQ(base->age(U'a'), std::string{"1.1"});
    }

    TEST_CASE("aliases") {
        REQUIRE_EQ(unicode::db::ch::aliases(U'\u0020')[0].alias, std::string{"SP"});
        REQUIRE_EQ(unicode::db::ch::aliases(U'\u0020')[0].type, unicode::db::ch::AliasType::ABBREVIATION);
        REQUIRE_EQ(base->aliases(U'\u0020').value()[unicode::db::ch::AliasType::ABBREVIATION][0], std::string{"SP"});
    }

    TEST_CASE("block") {
        REQUIRE_EQ(unicode::db::ch::block(U'a'), unicode::db::ch::Block::ASCII);
        REQUIRE_EQ(base->block(U'a'), unicode::db::ch::Block::ASCII);
    }

    TEST_CASE("name") {
        REQUIRE_EQ(unicode::db::ch::name(U'a'), std::string{"LATIN SMALL LETTER A"});
        REQUIRE_EQ(base->name(U'a'), std::string{"LATIN SMALL LETTER A"});
    }

    TEST_CASE("general_category") {
        REQUIRE_EQ(unicode::db::ch::general_category(U'a'), unicode::db::ch::GeneralCategory::LL);
        REQUIRE_EQ(base->general_category(U'a'), unicode::db::ch::GeneralCategory::LL);
    }

    TEST_CASE("chars_by_block") {
        REQUIRE_EQ(unicode::db::ch::chars_by_block(unicode::db::ch::Block::ASCII).size(), 128);
    }

    TEST_CASE("chars_by_block") {
        REQUIRE_GE(unicode::db::ch::chars_by_general_category(unicode::db::ch::GeneralCategory::LL).size(), 2233);
    }

    TEST_CASE("char_by_name") {
        REQUIRE_EQ(unicode::db::ch::char_by_name("LATIN SMALL LETTER A"), U'a');
    }
}