#include "doctest.h"
#include <db/ch/break.h>
#include "layers/base.h"

TEST_SUITE("[ch][break][data]") {
    static const auto base = unicode::layers::base::base_unicode_layer(); // NOLINT(cert-err58-cpp)

    TEST_CASE("grapheme_cluster_break") {
        REQUIRE_EQ(unicode::db::ch::grapheme_cluster_break(U'\U0000FE59'), unicode::db::ch::GraphemeClusterBreak::XX);
        REQUIRE_EQ(unicode::db::ch::grapheme_cluster_break(U'\U0000FEFF'), unicode::db::ch::GraphemeClusterBreak::CN);
        REQUIRE_EQ(base->grapheme_cluster_break(U'\U0000FE59'), unicode::db::ch::GraphemeClusterBreak::XX);
        REQUIRE_EQ(base->grapheme_cluster_break(U'\U0000FEFF'), unicode::db::ch::GraphemeClusterBreak::CN);
    }

    TEST_CASE("line_break") {
        REQUIRE_EQ(unicode::db::ch::line_break(U'\U0000FFFD'), unicode::db::ch::LineBreak::AI);
        REQUIRE_EQ(unicode::db::ch::line_break(U'\U00011F45'), unicode::db::ch::LineBreak::ID);
        REQUIRE_EQ(base->line_break(U'\U0000FFFD'), unicode::db::ch::LineBreak::AI);
        REQUIRE_EQ(base->line_break(U'\U00011F45'), unicode::db::ch::LineBreak::ID);
    }

    TEST_CASE("sentence_break") {
        REQUIRE_EQ(unicode::db::ch::sentence_break(U'\U0001FBF0'), unicode::db::ch::SentenceBreak::NU);
        REQUIRE_EQ(unicode::db::ch::sentence_break(U'\U0001F676'), unicode::db::ch::SentenceBreak::CL);
        REQUIRE_EQ(base->sentence_break(U'\U0001FBF0'), unicode::db::ch::SentenceBreak::NU);
        REQUIRE_EQ(base->sentence_break(U'\U0001F676'), unicode::db::ch::SentenceBreak::CL);
    }

    TEST_CASE("word_break") {
        REQUIRE_EQ(unicode::db::ch::word_break(U'\U0000000D'), unicode::db::ch::WordBreak::CR);
        REQUIRE_EQ(unicode::db::ch::word_break(U'\U00000300'), unicode::db::ch::WordBreak::EXTEND);
        REQUIRE_EQ(base->word_break(U'\U0000000D'), unicode::db::ch::WordBreak::CR);
        REQUIRE_EQ(base->word_break(U'\U00000300'), unicode::db::ch::WordBreak::EXTEND);
    }
}