#include "doctest.h"
#include "db/ch/normalize.h"

using namespace unicode::db::ch;

TEST_SUITE("[ch][normalize][data]") {
    TEST_CASE("decomposition_mapping") {
        REQUIRE_EQ(unicode::db::ch::decomposition_mapping(U'\u00A0'), std::u32string{U"\u0020"});
    }

    TEST_CASE("decomposition_type") {
        REQUIRE_EQ(unicode::db::ch::decomposition_type(U'\u00A0'), unicode::db::ch::DecompositionType::NB);
    }

    TEST_CASE("fc_nfkc_closure") {
        REQUIRE_EQ(unicode::db::ch::fc_nfkc_closure(U'\U0001D6BE'), std::u32string{U"\U000003C7"});
    }

    TEST_CASE("nfkc_case_fold") {
        REQUIRE_EQ(unicode::db::ch::nfkc_case_fold(U'\u327C'), std::u32string{U"\U0000CC38\U0000ACE0"});
    }

    TEST_CASE("nfc_quick_check") {
        REQUIRE_EQ(unicode::db::ch::nfc_quick_check(U'\u0653'), unicode::db::ch::NFC_QC::M);
    }

    TEST_CASE("nfc_quick_check") {
        REQUIRE_EQ(unicode::db::ch::nfkc_quick_check(U'\u0653'), unicode::db::ch::NFKC_QC::M);
    }

    TEST_CASE("nfd_quick_check") {
        REQUIRE_EQ(unicode::db::ch::nfd_quick_check(U'\u0653'), unicode::db::ch::NFD_QC::Y);
    }

    TEST_CASE("nfkd_quick_check") {
        REQUIRE_EQ(unicode::db::ch::nfkd_quick_check(U'\u0653'), unicode::db::ch::NFKD_QC::Y);
    }
}