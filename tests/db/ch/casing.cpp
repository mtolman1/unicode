#include "doctest.h"
#include <db/ch/casing.h>
#include "layers/base.h"

TEST_SUITE("[ch][casing][data]") {
    static const auto base = unicode::layers::base::base_unicode_layer(); // NOLINT(cert-err58-cpp)

    TEST_CASE("simple_case_folding") {
        REQUIRE_EQ(unicode::db::ch::simple_case_folding(U'\U0000024C'), U'\U0000024D');
        REQUIRE_EQ(unicode::db::ch::simple_case_folding(U'\U0000024D'), U'\U0000024D');
        REQUIRE_EQ(base->simple_case_folding(U'\U0000024C'), U'\U0000024D');
        REQUIRE_EQ(base->simple_case_folding(U'\U0000024D'), U'\U0000024D');
    }

    TEST_CASE("simple_lower_case") {
        REQUIRE_EQ(unicode::db::ch::simple_lower_case(U'\U0000024E'), U'\U0000024F');
        REQUIRE_EQ(unicode::db::ch::simple_lower_case(U'\U0000024F'), U'\U0000024F');
        REQUIRE_EQ(base->simple_lower_case(U'\U0000024E'), U'\U0000024F');
        REQUIRE_EQ(base->simple_lower_case(U'\U0000024F'), U'\U0000024F');
    }

    TEST_CASE("simple_title_case") {
        REQUIRE_EQ(unicode::db::ch::simple_title_case(U'\U0000024D'), U'\U0000024C');
        REQUIRE_EQ(unicode::db::ch::simple_title_case(U'\U0000024C'), U'\U0000024C');
        REQUIRE_EQ(base->simple_title_case(U'\U0000024D'), U'\U0000024C');
        REQUIRE_EQ(base->simple_title_case(U'\U0000024C'), U'\U0000024C');
    }

    TEST_CASE("simple_upper_case") {
        REQUIRE_EQ(unicode::db::ch::simple_upper_case(U'\U0000024B'), U'\U0000024A');
        REQUIRE_EQ(unicode::db::ch::simple_upper_case(U'\U0000024A'), U'\U0000024A');
        REQUIRE_EQ(base->simple_upper_case(U'\U0000024B'), U'\U0000024A');
        REQUIRE_EQ(base->simple_upper_case(U'\U0000024A'), U'\U0000024A');
    }

    TEST_CASE("case_folding") {
        REQUIRE_EQ(unicode::db::ch::case_folding(U'\U0000024C'), std::u32string{U"\U0000024D"});
        REQUIRE_EQ(unicode::db::ch::case_folding(U'\U0000024D'), std::u32string{U"\U0000024D"});
        REQUIRE_EQ(base->case_folding(U'\U0000024C'), std::u32string{U"\U0000024D"});
        REQUIRE_EQ(base->case_folding(U'\U0000024D'), std::u32string{U"\U0000024D"});
    }

    TEST_CASE("lower_case") {
        REQUIRE_EQ(unicode::db::ch::lower_case(U'\U0000024E'), std::u32string{U"\U0000024F"});
        REQUIRE_EQ(unicode::db::ch::lower_case(U'\U0000024F'), std::u32string{U"\U0000024F"});
        REQUIRE_EQ(base->lower_case(U'\U0000024E'), std::u32string{U"\U0000024F"});
        REQUIRE_EQ(base->lower_case(U'\U0000024F'), std::u32string{U"\U0000024F"});
    }

    TEST_CASE("title_case") {
        REQUIRE_EQ(unicode::db::ch::title_case(U'\U0000024D'), std::u32string{U"\U0000024C"});
        REQUIRE_EQ(unicode::db::ch::title_case(U'\U0000024C'), std::u32string{U"\U0000024C"});
        REQUIRE_EQ(base->title_case(U'\U0000024D'), std::u32string{U"\U0000024C"});
        REQUIRE_EQ(base->title_case(U'\U0000024C'), std::u32string{U"\U0000024C"});
    }

    TEST_CASE("upper_case") {
        REQUIRE_EQ(unicode::db::ch::upper_case(U'\U0000024B'), std::u32string{U"\U0000024A"});
        REQUIRE_EQ(unicode::db::ch::upper_case(U'\U0000024A'), std::u32string{U"\U0000024A"});
        REQUIRE_EQ(base->upper_case(U'\U0000024B'), std::u32string{U"\U0000024A"});
        REQUIRE_EQ(base->upper_case(U'\U0000024A'), std::u32string{U"\U0000024A"});
    }
}