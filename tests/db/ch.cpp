#include "doctest.h"
#include <db/ch.h>

TEST_SUITE("[char][props]") {
    TEST_CASE("[construct][a]") {
        auto props = unicode::db::ch::props(U'a');
        CHECK_EQ(std::string{"1.1"}, std::string{props.age});
        CHECK_EQ(std::string{props.name}, std::string{"LATIN SMALL LETTER A"});
        CHECK_EQ(props.aliases.size(), 0);
        CHECK_EQ(props.block, unicode::db::ch::Block::ASCII);
        CHECK_EQ(props.generalCategory, unicode::db::ch::GeneralCategory::LL);
    }
}
