#include "doctest.h"
#include <db/blocks.h>

TEST_SUITE("[blocks]") {
    TEST_CASE("[block_by_id]") {
        REQUIRE_EQ(unicode::db::block::block_by_id(0)->name, std::string{"Basic Latin"});
        REQUIRE_FALSE(unicode::db::block::block_by_id(1998097878));
    }

    TEST_CASE("[block]") {
        REQUIRE_EQ(unicode::db::block::block_for(U'\u0000')->name, std::string{"Basic Latin"});
        REQUIRE_EQ(unicode::db::block::block_for(U'\u007F')->name, std::string{"Basic Latin"});
        REQUIRE_EQ(unicode::db::block::block_for(U'\u004F')->name, std::string{"Basic Latin"});
        REQUIRE_EQ(unicode::db::block::block_for(U'\u0080')->name, std::string{"Latin-1 Supplement"});
        REQUIRE_EQ(unicode::db::block::block_for(U'\u17FE')->name, std::string{"Khmer"});
        REQUIRE_EQ(unicode::db::block::block_for(U'\U000119A0')->name, std::string{"Nandinagari"});
        REQUIRE_EQ(unicode::db::block::block_for(U'\U0010FFFF')->name, std::string{"Supplementary Private Use Area-B"});
        REQUIRE_FALSE(unicode::db::block::block_for(0x0011FFFF));
    }

    TEST_CASE("[blocks]") {
        REQUIRE_EQ(unicode::db::block::blocks().size(), 327);
    }
}