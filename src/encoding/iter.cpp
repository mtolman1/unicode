#include "encoding/iter.h"
#include "encoding/details/impl.h"
#include "encoding/byte_order.h"
#include "encoding/props.h"
#include <tuple>

using namespace unicode::encoding::details::impl;

// String to UTF 32 Iterator

unicode::encoding::iter::impl::StrToUtf32Iterator::StrToUtf32Iterator(const std::string_view &str, bool end)
        : strRef(str)
{
    if (end) {
        strIndex = strRef.size() + 1;
    }
    else {
        read_rune();
    }
}

auto unicode::encoding::iter::impl::StrToUtf32Iterator::read_rune() -> void {
    size_t curIndex = strIndex;
    char32_t currentRune = 0;
    if (strIndex == strRef.size()) {
        strIndex = strRef.size() + 1;
    }
    else if (strIndex < strRef.size()) {
        auto numBytes = num_bytes(strRef[curIndex]);
        if (numBytes < 0) {
            // If the character is corrupted, return a null character
            strIndex++;
        }
        else if (curIndex + numBytes - 1 >= strRef.size()) {
            // If the character goes past the end of the string, return a null character and jump to the end
            strIndex = strRef.size();
        }
        else {
            strIndex = curIndex + numBytes;
            for (size_t index = curIndex; index < strIndex; ++index) {
                auto flag = get_flag(strRef[index]);
                if (index > curIndex && flag != continueFlag) {
                    // Premature unicode character ending
                    strIndex = curIndex + index - 1;
                    currentRune = 0;
                    break;
                }
                auto noFlag = without_flag(strRef[index]);
                currentRune <<= 6;
                currentRune |= noFlag;
            }
        }
    }
    currentRunePos = std::make_tuple(currentRune, curIndex);
}

auto unicode::encoding::iter::impl::StrToUtf32Iterator::operator<=>(const StrToUtf32Iterator &o) const -> std::partial_ordering {
    if (o.strRef != strRef) {
        return std::partial_ordering::unordered;
    }
    return strIndex <=> o.strIndex;
}


// UTF 8 to UTF 32 Iterator

unicode::encoding::iter::impl::Utf8ToUtf32Iterator::Utf8ToUtf32Iterator(const std::u8string_view &str, bool end)
        : strRef(str)
{
    if (end) {
        strIndex = strRef.size() + 1;
    }
    else {
        read_rune();
    }
}

auto unicode::encoding::iter::impl::Utf8ToUtf32Iterator::read_rune() -> void {
    size_t curIndex = strIndex;
    char32_t currentRune = 0;
    if (strIndex == strRef.size()) {
        strIndex = strRef.size() + 1;
    }
    else if (strIndex < strRef.size()) {
        auto numBytes = num_bytes(strRef[curIndex]);
        if (numBytes < 0) {
            // If the character is corrupted, return a null character
            strIndex++;
        }
        else if (curIndex + numBytes - 1 >= strRef.size()) {
            // If the character goes past the end of the string, return a null character and jump to the end
            strIndex = strRef.size();
        }
        else {
            strIndex = curIndex + numBytes;
            for (size_t index = curIndex; index < strIndex; ++index) {
                auto flag = get_flag(strRef[index]);
                if (index > curIndex && flag != continueFlag) {
                    // Premature unicode character ending
                    strIndex = curIndex + index - 1;
                    currentRune = 0;
                    break;
                }
                auto noFlag = without_flag(strRef[index]);
                currentRune <<= 6;
                currentRune |= noFlag;
            }
        }
    }
    currentRunePos = std::make_tuple(currentRune, curIndex);
}

auto unicode::encoding::iter::impl::Utf8ToUtf32Iterator::operator<=>(const Utf8ToUtf32Iterator &o) const -> std::partial_ordering {
    if (o.strRef != strRef) {
        return std::partial_ordering::unordered;
    }
    return strIndex <=> o.strIndex;
}

// UTF 16 to UTF 32 Iterator

unicode::encoding::iter::Utf16RuneIterable::Utf16RuneIterable(std::u16string_view v)
    : strRef(v), reverse(!v.empty() && byte_order::detect_endian_ch16(v[0]).value_or(byte_order::endian::machine) != byte_order::endian::machine)
{}

unicode::encoding::iter::impl::Utf16ToUtf32Iterator::Utf16ToUtf32Iterator(const std::u16string_view &str, bool end, bool reverseBom)
        : strRef(str), reverseBom(reverseBom)
{
    if (end) {
        strIndex = strRef.size() + 1;
    }
    else {
        read_rune();
    }
}

auto unicode::encoding::iter::impl::Utf16ToUtf32Iterator::read_rune() -> void {
    size_t curIndex = strIndex;
    char32_t currentRune = 0;
    if (strIndex == strRef.size()) {
        strIndex = strRef.size() + 1;
    }
    else if (strIndex < strRef.size()) {
        auto ch = strRef[curIndex];
        if (reverseBom) {
            ch = encoding::byte_order::flip_endian_ch16(ch);
        }

        if (encoding::props::is_high_surrogate(ch)) {
            if (curIndex + 1 >= strRef.size()) {
                // If the character goes past the end of the string, return a null character and jump to the end
                strIndex = strRef.size();
            }
            else {
                auto chNext = strRef[curIndex + 1];
                if (reverseBom) {
                    chNext = encoding::byte_order::flip_endian_ch16(chNext);
                }

                auto high = (ch - utf16HighOffset) * 0x400;
                auto low = chNext - utf16LowOffset;
                currentRune = high + low + utf16Sub;
                strIndex = curIndex + 2;
            }
        }
        else {
            if (!encoding::props::is_low_surrogate(ch)) {
                // Only return valid characters
                currentRune = static_cast<char32_t>(ch);
            }
            ++strIndex;
        }
    }
    currentRunePos = std::make_tuple(currentRune, curIndex);
}

auto unicode::encoding::iter::impl::Utf16ToUtf32Iterator::operator<=>(const Utf16ToUtf32Iterator &o) const -> std::partial_ordering {
    if (o.strRef != strRef) {
        return std::partial_ordering::unordered;
    }
    return strIndex <=> o.strIndex;
}


// UTF 16 Endian Iterator

unicode::encoding::iter::Utf16EndianIterable::Utf16EndianIterable(std::u16string_view v)
        : strRef(v), reverse(!v.empty() && byte_order::detect_endian_ch16(v[0]).value_or(byte_order::endian::machine) != byte_order::endian::machine)
{}

unicode::encoding::iter::impl::Utf16EndianIterator::Utf16EndianIterator(const std::u16string_view &str, bool end, bool reverseBom)
        : strRef(str), reverseBom(reverseBom)
{
    if (end) {
        strIndex = strRef.size() + 1;
    }
    else {
        read_char();
    }
}

auto unicode::encoding::iter::impl::Utf16EndianIterator::read_char() -> void {
    if (strIndex == strRef.size()) {
        strIndex = strRef.size() + 1;
        currentChar = 0;
        return;
    }

    auto ch = strRef[strIndex];
    if (reverseBom) {
        ch = encoding::byte_order::flip_endian_ch16(ch);
    }
    currentChar = ch;
    ++strIndex;
}

auto unicode::encoding::iter::impl::Utf16EndianIterator::operator<=>(const Utf16EndianIterator &o) const -> std::partial_ordering {
    if (o.strRef != strRef) {
        return std::partial_ordering::unordered;
    }
    return strIndex <=> o.strIndex;
}


// UTF 16 Target Endian Iterator

unicode::encoding::iter::Utf16TargetEndianIterable::Utf16TargetEndianIterable(std::endian target, std::u16string_view v)
        : strRef(v), reverse(!v.empty() && byte_order::detect_endian_ch16(v[0]).value_or(byte_order::endian::machine) != target)
{}


// UTF 32 Endian Iterator

unicode::encoding::iter::Utf32EndianIterable::Utf32EndianIterable(std::u32string_view v)
        : strRef(v), reverse(!v.empty() && byte_order::detect_endian_ch32(v[0]).value_or(byte_order::endian::machine) != byte_order::endian::machine)
{}

unicode::encoding::iter::impl::Utf32EndianIterator::Utf32EndianIterator(const std::u32string_view &str, bool end, bool reverseBom)
        : strRef(str), reverseBom(reverseBom)
{
    if (end) {
        strIndex = strRef.size() + 1;
    }
    else {
        read_char();
    }
}

auto unicode::encoding::iter::impl::Utf32EndianIterator::read_char() -> void {
    if (strIndex == strRef.size()) {
        strIndex = strRef.size() + 1;
        currentChar = 0;
        return;
    }

    auto ch = strRef[strIndex];
    if (reverseBom) {
        ch = encoding::byte_order::flip_endian_ch32(ch);
    }
    currentChar = ch;
    ++strIndex;
}

auto unicode::encoding::iter::impl::Utf32EndianIterator::operator<=>(const Utf32EndianIterator &o) const -> std::partial_ordering {
    if (o.strRef != strRef) {
        return std::partial_ordering::unordered;
    }
    return strIndex <=> o.strIndex;
}


// UTF 32 Target Endian Iterator

unicode::encoding::iter::Utf32TargetEndianIterable::Utf32TargetEndianIterable(std::endian target, std::u32string_view v)
        : strRef(v), reverse(!v.empty() && byte_order::detect_endian_ch32(v[0]).value_or(byte_order::endian::machine) != target)
{}