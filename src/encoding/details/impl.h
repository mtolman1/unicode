#pragma once

#include <tuple>
#include <array>
#include <string>

namespace unicode::encoding::details::impl {
    constexpr uint8_t continueFlag = 0b10000000;

    constexpr auto utf8StartFlags = std::array<std::tuple<uint8_t, int, uint8_t>, 5> {
            std::make_tuple(0b11110000, 4, 0b00000111),
            std::make_tuple(0b11100000, 3, 0b00001111),
            std::make_tuple(0b11000000, 2, 0b00011111),
            std::make_tuple(continueFlag, -1, 0b00111111),
            std::make_tuple(0b00000000, 1, 0b01111111),
    };

    auto num_bytes(char8_t byte) -> int;
    auto without_flag(char8_t byte) -> uint8_t;
    auto get_flag(char8_t byte) -> uint8_t;

    template<typename Str>
    auto is_valid_utf8_char(const Str& str, size_t curIndex) -> std::tuple<bool, size_t> {
        if (curIndex > str.size()) {
            // If we're starting out of bounds, return false
            return std::make_tuple(false, curIndex);
        }
        auto numBytes = num_bytes(str[curIndex]);
        if (numBytes < 0 || curIndex + numBytes - 1 >= str.size()) {
            // If we find a "continue" character at a start character or
            // If the character goes past the end of the string, return a false
            return std::make_tuple(false, curIndex);
        }
        else {
            auto nextIndex = curIndex + numBytes;
            for (size_t index = curIndex; index < nextIndex; ++index) {
                auto flag = get_flag(str[index]);
                if (index > curIndex && flag != continueFlag) {
                    // Premature unicode character ending
                    return std::make_tuple(false, curIndex);
                }
            }
            return std::make_tuple(true, nextIndex);
        }
    }

    constexpr char16_t utf16HighOffset = 0xD800;
    constexpr char16_t utf16LowOffset = 0xDC00;
    constexpr char32_t utf16Sub = 0x10000;
}