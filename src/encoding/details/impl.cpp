#include "impl.h"

auto unicode::encoding::details::impl::num_bytes(char8_t byte) -> int {
    for (const auto [flagSet, bytes, _flag_removal] : utf8StartFlags) {
        if (byte >= flagSet) {
            return bytes;
        }
    }
    return -1;
}

auto unicode::encoding::details::impl::without_flag(char8_t byte) -> uint8_t {
    for (const auto [flagSet, _bytes, removeFlag] : utf8StartFlags) {
        if (byte >= flagSet) {
            return byte & removeFlag;
        }
    }
    return byte;
}

auto unicode::encoding::details::impl::get_flag(char8_t byte) -> uint8_t {
    for (const auto [flagSet, _bytes, _removeFlag] : utf8StartFlags) {
        if (byte >= flagSet) {
            return flagSet;
        }
    }
    return 0;
}
