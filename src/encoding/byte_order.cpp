#include "encoding/byte_order.h"

auto unicode::encoding::byte_order::flip_endian_ch16(char16_t ch) -> char16_t {
    return ch << 8 | ((ch >> 8) & 0xFF);
}

auto unicode::encoding::byte_order::flip_endian_ch32(char32_t ch) -> char32_t {
    return ch << 24
           | ((ch << 8) & 0xFF0000)
           | ((ch >> 8) & 0xFF00)
           | ((ch >> 24) & 0xFF);
}

auto unicode::encoding::byte_order::flip_endian(std::u16string& str) -> void {
    for (size_t i = 0; i < str.size(); ++i) {
        str[i] = flip_endian_ch16(str[i]);
    }
}


auto unicode::encoding::byte_order::flip_endian(std::u32string& str) -> void {
    for (size_t i = 0; i < str.size(); ++i) {
        str[i] = flip_endian_ch32(str[i]);
    }
}

auto unicode::encoding::byte_order::add_bom(std::u16string& str) -> void {
    if (is_bom(str[0])) {
        return;
    }
    str = u"\uFEFF" + str;
}

auto unicode::encoding::byte_order::add_bom(std::u32string& str) -> void {
    if (is_bom(str[0])) {
        return;
    }
    str = U"\uFEFF" + str;
}

auto unicode::encoding::byte_order::remove_bom(std::u16string& str) -> void {
    if (!is_bom(str[0])) {
        return;
    }
    str.erase(0, 1);
}

auto unicode::encoding::byte_order::remove_bom(std::u32string& str) -> void {
    if (!is_bom(str[0])) {
        return;
    }
    str.erase(0, 1);
}
