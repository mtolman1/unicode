#include "encoding/validate.h"
#include "encoding/props.h"
#include "encoding/details/impl.h"
#include <tuple>
#include "encoding/convert.h"
#include <span>

auto unicode::encoding::validate::is_valid_utf8(const std::string &str) -> bool {
    using namespace unicode::encoding::details::impl;
    for (size_t index = 0; index < str.size();) {
        auto [valid, nextIndex] = is_valid_utf8_char(str, index);
        if (!valid) {
            return false;
        }
        index = nextIndex;
    }
    return true;
}

auto unicode::encoding::validate::is_valid_utf8(const std::u8string &str) -> bool {
    using namespace unicode::encoding::details::impl;
    auto span = std::span{str.data(), str.size()};
    for (size_t index = 0; index < str.size();) {
        auto [valid, nextIndex] = is_valid_utf8_char(str, index);
        if (!valid) {
            return false;
        }

        auto runeOpt = encoding::convert::utf8_to_rune({&span[index], nextIndex - index});
        if (!runeOpt || runeOpt > max_code_point()) {
            return false;
        }
        index = nextIndex;
    }
    return true;
}

auto unicode::encoding::validate::is_valid_utf16(const std::u16string &str) -> bool {
    auto valid = true;
    auto span = std::span{str.data(), str.size()};
    for (size_t index = 0; valid && index < str.size(); ++index) {
        if (props::is_high_surrogate(str[index])) {
            valid = index + 1 < str.size() && props::is_high_surrogate(str[index]) &&
                    props::is_low_surrogate(str[index + 1]);
            if (valid) {
                auto runeOpt = encoding::convert::utf16_to_rune({&span[index], 2});
                valid = runeOpt && runeOpt <= max_code_point();
            }
            index += 1;
        } else if (props::is_low_surrogate(str[index])) {
            valid = false;
        }
    }
    return valid;
}

auto unicode::encoding::validate::is_valid_utf32(const std::u32string &str) -> bool {
    using namespace unicode::encoding::details::impl;
    for (const auto ch: str) {
        if (ch > max_code_point()) {
            return false;
        }
    }
    return true;
}
