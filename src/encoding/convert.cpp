#include "encoding/convert.h"
#include "encoding/iter.h"
#include "encoding/details/impl.h"
#include "encoding/byte_order.h"
#include <bit>
#include <array>

auto unicode::encoding::convert::utf8_to_str(const std::u8string_view & str) -> std::string {
    std::string s;
    s.reserve(str.size());
    for (auto ch : str) { s.append(1, static_cast<char>(ch)); }
    return s;
}

auto unicode::encoding::convert::str_to_utf8(const std::string_view & str) -> std::u8string {
    std::u8string s;
    s.reserve(str.size());
    for (auto ch : str) { s.append(1, static_cast<char8_t>(ch)); }
    return s;
}

auto unicode::encoding::convert::utf8_to_utf32(const std::u8string_view& str, bool addBom) -> std::u32string {
    std::u32string s;
    s.reserve(str.size());

    for (const auto [rune, index] : unicode::encoding::iter::Utf8RuneIterable{str}) {
        if (index == 0 && addBom && !encoding::byte_order::is_bom(rune)) {
            s.push_back(encoding::byte_order::bom);
        }
        s.push_back(rune);
    }

    return s;
}

auto unicode::encoding::convert::utf32_to_utf8(const std::u32string_view& str, bool removeBom) -> std::u8string {
    std::u8string s;
    s.reserve(str.size());

    bool first = true;
    for (auto rune : iter::Utf32EndianIterable{str}) {
        if (first && removeBom) {
            first = false;
            continue;
        }
        first = false;
        auto bytes = rune_to_utf8(rune);
        std::copy(bytes.begin(), bytes.end(), std::back_inserter(s));
    }

    s.shrink_to_fit();
    return s;
}

static constexpr auto runeMasks = std::array{
        0b111110000000000000000,
        0b000001111100000000000,
        0b000000000011110000000,
};

auto unicode::encoding::convert::utf16_to_str(const std::u16string_view& str, bool removeBom) -> std::string {
    std::string res{};
    res.reserve(str.size());
    for (auto [rune, index] : iter::Utf16RuneIterable{str}) {
        if (index == 0 && removeBom && byte_order::is_bom(rune)) {
            continue;
        }
        auto chars = rune_to_utf8(rune);
        std::copy(chars.begin(), chars.end(), std::back_inserter(res));
    }

    res.shrink_to_fit();
    return res;
}

auto unicode::encoding::convert::utf16_to_utf8(const std::u16string_view& str, bool removeBom) -> std::u8string {
    std::u8string res{};
    res.reserve(str.size());
    for (auto [rune, index] : iter::Utf16RuneIterable{str}) {
        if (index == 0 && removeBom && byte_order::is_bom(rune)) {
            continue;
        }
        auto chars = rune_to_utf8(rune);
        std::copy(chars.begin(), chars.end(), std::back_inserter(res));
    }

    res.shrink_to_fit();
    return res;
}

auto unicode::encoding::convert::str_to_utf16(const std::string_view &str, bool addBom) -> std::u16string {
    auto res = std::u16string{};
    res.reserve(str.size());

    for (auto [rune, index] : unicode::encoding::iter::StrRuneIterable{str}) {
        if (index == 0 && addBom && !encoding::byte_order::is_bom(rune)) {
            res.push_back(static_cast<char16_t>(encoding::byte_order::bom));
        }
        auto u16 = rune_to_utf16(rune);
        std::copy(u16.begin(), u16.end(), std::back_inserter(res));
    }
    return res;
}

auto unicode::encoding::convert::utf16_to_utf16le(const std::u16string_view &str, bool removeBom) -> unicode::u16lestring {
    std::u16string res{};
    res.reserve(str.size());

    for (auto ch : iter::Utf16TargetEndianIterable(std::endian::little, str)) {
        res.push_back(ch);
    }

    if (removeBom && !res.empty()) {
        if (unicode::encoding::byte_order::is_bom(res[0]) || unicode::encoding::byte_order::is_bom_rev_16_bit(res[0])) {
            res.erase(0, 1);
        }
    }

    return res;
}

auto unicode::encoding::convert::utf16_to_utf16be(const std::u16string_view &str, bool removeBom) -> unicode::u16bestring {
    std::u16string res{};
    res.reserve(str.size());

    for (auto ch : iter::Utf16TargetEndianIterable(std::endian::big, str)) {
        res.push_back(ch);
    }

    if (removeBom && !res.empty()) {
        if (unicode::encoding::byte_order::is_bom(res[0]) || unicode::encoding::byte_order::is_bom_rev_16_bit(res[0])) {
            res.erase(0, 1);
        }
    }

    return res;
}

auto unicode::encoding::convert::utf16le_to_utf16(const unicode::u16lestring_view& str, bool addBom) -> std::u16string {
    std::u16string res{};
    res.reserve(str.size());

    bool first = true;
    for (const auto ch : unicode::encoding::iter::Utf16EndianIterable(str, std::endian::little)) {
        if (first && addBom && !encoding::byte_order::is_bom(ch)) {
            res.push_back(static_cast<char16_t>(encoding::byte_order::bom));
        }
        first = false;
        res.push_back(ch);
    }
    return res;
}

auto unicode::encoding::convert::utf16be_to_utf16(const unicode::u16bestring_view& str, bool addBom) -> std::u16string {
    std::u16string res{};
    res.reserve(str.size());

    bool first = true;
    for (const auto ch : unicode::encoding::iter::Utf16EndianIterable(str, std::endian::big)) {
        if (first && addBom && !encoding::byte_order::is_bom(ch)) {
            res.push_back(static_cast<char16_t>(encoding::byte_order::bom));
        }
        first = false;
        res.push_back(ch);
    }
    return res;
}

auto unicode::encoding::convert::utf32_to_str(const std::u32string_view &str, bool removeBom) -> std::string {
    std::string res{};
    res.reserve(str.size());

    bool first = true;
    for (const auto rune : iter::Utf32EndianIterable{str}) {
        if (first && removeBom && byte_order::is_bom(rune)) {
            first = false;
            continue;
        }
        first = false;
        auto bytes = rune_to_utf8(rune);
        std::copy(bytes.begin(), bytes.end(), std::back_inserter(res));
    }

    res.shrink_to_fit();
    return res;
}

auto unicode::encoding::convert::str_to_utf32(const std::string_view &str, bool addBom) -> std::u32string {
    std::u32string res{};
    res.reserve(str.size());

    for (const auto [rune, index] : iter::StrRuneIterable{str}) {
        if (index == 0 && addBom && !encoding::byte_order::is_bom(rune)) {
            res.push_back(static_cast<char16_t>(encoding::byte_order::bom));
        }
        res.push_back(rune);
    }

    res.shrink_to_fit();
    return res;
}

auto unicode::encoding::convert::utf16_to_utf32(const std::u16string_view &str) -> std::u32string {
    std::u32string res{};
    res.reserve(str.size());

    for (const auto [rune, _index] : unicode::encoding::iter::Utf16RuneIterable{str}) {
        res.push_back(rune);
    }
    res.shrink_to_fit();
    return res;
}

auto unicode::encoding::convert::utf32_to_utf16(const std::u32string_view &str) -> std::u16string {
    std::u16string res{};
    res.reserve(str.size());

    for (const auto rune : unicode::encoding::iter::Utf32EndianIterable{str}) {
        auto chars = rune_to_utf16(rune);
        std::copy(chars.begin(), chars.end(), std::back_inserter(res));
    }
    res.shrink_to_fit();
    return res;
}

auto unicode::encoding::convert::utf8_to_rune(const std::u8string_view& vec) -> std::optional<char32_t> {
    if (vec.empty()) {
        return std::nullopt;
    }
    auto iter = unicode::encoding::iter::Utf8RuneIterable{vec};
    std::optional<char32_t> res = std::nullopt;
    for(const auto [rune, _index] : iter) {
        if (res) {
            return std::nullopt;
        }
        res = rune;
    }
    return res;
}

auto unicode::encoding::convert::utf16_to_rune(const std::u16string_view& vec) -> std::optional<char32_t> {
    if (vec.empty()) {
        return std::nullopt;
    }
    auto iter = unicode::encoding::iter::Utf16RuneIterable{vec};
    std::optional<char32_t> res = std::nullopt;
    for(const auto [rune, _index] : iter) {
        if (res) {
            return std::nullopt;
        }
        res = rune;
    }
    return res;
}

auto unicode::encoding::convert::rune_to_utf8_vector(char32_t rune) -> std::vector<char8_t>{
    std::vector<char8_t> res{};
    auto rep = rune_to_utf8(rune);
    std::copy(rep.begin(), rep.end(), std::back_inserter(res));
    return res;
}

auto unicode::encoding::convert::rune_to_utf8(char32_t rune) -> details::MultiByteChar<char8_t, 4> {
    auto bytes = details::MultiByteChar<char8_t, 4>{};

    auto fourBytes = rune & runeMasks[0];
    auto threeBytes = rune & runeMasks[1];
    auto twoBytes = rune & runeMasks[2];

    if (fourBytes != 0U) {
        auto byte1 = static_cast<char8_t>((rune & 0b111000000000000000000) >> 18);
        auto byte2 = static_cast<char8_t>((rune & 0b000111111000000000000) >> 12);
        auto byte3 = static_cast<char8_t>((rune & 0b000000000111111000000) >> 6);
        auto byte4 = static_cast<char8_t>(rune & 0b000000000000000111111);
        bytes.emplace_back(0b11110000 | byte1);
        bytes.emplace_back(0b10000000 | byte2);
        bytes.emplace_back(0b10000000 | byte3);
        bytes.emplace_back(0b10000000 | byte4);
    }
    else if (threeBytes != 0U) {
        auto byte1 = static_cast<char8_t>((rune & 0b1111000000000000) >> 12);
        auto byte2 = static_cast<char8_t>((rune & 0b0000111111000000) >> 6);
        auto byte3 = static_cast<char8_t>((rune & 0b0000000000111111));
        bytes.emplace_back(0b11100000 | byte1);
        bytes.emplace_back(0b10000000 | byte2);
        bytes.emplace_back(0b10000000 | byte3);
    }
    else if (twoBytes != 0U) {
        auto byte1 = static_cast<char8_t>((rune & 0b11111000000) >> 6);
        auto byte2 = static_cast<char8_t>(rune & 0b00000111111);
        bytes.emplace_back(0b11000000 | byte1);
        bytes.emplace_back(0b10000000 | byte2);
    }
    else {
        bytes.emplace_back(static_cast<char8_t>(rune));
    }
    return bytes;
}


auto unicode::encoding::convert::rune_to_utf16_vector(char32_t rune) -> std::vector<char16_t>{
    std::vector<char16_t> res{};
    auto rep = rune_to_utf16(rune);
    std::copy(rep.begin(), rep.end(), std::back_inserter(res));
    return res;
}

auto unicode::encoding::convert::rune_to_utf16(char32_t rune) -> details::MultiByteChar<char16_t, 2> {
    auto res = details::MultiByteChar<char16_t, 2>{};

    if (rune <= 0xD7FF || (rune >= 0xE000 && rune <= 0xFFFF)) {
        res.emplace_back(static_cast<char16_t>(rune));
    }
    else {
        auto b20 = rune - unicode::encoding::details::impl::utf16Sub;
        auto high = b20 / 0x400 + unicode::encoding::details::impl::utf16HighOffset;
        auto low = b20 % 0x400 + unicode::encoding::details::impl::utf16LowOffset;
        res.emplace_back(high);
        res.emplace_back(low);
    }

    return res;
}
