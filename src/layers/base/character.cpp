#include "layers/base/character.h"
#include "db/ch.h"

auto unicode::layers::base::base_unicode_layer() -> std::shared_ptr<CharacterLayer> {
    auto dl = std::make_shared<CharacterLayer>();

#define DATA_LAYER_BASE_DEF(prop) dl->override_ ## prop([](char32_t ch) { \
    return db::ch:: prop(ch);                                     \
})
#define DATA_LAYER_BASE_DEF_CAST(prop, type) dl->override_ ## prop([](char32_t ch) { \
    auto res = db::ch:: prop(ch);                                     \
    return res ? std::optional{type{res}} : std::nullopt;\
})
#define DATA_LAYER_BASE_DEF_STR(prop) DATA_LAYER_BASE_DEF_CAST(prop, std::string)
#define DATA_LAYER_BASE_DEF_UTF32(prop) DATA_LAYER_BASE_DEF_CAST(prop, std::u32string)
#define DATA_LAYER_BASE_DEF_UTF8(prop) DATA_LAYER_BASE_DEF_CAST(prop, std::u8string)

    DATA_LAYER_BASE_DEF_STR(age);

    dl->override_aliases([](char32_t ch) -> std::optional<std::map<db::ch::AliasType, std::vector<std::string>>> {
        auto tmp = db::ch::aliases(ch);
        if (tmp.empty()) {
            return std::nullopt;
        }
        std::map<db::ch::AliasType, std::vector<std::string>> res{};
        for (const auto& e : tmp) {
            res[e.type].emplace_back(e.alias);
        }
        return res;
    });

    DATA_LAYER_BASE_DEF(bidi_class);
    DATA_LAYER_BASE_DEF(bidi_mirrored_glyph);
    DATA_LAYER_BASE_DEF(bidi_paired_bracket);
    DATA_LAYER_BASE_DEF(bidi_paired_bracket_type);
    DATA_LAYER_BASE_DEF(block);
    DATA_LAYER_BASE_DEF_UTF32(case_folding);
    DATA_LAYER_BASE_DEF(combining_properties);
    DATA_LAYER_BASE_DEF_UTF32(decomposition_mapping);
    DATA_LAYER_BASE_DEF(decomposition_type);
    DATA_LAYER_BASE_DEF(east_asian_width);
    DATA_LAYER_BASE_DEF(equivalent_unified_ideograph);
    DATA_LAYER_BASE_DEF_UTF32(fc_nfkc_closure);
    DATA_LAYER_BASE_DEF(general_category);
    DATA_LAYER_BASE_DEF(grapheme_cluster_break);
    DATA_LAYER_BASE_DEF(hangul_syllable_type);
    DATA_LAYER_BASE_DEF(indic_mantra_category);
    DATA_LAYER_BASE_DEF(indic_positional_category);
    DATA_LAYER_BASE_DEF(indic_syllabic_category);
    DATA_LAYER_BASE_DEF(is_alphabetic);
    DATA_LAYER_BASE_DEF(is_ascii_hex);
    DATA_LAYER_BASE_DEF(is_bidi_control);
    DATA_LAYER_BASE_DEF(is_bidi_mirrored);
    DATA_LAYER_BASE_DEF(is_case_ignorable);
    DATA_LAYER_BASE_DEF(is_cased);
    DATA_LAYER_BASE_DEF(is_changed_when_case_folded);
    DATA_LAYER_BASE_DEF(is_changed_when_case_mapped);
    DATA_LAYER_BASE_DEF(is_changed_when_lower_cased);
    DATA_LAYER_BASE_DEF(is_changed_when_title_cased);
    DATA_LAYER_BASE_DEF(is_changed_when_nfkc_case_folded);
    DATA_LAYER_BASE_DEF(is_changed_when_upper_cased);
    DATA_LAYER_BASE_DEF(is_composition_exclusion);
    DATA_LAYER_BASE_DEF(is_dash);
    DATA_LAYER_BASE_DEF(is_default_ignorable_code_point);
    DATA_LAYER_BASE_DEF(is_deprecated);
    DATA_LAYER_BASE_DEF(is_diacritic);
    DATA_LAYER_BASE_DEF(is_emoji);
    DATA_LAYER_BASE_DEF(is_emoji_component);
    DATA_LAYER_BASE_DEF(is_emoji_modifier);
    DATA_LAYER_BASE_DEF(is_emoji_modifier_base);
    DATA_LAYER_BASE_DEF(is_emoji_presentation);
    DATA_LAYER_BASE_DEF(is_expand_on_nfc);
    DATA_LAYER_BASE_DEF(is_expand_on_nfd);
    DATA_LAYER_BASE_DEF(is_expand_on_nfkc);
    DATA_LAYER_BASE_DEF(is_expand_on_nfkd);
    DATA_LAYER_BASE_DEF(is_extended_identifier_continue);
    DATA_LAYER_BASE_DEF(is_extended_identifier_start);
    DATA_LAYER_BASE_DEF(is_extended_pictograph);
    DATA_LAYER_BASE_DEF(is_extender);
    DATA_LAYER_BASE_DEF(is_full_composition_exclusion);
    DATA_LAYER_BASE_DEF(is_grapheme_base);
    DATA_LAYER_BASE_DEF(is_grapheme_extend);
    DATA_LAYER_BASE_DEF(is_grapheme_link);
    DATA_LAYER_BASE_DEF(is_hex);
    DATA_LAYER_BASE_DEF(is_hyphen);
    DATA_LAYER_BASE_DEF(is_identifier_continue);
    DATA_LAYER_BASE_DEF(is_identifier_start);
    DATA_LAYER_BASE_DEF(is_ideographic);
    DATA_LAYER_BASE_DEF(is_ids_binary_operator);
    DATA_LAYER_BASE_DEF(is_ids_trinary_operator);
    DATA_LAYER_BASE_DEF(is_join_control);
    DATA_LAYER_BASE_DEF(is_logical_order_exception);
    DATA_LAYER_BASE_DEF(is_lower);
    DATA_LAYER_BASE_DEF(is_math);
    DATA_LAYER_BASE_DEF(is_non_character_code_point);
    DATA_LAYER_BASE_DEF(is_other_alphabetic);
    DATA_LAYER_BASE_DEF(is_other_default_ignorable_code_point);
    DATA_LAYER_BASE_DEF(is_other_grapheme_extend);
    DATA_LAYER_BASE_DEF(is_other_identifier_continue);
    DATA_LAYER_BASE_DEF(is_other_identifier_start);
    DATA_LAYER_BASE_DEF(is_other_lower);
    DATA_LAYER_BASE_DEF(is_other_math);
    DATA_LAYER_BASE_DEF(is_other_upper);
    DATA_LAYER_BASE_DEF(is_pattern_syntax);
    DATA_LAYER_BASE_DEF(is_pattern_whitespace);
    DATA_LAYER_BASE_DEF(is_prepended_concatenation_mark);
    DATA_LAYER_BASE_DEF(is_quotation_mark);
    DATA_LAYER_BASE_DEF(is_radical);
    DATA_LAYER_BASE_DEF(is_regional_indicator);
    DATA_LAYER_BASE_DEF(is_sentence_terminal);
    DATA_LAYER_BASE_DEF(is_soft_dotted);
    DATA_LAYER_BASE_DEF(is_terminal_punctuation);
    DATA_LAYER_BASE_DEF(is_unified_ideograph);
    DATA_LAYER_BASE_DEF(is_upper);
    DATA_LAYER_BASE_DEF(is_variation_selector);
    DATA_LAYER_BASE_DEF(is_white_space);
    DATA_LAYER_BASE_DEF_STR(iso10646_comment);
    DATA_LAYER_BASE_DEF_STR(jamo_short_name);
    DATA_LAYER_BASE_DEF(join_type);
    DATA_LAYER_BASE_DEF(joining_group);
    DATA_LAYER_BASE_DEF_STR(kAccountingNumeric);
    DATA_LAYER_BASE_DEF_STR(kAlternateHanYu);
    DATA_LAYER_BASE_DEF_STR(kAlternateJEF);
    DATA_LAYER_BASE_DEF_STR(kAlternateKangXi);
    DATA_LAYER_BASE_DEF_STR(kAlternateMorohashi);
    DATA_LAYER_BASE_DEF_STR(kAlternateTotalStrokes);
    DATA_LAYER_BASE_DEF_STR(kBigFive);
    DATA_LAYER_BASE_DEF_STR(kCCCII);
    DATA_LAYER_BASE_DEF_STR(kCNS1986);
    DATA_LAYER_BASE_DEF_STR(kCNS1992);
    DATA_LAYER_BASE_DEF_STR(kCangjie);
    DATA_LAYER_BASE_DEF_STR(kCantonese);
    DATA_LAYER_BASE_DEF_STR(kCheungBauer);
    DATA_LAYER_BASE_DEF_STR(kCheungBauerIndex);
    DATA_LAYER_BASE_DEF_STR(kCihaiT);
    DATA_LAYER_BASE_DEF_STR(kCompatibilityVariant);
    DATA_LAYER_BASE_DEF_STR(kCowles);
    DATA_LAYER_BASE_DEF_STR(kDaeJaweon);
    DATA_LAYER_BASE_DEF_STR(kDefinition);
    DATA_LAYER_BASE_DEF_STR(kEACC);
    DATA_LAYER_BASE_DEF_STR(kFenn);
    DATA_LAYER_BASE_DEF_STR(kFennIndex);
    DATA_LAYER_BASE_DEF_STR(kFourCornerCode);
    DATA_LAYER_BASE_DEF_STR(kFrequency);
    DATA_LAYER_BASE_DEF_STR(kGB0);
    DATA_LAYER_BASE_DEF_STR(kGB1);
    DATA_LAYER_BASE_DEF_STR(kGB3);
    DATA_LAYER_BASE_DEF_STR(kGB5);
    DATA_LAYER_BASE_DEF_STR(kGB7);
    DATA_LAYER_BASE_DEF_STR(kGB8);
    DATA_LAYER_BASE_DEF_STR(kGSR);
    DATA_LAYER_BASE_DEF_STR(kGradeLevel);
    DATA_LAYER_BASE_DEF_UTF8(kHDZRadBreak);
    DATA_LAYER_BASE_DEF_STR(kHKGlyph);
    DATA_LAYER_BASE_DEF_STR(kHKSCS);
    DATA_LAYER_BASE_DEF_UTF8(kHangul);
    DATA_LAYER_BASE_DEF_UTF8(kHanyuPinlu);
    DATA_LAYER_BASE_DEF_UTF8(kHanyuPinyin);
    DATA_LAYER_BASE_DEF_STR(kIBMJapan);
    DATA_LAYER_BASE_DEF_STR(kIICore);
    DATA_LAYER_BASE_DEF_STR(kIRGDaeJaweon);
    DATA_LAYER_BASE_DEF_STR(kIRGDaiKanwaZiten);
    DATA_LAYER_BASE_DEF_STR(kIRGHanyuDaZidian);
    DATA_LAYER_BASE_DEF_STR(kIRGKangXi);
    DATA_LAYER_BASE_DEF_STR(kIRG_GSource);
    DATA_LAYER_BASE_DEF_STR(kIRG_HSource);
    DATA_LAYER_BASE_DEF_STR(kIRG_JSource);
    DATA_LAYER_BASE_DEF_STR(kIRG_KPSource);
    DATA_LAYER_BASE_DEF_STR(kIRG_KSource);
    DATA_LAYER_BASE_DEF_STR(kIRG_MSource);
    DATA_LAYER_BASE_DEF_STR(kIRG_SSource);
    DATA_LAYER_BASE_DEF_STR(kIRG_TSource);
    DATA_LAYER_BASE_DEF_STR(kIRG_UKSource);
    DATA_LAYER_BASE_DEF_STR(kIRG_USource);
    DATA_LAYER_BASE_DEF_STR(kIRG_VSource);
    DATA_LAYER_BASE_DEF_STR(kJHJ);
    DATA_LAYER_BASE_DEF_STR(kJIS0213);
    DATA_LAYER_BASE_DEF_STR(kJa);
    DATA_LAYER_BASE_DEF_STR(kJapaneseKun);
    DATA_LAYER_BASE_DEF_STR(kJapaneseOn);
    DATA_LAYER_BASE_DEF_STR(kJinmeiyoKanji);
    DATA_LAYER_BASE_DEF_STR(kJis0);
    DATA_LAYER_BASE_DEF_STR(kJis1);
    DATA_LAYER_BASE_DEF_STR(kJoyoKanji);
    DATA_LAYER_BASE_DEF_STR(kKPS0);
    DATA_LAYER_BASE_DEF_STR(kKPS1);
    DATA_LAYER_BASE_DEF_STR(kKSC0);
    DATA_LAYER_BASE_DEF_STR(kKangXi);
    DATA_LAYER_BASE_DEF_STR(kKarlgren);
    DATA_LAYER_BASE_DEF_STR(kKorean);
    DATA_LAYER_BASE_DEF_STR(kKoreanEducationHanja);
    DATA_LAYER_BASE_DEF_STR(kKoreanName);
    DATA_LAYER_BASE_DEF_STR(kLau);
    DATA_LAYER_BASE_DEF_STR(kMainlandTelegraph);
    DATA_LAYER_BASE_DEF_UTF8(kMandarin);
    DATA_LAYER_BASE_DEF_STR(kMatthews);
    DATA_LAYER_BASE_DEF_STR(kMeyerWempe);
    DATA_LAYER_BASE_DEF_STR(kMorohashi);
    DATA_LAYER_BASE_DEF_STR(kNelson);
    DATA_LAYER_BASE_DEF_STR(kOtherNumeric);
    DATA_LAYER_BASE_DEF_STR(kPhonetic);
    DATA_LAYER_BASE_DEF_STR(kPrimaryNumeric);
    DATA_LAYER_BASE_DEF_STR(kPseudoGB1);
    DATA_LAYER_BASE_DEF_STR(kRSAdobe_Japan1_6);
    DATA_LAYER_BASE_DEF_STR(kRSJapanese);
    DATA_LAYER_BASE_DEF_STR(kRSKanWa);
    DATA_LAYER_BASE_DEF_STR(kRSKangXi);
    DATA_LAYER_BASE_DEF_STR(kRSKorean);
    DATA_LAYER_BASE_DEF_STR(kRSMerged);
    DATA_LAYER_BASE_DEF_STR(kRSTUnicode);
    DATA_LAYER_BASE_DEF_STR(kRSUnicode);
    DATA_LAYER_BASE_DEF_STR(kReading);
    DATA_LAYER_BASE_DEF_STR(kSBGY);
    DATA_LAYER_BASE_DEF_STR(kSemanticVariant);
    DATA_LAYER_BASE_DEF_STR(kSimplifiedVariant);
    DATA_LAYER_BASE_DEF_STR(kSpecializedSemanticVariant);
    DATA_LAYER_BASE_DEF_STR(kSpoofingVariant);
    DATA_LAYER_BASE_DEF_STR(kSrc_NushuDuben);
    DATA_LAYER_BASE_DEF_STR(kStrange);
    DATA_LAYER_BASE_DEF_STR(kTGH);
    DATA_LAYER_BASE_DEF_UTF8(kTGHZ2013);
    DATA_LAYER_BASE_DEF_STR(kTGT_MergedSrc);
    DATA_LAYER_BASE_DEF_STR(kTaiwanTelegraph);
    DATA_LAYER_BASE_DEF_UTF8(kTang);
    DATA_LAYER_BASE_DEF_STR(kTotalStrokes);
    DATA_LAYER_BASE_DEF_STR(kTraditionalVariant);
    DATA_LAYER_BASE_DEF_STR(kUnihanCore2020);
    DATA_LAYER_BASE_DEF_UTF8(kVietnamese);
    DATA_LAYER_BASE_DEF_UTF8(kXHC1983);
    DATA_LAYER_BASE_DEF_STR(kXerox);
    DATA_LAYER_BASE_DEF_STR(kZVariant);
    DATA_LAYER_BASE_DEF_STR(legacy_name);
    DATA_LAYER_BASE_DEF(line_break);
    DATA_LAYER_BASE_DEF_UTF32(lower_case);
    DATA_LAYER_BASE_DEF_STR(name);
    DATA_LAYER_BASE_DEF(nfc_quick_check);
    DATA_LAYER_BASE_DEF(nfd_quick_check);
    DATA_LAYER_BASE_DEF_UTF32(nfkc_case_fold);
    DATA_LAYER_BASE_DEF(nfkc_quick_check);
    DATA_LAYER_BASE_DEF(nfkd_quick_check);
    DATA_LAYER_BASE_DEF(numeric_type);
    DATA_LAYER_BASE_DEF_STR(numeric_value);
    DATA_LAYER_BASE_DEF(script);
    DATA_LAYER_BASE_DEF(script_extensions);
    DATA_LAYER_BASE_DEF(sentence_break);
    DATA_LAYER_BASE_DEF(simple_case_folding);
    DATA_LAYER_BASE_DEF(simple_lower_case);
    DATA_LAYER_BASE_DEF(simple_title_case);
    DATA_LAYER_BASE_DEF(simple_upper_case);
    DATA_LAYER_BASE_DEF_UTF32(title_case);
    DATA_LAYER_BASE_DEF_UTF32(upper_case);
    DATA_LAYER_BASE_DEF(vertical_orientation);
    DATA_LAYER_BASE_DEF(word_break);

#undef DATA_LAYER_BASE_DEF_STR
#undef DATA_LAYER_BASE_DEF_UTF32
#undef DATA_LAYER_BASE_DEF_UTF8
#undef DATA_LAYER_BASE_DEF_CAST
#undef DATA_LAYER_BASE_DEF

    return dl;
}
