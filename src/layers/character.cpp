#include "layers/character.h"

auto unicode::layers::CharacterLayer::set_fallback_layer(std::shared_ptr<CharacterLayer> fallback) -> CharacterLayer& {
    child = std::move(fallback);
    return *this;
}

auto unicode::layers::CharacterLayer::merge(const CharacterLayer& other) -> CharacterLayer& {
    for (const auto& pair : other.flagOverrides) {
        flagOverrides[pair.first] = pair.second;
    }

    for (const auto& pair : other.charOverrides) {
        charOverrides[pair.first] = pair.second;
    }

    for (const auto& pair : other.strOverrides) {
        strOverrides[pair.first] = pair.second;
    }

    for (const auto& pair : other.utf32Overrides) {
        utf32Overrides[pair.first] = pair.second;
    }

    for (const auto& pair : other.utf8Overrides) {
        utf8Overrides[pair.first] = pair.second;
    }

#define DATA_LAYER_OPT_MOVE(opt) if (other.opt.has_value()) { opt = other.opt ; }

    DATA_LAYER_OPT_MOVE(aliasesOverride);
    DATA_LAYER_OPT_MOVE(isBomRev16BitOverride);
    DATA_LAYER_OPT_MOVE(bidiClassOverride);
    DATA_LAYER_OPT_MOVE(bidiPairedBracketOverride);
    DATA_LAYER_OPT_MOVE(graphemeClusterBreakOverride);
    DATA_LAYER_OPT_MOVE(indicMantraCategoryOverride);
    DATA_LAYER_OPT_MOVE(indicPositionalCategoryOverride);
    DATA_LAYER_OPT_MOVE(indicSyllabicCategoryOverride);
    DATA_LAYER_OPT_MOVE(joinTypeOverride);
    DATA_LAYER_OPT_MOVE(joiningGroupOverride);
    DATA_LAYER_OPT_MOVE(lineBreakOverride);
    DATA_LAYER_OPT_MOVE(sentenceBreakOverride);
    DATA_LAYER_OPT_MOVE(wordBreakOverride);
    DATA_LAYER_OPT_MOVE(blockOverride);
    DATA_LAYER_OPT_MOVE(generalCategoryOverride);
    DATA_LAYER_OPT_MOVE(eastAsianWidthOverride);
    DATA_LAYER_OPT_MOVE(hangulSyllableTypeOverride);
    DATA_LAYER_OPT_MOVE(decompositionTypeOverride);
    DATA_LAYER_OPT_MOVE(nfcQcOverride);
    DATA_LAYER_OPT_MOVE(nfdQcOverride);
    DATA_LAYER_OPT_MOVE(nfkcQcOverride);
    DATA_LAYER_OPT_MOVE(nfkdQcOverride);
    DATA_LAYER_OPT_MOVE(numericTypeOverride);
    DATA_LAYER_OPT_MOVE(scriptOverride);
    DATA_LAYER_OPT_MOVE(scriptExtensionsOverride);
    DATA_LAYER_OPT_MOVE(combiningPropertiesOverride);

#undef DATA_LAYER_OPT_MOVE

    if (!child && other.child) {
        child = other.child;
    }

    return *this;
}

auto unicode::layers::CharacterLayer::combining_properties(char32_t ch) const -> std::optional<uint8_t> {
    std::optional<uint8_t> res = std::nullopt;
    if (combiningPropertiesOverride.has_value()) {
        res = combiningPropertiesOverride.value()(ch);
    }
    if (!res.has_value() && child) {
        res = child->combining_properties(ch);
    }
    return res;
}

auto unicode::layers::CharacterLayer::override_combining_properties(std::function<auto (char32_t) -> std::optional<uint8_t>> override) -> CharacterLayer& {
    combiningPropertiesOverride = std::move(override);
    return *this;
}

auto unicode::layers::CharacterLayer::aliases(char32_t ch) const -> std::optional<std::map<db::ch::AliasType, std::vector<std::string>>> {
    std::optional<std::map<db::ch::AliasType, std::vector<std::string>>> res = std::nullopt;
    if (aliasesOverride.has_value()) {
        res = aliasesOverride.value()(ch);
    }
    if (!res.has_value() && child) {
        res = child->aliases(ch);
    }
    return res;
}

auto unicode::layers::CharacterLayer::override_aliases(std::function<auto (char32_t) -> std::optional<std::map<db::ch::AliasType, std::vector<std::string>>>> override) -> CharacterLayer& {
    aliasesOverride = std::move(override);
    return *this;
}

auto unicode::layers::CharacterLayer::script_extensions(char32_t ch) const -> std::optional<std::vector<db::ch::Script>> {
    std::optional<std::vector<db::ch::Script>> res = std::nullopt;
    if (scriptExtensionsOverride.has_value()) {
        res = scriptExtensionsOverride.value()(ch);
    }
    if (!res.has_value() && child) {
        res = child->script_extensions(ch);
    }
    return res;
}

auto unicode::layers::CharacterLayer::override_script_extensions(std::function<auto (char32_t) -> std::optional<std::vector<db::ch::Script>>> override) -> CharacterLayer& {
    scriptExtensionsOverride = std::move(override);
    return *this;
}

#define DATA_LAYER_FLAG(flag) \
auto unicode::layers::CharacterLayer:: flag (char32_t ch) const -> std::optional<bool> { \
    std::optional<bool> res = std::nullopt; \
    if (flagOverrides.contains( #flag )) { \
        res = flagOverrides.at( #flag )(ch); \
    } \
    if (!res.has_value() && child) { \
        res = child-> flag(ch); \
    } \
    return res; \
}                             \
                              \
auto unicode::layers::CharacterLayer::override_ ## flag (std::function<auto (char32_t) -> std::optional<bool>> override) -> CharacterLayer& { \
    flagOverrides[ #flag ] = std::move(override);                                   \
    return *this; \
}

DATA_LAYER_FLAG(is_alphabetic);

DATA_LAYER_FLAG(is_ascii_hex);

DATA_LAYER_FLAG(is_bidi_control);

DATA_LAYER_FLAG(is_bidi_mirrored);

DATA_LAYER_FLAG(is_case_ignorable);

DATA_LAYER_FLAG(is_cased);

DATA_LAYER_FLAG(is_changed_when_case_folded);

DATA_LAYER_FLAG(is_changed_when_case_mapped);

DATA_LAYER_FLAG(is_changed_when_lower_cased);

DATA_LAYER_FLAG(is_changed_when_title_cased);

DATA_LAYER_FLAG(is_changed_when_nfkc_case_folded);

DATA_LAYER_FLAG(is_changed_when_upper_cased);

DATA_LAYER_FLAG(is_composition_exclusion);

DATA_LAYER_FLAG(is_dash);

DATA_LAYER_FLAG(is_default_ignorable_code_point);

DATA_LAYER_FLAG(is_deprecated);

DATA_LAYER_FLAG(is_diacritic);

DATA_LAYER_FLAG(is_emoji);

DATA_LAYER_FLAG(is_emoji_component);

DATA_LAYER_FLAG(is_emoji_modifier);

DATA_LAYER_FLAG(is_emoji_modifier_base);

DATA_LAYER_FLAG(is_emoji_presentation);

DATA_LAYER_FLAG(is_expand_on_nfc);

DATA_LAYER_FLAG(is_expand_on_nfd);

DATA_LAYER_FLAG(is_expand_on_nfkc);

DATA_LAYER_FLAG(is_expand_on_nfkd);

DATA_LAYER_FLAG(is_extended_identifier_continue);

DATA_LAYER_FLAG(is_extended_identifier_start);

DATA_LAYER_FLAG(is_extended_pictograph);

DATA_LAYER_FLAG(is_extender);

DATA_LAYER_FLAG(is_full_composition_exclusion);

DATA_LAYER_FLAG(is_grapheme_base);

DATA_LAYER_FLAG(is_grapheme_extend);

DATA_LAYER_FLAG(is_grapheme_link);

DATA_LAYER_FLAG(is_hex);

DATA_LAYER_FLAG(is_hyphen);

DATA_LAYER_FLAG(is_identifier_continue);

DATA_LAYER_FLAG(is_identifier_start);

DATA_LAYER_FLAG(is_ideographic);

DATA_LAYER_FLAG(is_ids_binary_operator);

DATA_LAYER_FLAG(is_ids_trinary_operator);

DATA_LAYER_FLAG(is_join_control);

DATA_LAYER_FLAG(is_logical_order_exception);

DATA_LAYER_FLAG(is_lower);

DATA_LAYER_FLAG(is_math);

DATA_LAYER_FLAG(is_non_character_code_point);

DATA_LAYER_FLAG(is_other_alphabetic);

DATA_LAYER_FLAG(is_other_default_ignorable_code_point);

DATA_LAYER_FLAG(is_other_grapheme_extend);

DATA_LAYER_FLAG(is_other_identifier_continue);

DATA_LAYER_FLAG(is_other_identifier_start);

DATA_LAYER_FLAG(is_other_lower);

DATA_LAYER_FLAG(is_other_math);

DATA_LAYER_FLAG(is_other_upper);

DATA_LAYER_FLAG(is_pattern_syntax);

DATA_LAYER_FLAG(is_pattern_whitespace);

DATA_LAYER_FLAG(is_prepended_concatenation_mark);

DATA_LAYER_FLAG(is_quotation_mark);

DATA_LAYER_FLAG(is_radical);

DATA_LAYER_FLAG(is_regional_indicator);

DATA_LAYER_FLAG(is_sentence_terminal);

DATA_LAYER_FLAG(is_soft_dotted);

DATA_LAYER_FLAG(is_terminal_punctuation);

DATA_LAYER_FLAG(is_unified_ideograph);

DATA_LAYER_FLAG(is_upper);

DATA_LAYER_FLAG(is_variation_selector);

DATA_LAYER_FLAG(is_white_space);

#undef DATA_LAYER_FLAG

#define DATA_LAYER_CHAR32(prop) \
auto unicode::layers::CharacterLayer:: prop (char32_t ch) const -> std::optional<char32_t> { \
    std::optional<char32_t> res = std::nullopt; \
    if (charOverrides.contains( #prop )) { \
        res = charOverrides.at( #prop )(ch); \
    } \
    if (!res.has_value() && child) { \
        res = child-> prop(ch); \
    } \
    return res; \
}                             \
                              \
auto unicode::layers::CharacterLayer::override_ ## prop (std::function<auto (char32_t) -> std::optional<char32_t>> override) -> CharacterLayer& { \
    charOverrides[ #prop ] = std::move(override);                                   \
    return *this; \
}

DATA_LAYER_CHAR32(bidi_mirrored_glyph)

DATA_LAYER_CHAR32(bidi_paired_bracket)

DATA_LAYER_CHAR32(equivalent_unified_ideograph)

DATA_LAYER_CHAR32(simple_case_folding)

DATA_LAYER_CHAR32(simple_lower_case)

DATA_LAYER_CHAR32(simple_title_case)

DATA_LAYER_CHAR32(simple_upper_case)

#undef DATA_LAYER_CHAR32

#define DATA_LAYER_STR(prop) \
auto unicode::layers::CharacterLayer:: prop (char32_t ch) const -> std::optional<std::string> { \
    std::optional<std::string> res = std::nullopt; \
    if (strOverrides.contains( #prop )) { \
        res = strOverrides.at( #prop )(ch); \
    } \
    if (!res.has_value() && child) { \
        res = child-> prop(ch); \
    } \
    return res; \
}                             \
                              \
auto unicode::layers::CharacterLayer::override_ ## prop (std::function<auto (char32_t) -> std::optional<std::string>> override) -> CharacterLayer& { \
    strOverrides[ #prop ] = std::move(override);                                   \
    return *this; \
}

#define DATA_LAYER_UTF8(prop) \
auto unicode::layers::CharacterLayer:: prop (char32_t ch) const -> std::optional<std::u8string> { \
    std::optional<std::u8string> res = std::nullopt; \
    if (utf8Overrides.contains( #prop )) { \
        res = utf8Overrides.at( #prop )(ch); \
    } \
    if (!res.has_value() && child) { \
        res = child-> prop(ch); \
    } \
    return res; \
}                             \
                              \
auto unicode::layers::CharacterLayer::override_ ## prop (std::function<auto (char32_t) -> std::optional<std::u8string>> override) -> CharacterLayer& { \
    utf8Overrides[ #prop ] = std::move(override);                                   \
    return *this; \
}

DATA_LAYER_STR(age)

DATA_LAYER_STR(name)

DATA_LAYER_STR(legacy_name)

DATA_LAYER_STR(iso10646_comment)

DATA_LAYER_STR(jamo_short_name)

DATA_LAYER_STR(numeric_value)

DATA_LAYER_STR(kSrc_NushuDuben)

DATA_LAYER_STR(kReading)

DATA_LAYER_STR(kRSTUnicode)

DATA_LAYER_STR(kTGT_MergedSrc)

DATA_LAYER_STR(kBigFive)

DATA_LAYER_STR(kIRGKangXi)

DATA_LAYER_STR(kRSKorean)

DATA_LAYER_STR(kAccountingNumeric)

DATA_LAYER_STR(kAlternateKangXi)

DATA_LAYER_STR(kAlternateHanYu)

DATA_LAYER_STR(kAlternateJEF)

DATA_LAYER_STR(kAlternateMorohashi)

DATA_LAYER_STR(kAlternateTotalStrokes)

DATA_LAYER_STR(kCCCII)

DATA_LAYER_STR(kCNS1986)

DATA_LAYER_STR(kCNS1992)

DATA_LAYER_STR(kCangjie)

DATA_LAYER_STR(kCantonese)

DATA_LAYER_STR(kCheungBauer)

DATA_LAYER_STR(kCheungBauerIndex)

DATA_LAYER_STR(kCihaiT)

DATA_LAYER_STR(kCompatibilityVariant)

DATA_LAYER_STR(kCowles)

DATA_LAYER_STR(kDaeJaweon)

DATA_LAYER_STR(kDefinition)

DATA_LAYER_STR(kEACC)

DATA_LAYER_STR(kFenn)

DATA_LAYER_STR(kFennIndex)

DATA_LAYER_STR(kFourCornerCode)

DATA_LAYER_STR(kFrequency)

DATA_LAYER_STR(kGB0)

DATA_LAYER_STR(kGB1)

DATA_LAYER_STR(kGB3)

DATA_LAYER_STR(kGB5)

DATA_LAYER_STR(kGB7)

DATA_LAYER_STR(kGB8)

DATA_LAYER_STR(kGSR)

DATA_LAYER_STR(kGradeLevel)

DATA_LAYER_UTF8(kHDZRadBreak)

DATA_LAYER_STR(kHKGlyph)

DATA_LAYER_STR(kHKSCS)

DATA_LAYER_UTF8(kHangul)

DATA_LAYER_UTF8(kHanyuPinlu)

DATA_LAYER_UTF8(kHanyuPinyin)

DATA_LAYER_STR(kIBMJapan)

DATA_LAYER_STR(kIICore)

DATA_LAYER_STR(kIRGDaeJaweon)

DATA_LAYER_STR(kIRGDaiKanwaZiten)

DATA_LAYER_STR(kIRGHanyuDaZidian)

DATA_LAYER_STR(kIRG_GSource)

DATA_LAYER_STR(kIRG_HSource)

DATA_LAYER_STR(kIRG_JSource)

DATA_LAYER_STR(kIRG_KPSource)

DATA_LAYER_STR(kIRG_KSource)

DATA_LAYER_STR(kIRG_MSource)

DATA_LAYER_STR(kIRG_SSource)

DATA_LAYER_STR(kIRG_TSource)

DATA_LAYER_STR(kIRG_UKSource)

DATA_LAYER_STR(kIRG_USource)

DATA_LAYER_STR(kIRG_VSource)

DATA_LAYER_STR(kJHJ)

DATA_LAYER_STR(kJIS0213)

DATA_LAYER_STR(kJa)

DATA_LAYER_STR(kJapaneseKun)

DATA_LAYER_STR(kJapaneseOn)

DATA_LAYER_STR(kJinmeiyoKanji)

DATA_LAYER_STR(kJis0)

DATA_LAYER_STR(kJis1)

DATA_LAYER_STR(kJoyoKanji)

DATA_LAYER_STR(kKPS0)

DATA_LAYER_STR(kKPS1)

DATA_LAYER_STR(kKSC0)

DATA_LAYER_STR(kKangXi)

DATA_LAYER_STR(kKarlgren)

DATA_LAYER_STR(kKorean)

DATA_LAYER_STR(kKoreanEducationHanja)

DATA_LAYER_STR(kKoreanName)

DATA_LAYER_STR(kLau)

DATA_LAYER_STR(kMainlandTelegraph)

DATA_LAYER_UTF8(kMandarin)

DATA_LAYER_STR(kMatthews)

DATA_LAYER_STR(kMeyerWempe)

DATA_LAYER_STR(kMorohashi)

DATA_LAYER_STR(kNelson)

DATA_LAYER_STR(kOtherNumeric)

DATA_LAYER_STR(kPhonetic)

DATA_LAYER_STR(kPrimaryNumeric)

DATA_LAYER_STR(kPseudoGB1)

DATA_LAYER_STR(kRSAdobe_Japan1_6)

DATA_LAYER_STR(kRSJapanese)

DATA_LAYER_STR(kRSKanWa)

DATA_LAYER_STR(kRSKangXi)

DATA_LAYER_STR(kRSMerged)

DATA_LAYER_STR(kRSUnicode)

DATA_LAYER_STR(kSBGY)

DATA_LAYER_STR(kSpecializedSemanticVariant)

DATA_LAYER_STR(kSemanticVariant)

DATA_LAYER_STR(kSimplifiedVariant)

DATA_LAYER_STR(kSpoofingVariant)

DATA_LAYER_STR(kStrange)

DATA_LAYER_STR(kTGH)

DATA_LAYER_UTF8(kTGHZ2013)

DATA_LAYER_STR(kTaiwanTelegraph)

DATA_LAYER_UTF8(kTang)

DATA_LAYER_STR(kTotalStrokes)

DATA_LAYER_STR(kTraditionalVariant)

DATA_LAYER_STR(kUnihanCore2020)

DATA_LAYER_UTF8(kVietnamese)

DATA_LAYER_UTF8(kXHC1983)

DATA_LAYER_STR(kXerox)

DATA_LAYER_STR(kZVariant)

#undef DATA_LAYER_STR
#undef DATA_LAYER_UTF8

#define DATA_LAYER_ENUM(prop, camelCase, enum) \
auto unicode::layers::CharacterLayer:: prop (char32_t ch) const -> std::optional<db::ch:: enum> { \
    std::optional<db::ch:: enum> res = std::nullopt; \
    if (camelCase.has_value()) { \
        res = camelCase.value()(ch); \
    } \
    if (!res.has_value() && child) { \
        res = child-> prop(ch); \
    } \
    return res; \
}                             \
                              \
auto unicode::layers::CharacterLayer::override_ ## prop (std::function<auto (char32_t) -> std::optional<db::ch:: enum>> override) -> CharacterLayer& { \
    camelCase = std::move(override);                                   \
    return *this; \
}

DATA_LAYER_ENUM(bidi_class, bidiClassOverride, BidiClass)

DATA_LAYER_ENUM(bidi_paired_bracket_type, bidiPairedBracketOverride, BidiPairedBracketType)

DATA_LAYER_ENUM(grapheme_cluster_break, graphemeClusterBreakOverride, GraphemeClusterBreak)

DATA_LAYER_ENUM(indic_mantra_category, indicMantraCategoryOverride, IndicMantraCategory)

DATA_LAYER_ENUM(indic_positional_category, indicPositionalCategoryOverride, IndicPositionalCategory)

DATA_LAYER_ENUM(indic_syllabic_category, indicSyllabicCategoryOverride, IndicSyllabicCategory)

DATA_LAYER_ENUM(join_type, joinTypeOverride, JoinType)

DATA_LAYER_ENUM(joining_group, joiningGroupOverride, JoiningGroup)

DATA_LAYER_ENUM(line_break, lineBreakOverride, LineBreak)

DATA_LAYER_ENUM(sentence_break, sentenceBreakOverride, SentenceBreak)

DATA_LAYER_ENUM(vertical_orientation, verticalOrientationOverride, VerticalOrientation)

DATA_LAYER_ENUM(word_break, wordBreakOverride, WordBreak)

DATA_LAYER_ENUM(block, blockOverride, Block)

DATA_LAYER_ENUM(general_category, generalCategoryOverride, GeneralCategory)

DATA_LAYER_ENUM(east_asian_width, eastAsianWidthOverride, EastAsianWidth)

DATA_LAYER_ENUM(hangul_syllable_type, hangulSyllableTypeOverride, HangulSyllableType)

DATA_LAYER_ENUM(decomposition_type, decompositionTypeOverride, DecompositionType)

DATA_LAYER_ENUM(nfc_quick_check, nfcQcOverride, NFC_QC)

DATA_LAYER_ENUM(nfd_quick_check, nfdQcOverride, NFD_QC)

DATA_LAYER_ENUM(nfkc_quick_check, nfkcQcOverride, NFKC_QC)

DATA_LAYER_ENUM(nfkd_quick_check, nfkdQcOverride, NFKD_QC)

DATA_LAYER_ENUM(numeric_type, numericTypeOverride, NumericType)

DATA_LAYER_ENUM(script, scriptOverride, Script)

#undef DATA_LAYER_ENUM


#define DATA_LAYER_UTF32(prop) \
auto unicode::layers::CharacterLayer:: prop (char32_t ch) const -> std::optional<std::u32string> { \
    std::optional<std::u32string> res = std::nullopt; \
    if (utf32Overrides.contains( #prop)) { \
        res = utf32Overrides.at( #prop)(ch); \
    } \
    if (!res.has_value() && child) { \
        res = child-> prop(ch); \
    } \
    return res; \
}                             \
                              \
auto unicode::layers::CharacterLayer::override_ ## prop (std::function<auto (char32_t) -> std::optional<std::u32string>> override) -> CharacterLayer& { \
    utf32Overrides[ #prop ] = std::move(override);                                   \
    return *this; \
}

DATA_LAYER_UTF32(upper_case)

DATA_LAYER_UTF32(lower_case)

DATA_LAYER_UTF32(title_case)

DATA_LAYER_UTF32(case_folding)

DATA_LAYER_UTF32(decomposition_mapping)

DATA_LAYER_UTF32(fc_nfkc_closure)

DATA_LAYER_UTF32(nfkc_case_fold)

#undef DATA_LAYER_UTF32
