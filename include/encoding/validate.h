#pragma once

#include <string>

namespace unicode::encoding::validate {
    /**
     * Maximum Unicode code point
     * @return
     */
    constexpr auto max_code_point() -> char32_t {
        return 0x10FFFF;
    }

    /**
     * Checks if the given string is a valid UTF-8 string
     * @param str
     * @return is valid
     */
    auto is_valid_utf8(const std::string& str) -> bool;

    /**
     * Checks if the given string is a valid UTF-8 string
     * @param str
     * @return is valid
     */
    auto is_valid_utf8(const std::u8string& str) -> bool;

    /**
     * Checks if the given string is a valid UTF-16 string
     * @param str
     * @return is valid
     */
    auto is_valid_utf16(const std::u16string& str) -> bool;

    /**
     * Checks if the given string is a valid UTF-32 string
     * @param str
     * @return is valid
     */
    auto is_valid_utf32(const std::u32string& str) -> bool;
}
