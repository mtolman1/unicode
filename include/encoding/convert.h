#pragma once

#include <string>
#include <tuple>
#include <vector>
#include <array>
#include <optional>

namespace unicode {
    using char16le_t [[maybe_unused]] = char16_t;
    using char16be_t [[maybe_unused]] = char16_t;
    using u16lestring = std::u16string;
    using u16bestring = std::u16string;
    using u16lestring_view = std::u16string_view;
    using u16bestring_view = std::u16string_view;
}

namespace unicode::encoding::convert {

    /**
     * Converts a u8string to a normal string
     * @param str UTF-8 string
     * @return String
     */
    auto utf8_to_str(const std::u8string_view& str) -> std::string;

    /**
     * Converts a normal string to a u8string
     * @param str String
     * @return UTF-8 string
     */
    auto str_to_utf8(const std::string_view& str) -> std::u8string;

    /**
     * Converts a u16string to a normal string
     * @param str UTF-16 string
     * @return String
     */
    auto utf16_to_str(const std::u16string_view& str, bool removeBom = false) -> std::string;

    /**
     * Converts a u16string to a UTF-8 string
     * @param str UTF-16 string
     * @return String
     */
    auto utf16_to_utf8(const std::u16string_view& str, bool removeBom = false) -> std::u8string;

    /**
     * Converts a normal string to a u16string
     * Uses byte order of machine
     * @param str String
     * @return UTF-16 string
     */
    auto str_to_utf16(const std::string_view& str, bool addBom = false) -> std::u16string;

    /**
     * Converts a normal string to a u16string
     * Uses little endian byte order
     * @param str String
     * @return UTF-16 string
     */
    auto utf16_to_utf16le(const std::u16string_view& str, bool removeBom = false) -> unicode::u16lestring ;

    /**
     * Converts a normal string to a u16string
     * Uses big endian byte order
     * @param str String
     * @return UTF-16 string
     */
    auto utf16_to_utf16be(const std::u16string_view& str, bool removeBom = false) -> unicode::u16bestring;

    /**
     * Converts a UTF-16 Little Endian string to std::string (UTF-8 encoded)
     * @param str String
     * @param removeBom Whether to remove the Byte Order Mark (if present)
     * @return UTF-8 string
     */
    auto utf16le_to_utf16(const unicode::u16lestring_view& str, bool addBom = false) -> std::u16string;

    /**
     * Converts a UTF-16 Big Endian string to std::string (UTF-8 encoded)
     * @param str String
     * @param removeBom Whether to remove the Byte Order Mark (if present)
     * @return UTF-8 string
     */
    auto utf16be_to_utf16(const unicode::u16bestring_view& str, bool addBom = false) -> std::u16string;

    /**
     * Converts a u32string to a normal string
     * @param str UTF-32 string
     * @return String
     */
    auto utf32_to_str(const std::u32string_view& str, bool removeBom = false) -> std::string;

    /**
     * Converts a normal string to a u8string
     * @param str String
     * @return UTF-32 string
     */
    auto str_to_utf32(const std::string_view& str, bool addBom = false) -> std::u32string;

    /**
     * Converts a u8string to a u32string
     * @param str UTF-8 string
     * @return UTF-32 string
     */
    auto utf8_to_utf32(const std::u8string_view& str, bool addBom = false) -> std::u32string;

    /**
     * Converts a u32string to a u8string
     * @param str UTF-32 string
     * @return UTF-8 string
     */
    auto utf32_to_utf8(const std::u32string_view& str, bool removeBom = false) -> std::u8string;

    /**
     * Converts a u8string to a u16string
     * @param str UTF-16 string
     * @return UTF-32 string
     */
    auto utf16_to_utf32(const std::u16string_view& str) -> std::u32string;

    /**
     * Converts a u16string to a u8string
     * @param str UTF-32 string
     * @return UTF-16 string
     */
    auto utf32_to_utf16(const std::u32string_view& str) -> std::u16string;

    /**
     * Converts a rune (Unicode character) to a UTF-8 character sequence
     * @param rune Unicode character (UTF-32)
     * @return Tuple of UTF-8 char array and number of bytes in character
     */
    auto rune_to_utf8_vector(char32_t rune) -> std::vector<char8_t>;

    /**
     * Converts a rune (Unicode character) to a UTF-16 character sequence
     * @param rune Unicode character (UTF-32)
     * @return UTF-8 characters
     */
    auto rune_to_utf16_vector(char32_t rune) -> std::vector<char16_t>;

    namespace details {
        /**
         * Struct to hold multibyte encoding character information
         * Avoids heap allocations when possible
         * @tparam T Type for character
         * @tparam Max Maximum number of characters
         */
        template<typename T, unsigned Max>
        class MultiByteChar {
        public:
            using Array = std::array<T, Max>;
            using iterator = typename Array::iterator;
            using const_iterator = typename Array::const_iterator;

            size_t size() { return used; }
            size_t capacity() { return Max; }

            auto begin() -> iterator { return bytes.begin(); }
            auto end() -> iterator  { return bytes.begin() + used; }

            auto begin() const -> const_iterator { return bytes.begin(); }
            auto end() const -> const_iterator { return bytes.begin() + used; }

            constexpr auto at(size_t index) -> T& {
                if (index >= used) {
                    throw std::out_of_range{"MultiByteChar at out of bounds"};
                }
                return bytes.at(index);
            }

            constexpr auto at(size_t index) const -> const T& {
                if (index >= used) {
                    throw std::out_of_range{"MultiByteChar at out of bounds"};
                }
                return bytes.at(index);
            }

            constexpr auto emplace_back(T elem) -> void {
                if (used + 1 > Max) {
                    throw std::out_of_range{"MultiByteChar emplace_back out of bounds"};
                }
                bytes[used++] = std::move(elem);
            }

        private:
            Array bytes = {};
            unsigned used = 0;
        };
    }

    /**
     * Converts a rune (Unicode character) to a UTF-8 character sequence
     * @param rune Unicode character (UTF-32)
     * @return Multibyte character which holds the bytes and the number of bytes used
     */
    auto rune_to_utf8(char32_t rune) -> details::MultiByteChar<char8_t, 4>;

    /**
     * Converts a rune (Unicode character) to a UTF-16 character sequence
     * @param rune Unicode character (UTF-32)
     * @return Multibyte character which holds the bytes and the number of bytes used
     */
    auto rune_to_utf16(char32_t rune) -> details::MultiByteChar<char16_t, 2>;

    /**
     * Converts UTF-8 bytes to a rune (UTF-32 character)
     * If the bytes are not a valid single UTF-8 character, nullopt is returned
     * @return
     */
    auto utf8_to_rune(const std::u8string_view&) -> std::optional<char32_t>;

    /**
     * Converts UTF-16 bytes to a rune (UTF-32 character)
     * If the bytes are not a valid single UTF-16 character, nullopt is returned
     * @return
     */
    auto utf16_to_rune(const std::u16string_view&) -> std::optional<char32_t>;
}
