#pragma once

namespace unicode::encoding::props {
    /**
     * Returns whether a 16-bit character is a surrogate (high or low)
     * @param ch
     * @return
     */
    constexpr auto is_surrogate(char16_t ch) -> bool { return ch >= 0xD800 && ch <= 0xDFFF; }

    /**
     * Returns whether a 16-bit character is a high surrogate (start of surrogate pair)
     * @param ch
     * @return
     */
    constexpr auto is_high_surrogate(char16_t ch) -> bool { return ch >= 0xD800 && ch <= 0xDBFF; }

    /**
     * Returns whether a 16-bit character is a low surrogate (end of surrogate pair)
     * @param ch
     * @return
     */
    constexpr auto is_low_surrogate(char16_t ch) -> bool { return ch >= 0xDC00 && ch <= 0xDFFF; }
}
