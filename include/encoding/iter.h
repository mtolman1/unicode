#pragma clang diagnostic push
#pragma ide diagnostic ignored "cppcoreguidelines-avoid-const-or-ref-data-members"
#pragma once

#include <string>
#include <tuple>
#include <optional>
#include <bit>

namespace unicode::encoding::iter {

    namespace impl {
        /**
         * Read iterator for strings which will iterate over a string getting all of the runes
         * Assumes UTF-8 encoding for strings
         */
        class StrToUtf32Iterator {
        private:
            const std::string_view &strRef;
            size_t strIndex = 0;
            std::tuple<char32_t, size_t> currentRunePos;

            auto read_rune() -> void;

        public:
            using iterator_category [[maybe_unused]] = std::forward_iterator_tag;
            using difference_type [[maybe_unused]] = std::ptrdiff_t;
            using value_type = char32_t;
            using pointer = const std::tuple<char32_t, size_t> *;  // or also value_type*
            using reference = const std::tuple<char32_t, size_t> &;  // or also value_type&

            StrToUtf32Iterator(const std::string_view &str, bool end);

            auto operator<=>(const StrToUtf32Iterator &o) const -> std::partial_ordering;

            auto operator==(const StrToUtf32Iterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::equivalent;
            }

            auto operator!=(const StrToUtf32Iterator &o) const -> bool {
                return (*this <=> o) != std::partial_ordering::equivalent;
            }

            auto operator<(const StrToUtf32Iterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::less;
            }

            auto operator>(const StrToUtf32Iterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::greater;
            }

            auto operator<=(const StrToUtf32Iterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::greater && cmp != std::partial_ordering::unordered;
            }

            auto operator>=(const StrToUtf32Iterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::less && cmp != std::partial_ordering::unordered;
            }

            auto operator*() const -> reference { return currentRunePos; }

            auto operator->() const -> pointer { return &currentRunePos; }

            auto operator++() -> StrToUtf32Iterator & {
                read_rune();
                return *this;
            }

            auto operator++(int) -> StrToUtf32Iterator {
                auto cpy = *this;
                ++(*this);
                return cpy;
            }
        };

        /**
         * Read iterator for UTF-8 strings which will iterate over a string getting all of the runes
         */
        class Utf8ToUtf32Iterator {
        private:
            const std::u8string_view &strRef;
            size_t strIndex = 0;
            std::tuple<char32_t, size_t> currentRunePos;

            auto read_rune() -> void;

        public:
            using iterator_category [[maybe_unused]] = std::forward_iterator_tag;
            using difference_type [[maybe_unused]] = std::ptrdiff_t;
            using value_type = char32_t;
            using pointer = const std::tuple<char32_t, size_t> *;  // or also value_type*
            using reference = const std::tuple<char32_t, size_t> &;  // or also value_type&

            Utf8ToUtf32Iterator(const std::u8string_view &str, bool end);

            auto operator<=>(const Utf8ToUtf32Iterator &o) const -> std::partial_ordering;

            auto operator==(const Utf8ToUtf32Iterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::equivalent;
            }

            auto operator!=(const Utf8ToUtf32Iterator &o) const -> bool {
                return (*this <=> o) != std::partial_ordering::equivalent;
            }

            auto operator<(const Utf8ToUtf32Iterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::less;
            }

            auto operator>(const Utf8ToUtf32Iterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::greater;
            }

            auto operator<=(const Utf8ToUtf32Iterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::greater && cmp != std::partial_ordering::unordered;
            }

            auto operator>=(const Utf8ToUtf32Iterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::less && cmp != std::partial_ordering::unordered;
            }

            auto operator*() const -> reference { return currentRunePos; }

            auto operator->() const -> pointer { return &currentRunePos; }

            auto operator++() -> Utf8ToUtf32Iterator & {
                read_rune();
                return *this;
            }

            auto operator++(int) -> Utf8ToUtf32Iterator {
                auto cpy = *this;
                ++(*this);
                return cpy;
            }
        };

        /**
         * Read iterator for UTF-16 strings which will iterate over a string getting all of the runes
         */
        class Utf16ToUtf32Iterator {
        private:
            const std::u16string_view &strRef;
            size_t strIndex = 0;
            std::tuple<char32_t, size_t> currentRunePos{};
            bool reverseBom = false;

            auto read_rune() -> void;


        public:
            using iterator_category [[maybe_unused]] = std::forward_iterator_tag;
            using difference_type [[maybe_unused]] = std::ptrdiff_t;
            using value_type = char32_t;
            using pointer = const std::tuple<char32_t, size_t> *;  // or also value_type*
            using reference = const std::tuple<char32_t, size_t> &;  // or also value_type&

            Utf16ToUtf32Iterator(const std::u16string_view &str, bool end, bool reverseBom);

            auto operator<=>(const Utf16ToUtf32Iterator &o) const -> std::partial_ordering;

            auto operator==(const Utf16ToUtf32Iterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::equivalent;
            }

            auto operator!=(const Utf16ToUtf32Iterator &o) const -> bool {
                return (*this <=> o) != std::partial_ordering::equivalent;
            }

            auto operator<(const Utf16ToUtf32Iterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::less;
            }

            auto operator>(const Utf16ToUtf32Iterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::greater;
            }

            auto operator<=(const Utf16ToUtf32Iterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::greater && cmp != std::partial_ordering::unordered;
            }

            auto operator>=(const Utf16ToUtf32Iterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::less && cmp != std::partial_ordering::unordered;
            }

            auto operator*() const -> reference { return currentRunePos; }

            auto operator->() const -> pointer { return &currentRunePos; }

            auto operator++() -> Utf16ToUtf32Iterator & {
                read_rune();
                return *this;
            }

            auto operator++(int) -> Utf16ToUtf32Iterator {
                auto cpy = *this;
                ++(*this);
                return cpy;
            }
        };

        /**
         * Read iterator for UTF-16 strings which will iterate over all the characters of a UTF-16 string
         * and convert the UTF-16 characters to the machine's endian order
         */
        class Utf16EndianIterator {
        private:
            const std::u16string_view &strRef;
            size_t strIndex = 0;
            char16_t currentChar = 0;
            bool reverseBom = false;

            auto read_char() -> void;

        public:
            using iterator_category [[maybe_unused]] = std::forward_iterator_tag;
            using difference_type [[maybe_unused]] = std::ptrdiff_t;
            using value_type = char16_t;
            using pointer = const char16_t*;  // or also value_type*
            using reference = const char16_t &;  // or also value_type&

            Utf16EndianIterator(const std::u16string_view &str, bool end, bool reverseBom);

            auto operator<=>(const Utf16EndianIterator &o) const -> std::partial_ordering;

            auto operator==(const Utf16EndianIterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::equivalent;
            }

            auto operator!=(const Utf16EndianIterator &o) const -> bool {
                return (*this <=> o) != std::partial_ordering::equivalent;
            }

            auto operator<(const Utf16EndianIterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::less;
            }

            auto operator>(const Utf16EndianIterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::greater;
            }

            auto operator<=(const Utf16EndianIterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::greater && cmp != std::partial_ordering::unordered;
            }

            auto operator>=(const Utf16EndianIterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::less && cmp != std::partial_ordering::unordered;
            }

            auto operator*() const -> reference { return currentChar; }

            auto operator->() const -> pointer { return &currentChar; }

            auto operator++() -> Utf16EndianIterator & {
                read_char();
                return *this;
            }

            auto operator++(int) -> Utf16EndianIterator {
                auto cpy = *this;
                ++(*this);
                return cpy;
            }
        };

        /**
         * Read iterator for UTF-32 strings which will iterate over all the characters of a UTF-32 string
         * and convert the UTF-32 characters to the machine's endian order
         */
        class Utf32EndianIterator {
        private:
            const std::u32string_view &strRef;
            size_t strIndex = 0;
            char32_t currentChar = 0;
            bool reverseBom = false;

            auto read_char() -> void;

        public:
            using iterator_category [[maybe_unused]] = std::forward_iterator_tag;
            using difference_type [[maybe_unused]] = std::ptrdiff_t;
            using value_type = char32_t;
            using pointer = const char32_t*;  // or also value_type*
            using reference = const char32_t &;  // or also value_type&

            Utf32EndianIterator(const std::u32string_view &str, bool end, bool reverseBom);

            auto operator<=>(const Utf32EndianIterator &o) const -> std::partial_ordering;

            auto operator==(const Utf32EndianIterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::equivalent;
            }

            auto operator!=(const Utf32EndianIterator &o) const -> bool {
                return (*this <=> o) != std::partial_ordering::equivalent;
            }

            auto operator<(const Utf32EndianIterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::less;
            }

            auto operator>(const Utf32EndianIterator &o) const -> bool {
                return (*this <=> o) == std::partial_ordering::greater;
            }

            auto operator<=(const Utf32EndianIterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::greater && cmp != std::partial_ordering::unordered;
            }

            auto operator>=(const Utf32EndianIterator &o) const -> bool {
                auto cmp = (*this <=> o);
                return cmp != std::partial_ordering::less && cmp != std::partial_ordering::unordered;
            }

            auto operator*() const -> reference { return currentChar; }

            auto operator->() const -> pointer { return &currentChar; }

            auto operator++() -> Utf32EndianIterator & {
                read_char();
                return *this;
            }

            auto operator++(int) -> Utf32EndianIterator {
                auto cpy = *this;
                ++(*this);
                return cpy;
            }
        };
    }

    /**
     * Iterable for strings that can be used in a for-each loop
     * Each step returns a tuple of the unicode rune and the string index of that rune
     * Assumes UTF-8 encoding for strings
     */
    struct StrRuneIterable {
        const std::string_view strRef;

        using iterator = impl::StrToUtf32Iterator;
        using const_iterator = impl::StrToUtf32Iterator;

        auto begin() { return iterator(strRef, false); }
        auto end() { return iterator(strRef, true); }

        auto begin() const { return const_iterator(strRef, false); }
        auto end() const { return const_iterator(strRef, true); }

    };

    /**
     * Iterable for UTF-8 strings that can be used in a for-each loop
     * Each step returns a tuple of the unicode rune and the UTF-8 string index of that rune
     */
    struct Utf8RuneIterable {
        const std::u8string_view strRef;

        using iterator = impl::Utf8ToUtf32Iterator;
        using const_iterator = impl::Utf8ToUtf32Iterator;

        auto begin() { return iterator(strRef, false); }
        auto end() { return iterator(strRef, true); }

        auto begin() const { return const_iterator(strRef, false); }
        auto end() const { return const_iterator(strRef, true); }
    };

    /**
     * Iterable for UTF-16 strings that can be used in a for-each loop
     * Each step returns a tuple of the unicode rune and the UTF-16 string index of that rune
     */
    struct Utf16RuneIterable {
        using iterator = impl::Utf16ToUtf32Iterator;
        using const_iterator = impl::Utf16ToUtf32Iterator;

        auto begin() { return iterator(strRef, false, reverse); }
        auto end() { return iterator(strRef, true, reverse); }

        auto begin() const { return const_iterator(strRef, false, reverse); }
        auto end() const { return const_iterator(strRef, true, reverse); }

        Utf16RuneIterable() = default;
        Utf16RuneIterable(std::u16string_view);
        [[maybe_unused]] Utf16RuneIterable(std::u16string_view sv, std::endian encodingEndian) : strRef(sv), reverse(std::endian::native != encodingEndian) {}
    private:
        const std::u16string_view strRef = {};
        const bool reverse = false;
    };

    /**
     * Iterable for UTF-16 strings that returns the UTF-16 characters in the current machine's endian order
     */
    struct Utf16EndianIterable {
        using iterator = impl::Utf16EndianIterator;
        using const_iterator = impl::Utf16EndianIterator;

        auto begin() { return iterator(strRef, false, reverse); }
        auto end() { return iterator(strRef, true, reverse); }

        auto begin() const { return const_iterator(strRef, false, reverse); }
        auto end() const { return const_iterator(strRef, true, reverse); }

        Utf16EndianIterable() = default;
        Utf16EndianIterable(std::u16string_view);
        Utf16EndianIterable(std::u16string_view sv, std::endian encodingEndian) : strRef(sv), reverse(std::endian::native != encodingEndian) {}
    private:
        const std::u16string_view strRef = {};
        const bool reverse = false;
    };

    /**
     * Iterable for UTF-16 strings that returns the UTF-16 characters in the target endian order
     */
    struct Utf16TargetEndianIterable {
        using iterator = impl::Utf16EndianIterator;
        using const_iterator = impl::Utf16EndianIterator;

        auto begin() { return iterator(strRef, false, reverse); }
        auto end() { return iterator(strRef, true, reverse); }

        auto begin() const { return const_iterator(strRef, false, reverse); }
        auto end() const { return const_iterator(strRef, true, reverse); }

        Utf16TargetEndianIterable() = default;
        Utf16TargetEndianIterable(std::endian targetEndian, std::u16string_view);
        [[maybe_unused]] Utf16TargetEndianIterable(std::endian targetEndian, std::u16string_view sv, std::endian sourceEndian) : strRef(sv), reverse(targetEndian != sourceEndian) {}
    private:
        const std::u16string_view strRef = {};
        const bool reverse = false;
    };

    /**
     * Iterable for UTF-32 strings that returns the UTF-32 characters in the current machine's endian order
     */
    struct Utf32EndianIterable {
        using iterator = impl::Utf32EndianIterator;
        using const_iterator = impl::Utf32EndianIterator;

        auto begin() { return iterator(strRef, false, reverse); }
        auto end() { return iterator(strRef, true, reverse); }

        auto begin() const { return const_iterator(strRef, false, reverse); }
        auto end() const { return const_iterator(strRef, true, reverse); }

        Utf32EndianIterable() = default;
        Utf32EndianIterable(std::u32string_view);
        [[maybe_unused]] Utf32EndianIterable(std::u32string_view sv, std::endian encodingEndian) : strRef(sv), reverse(std::endian::native != encodingEndian) {}
    private:
        const std::u32string_view strRef = {};
        const bool reverse = false;
    };

    /**
     * Iterable for UTF-32 strings that returns the UTF-32 characters in the target endian order
     */
    struct Utf32TargetEndianIterable {
        using iterator = impl::Utf32EndianIterator;
        using const_iterator = impl::Utf32EndianIterator;

        auto begin() { return iterator(strRef, false, reverse); }
        auto end() { return iterator(strRef, true, reverse); }

        auto begin() const { return const_iterator(strRef, false, reverse); }
        auto end() const { return const_iterator(strRef, true, reverse); }

        Utf32TargetEndianIterable() = default;
        Utf32TargetEndianIterable(std::endian targetEndian, std::u32string_view);
        [[maybe_unused]] Utf32TargetEndianIterable(std::endian targetEndian, std::u32string_view sv, std::endian sourceEndian) : strRef(sv), reverse(targetEndian != sourceEndian) {}
    private:
        const std::u32string_view strRef = {};
        const bool reverse = false;
    };
}

#pragma clang diagnostic pop