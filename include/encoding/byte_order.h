#pragma once

#include <string>
#include <bit>
#include <optional>

namespace unicode::encoding::byte_order {

    constexpr char32_t bom = 0xFEFF;

    namespace endian {
        constexpr auto machine = std::endian::native;
        constexpr auto opposite = std::endian::native == std::endian::little ? std::endian::big : std::endian::little;
    }

    /**
     * Returns whether a character is a byte-order-mark (BOM) in the current machine's endian order
     * @param ch Unicode character to check
     * @return
     */
    constexpr auto is_bom(char32_t ch) -> bool { return ch == bom; }

    /**
     * Returns whether a character is a reversed 16-bit byte-order-mark (BOM)
     * @param ch Unicode character to check
     * @return
     */
    constexpr auto is_bom_rev_16_bit(char16_t ch) -> bool { return ch == 0xFFFE; }

    /**
     * Returns whether a character is a reversed 32-bit byte-order-mark (BOM)
     * @param ch Unicode character to check
     * @return
     */
    constexpr auto is_bom_rev_32_bit(char32_t ch) -> bool { return ch == 0xFFFE0000; }

    /**
     * Detects the endianness from a UTF-16 Byte Order Marker character
     * If character is not a BOM (either endian), will return std::nullopt
     * Allows testing that a string starts with a BOM, and if so returns the endian ordering
     * @param ch UTF-16 BOM character
     * @return
     */
    constexpr auto detect_endian_ch16(char16_t ch) -> std::optional<std::endian> {
        if (is_bom(ch)) {
            return endian::machine;
        }
        else if (is_bom_rev_16_bit(ch)) {
            return endian::opposite;
        }
        else {
            return std::nullopt;
        }
    }

    /**
     * Detects the endianness from a UTF-32 Byte Order Marker character
     * If character is not a BOM (either endian), will return std::nullopt
     * Allows testing that a string starts with a BOM, and if so returns the endian ordering
     * @param ch UTF-32 BOM character
     * @return
     */
    constexpr auto detect_endian_ch32(char32_t ch) -> std::optional<std::endian> {
        if (is_bom(ch)) {
            return endian::machine;
        }
        else if (is_bom_rev_32_bit(ch)) {
            return endian::opposite;
        }
        else {
            return std::nullopt;
        }
    }

    /**
     * Flips the endian order for a single 16-bit character
     * @param ch 16 bit character to flip endian order
     * @return Flipped character
     */
    auto flip_endian_ch16(char16_t ch) -> char16_t;

    /**
     * Flips the endian order for a single 32-bit character
     * @param ch 32 bit character to flip endian order
     * @return Flipped character
     */
    auto flip_endian_ch32(char32_t ch) -> char32_t;

    /**
     * Flips endian order for a UTF-16 string (in-place)
     * @param str String to flip endian for
     */
    auto flip_endian(std::u16string& str) -> void;

    /**
     * Flips endian order for a UTF-32 string (in-place)
     * @param str String to flip endian for
     */
    auto flip_endian(std::u32string& str) -> void;

    /**
     * Adds a byte-order-mark to the start of a string if not already there (in-place)
     * Note: BOM will be in current machine's byte order, not always the string's byte order
     * @param str String to add BOM to
     */
    auto add_bom(std::u16string& str) -> void;

    /**
     * Adds a byte-order-mark to the start of a string if not already there (in-place)
     * Note: BOM will be in current machine's byte order, not always the string's byte order
     * @param str String to add BOM to
     */
    auto add_bom(std::u32string& str) -> void;

    /**
     * Removes a byte-order-mark from the start of a string if present (in-place)
     * Note: Only checks for BOM in current machine's byte order, will not remove a reversed BOM
     * @param str String to remove BOM from
     */
    auto remove_bom(std::u16string& str) -> void;

    /**
     * Removes a byte-order-mark from the start of a string if present (in-place)
     * Note: Only checks for BOM in current machine's byte order, will not remove a reversed BOM
     * @param str String to remove BOM from
     */
    auto remove_bom(std::u32string& str) -> void;
}
