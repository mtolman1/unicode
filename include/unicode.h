#pragma once

#include "db.h"
#include "encoding.h"
#include "layers.h"

namespace unicode {
    auto version() -> const char*;
}
