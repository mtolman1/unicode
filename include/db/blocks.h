#pragma once

#include <string>
#include <vector>
#include <optional>

namespace unicode::db::block {
    struct Block {
        [[maybe_unused]] char32_t firstCodePoint;
        [[maybe_unused]] char32_t lastCodePoint;
        [[maybe_unused]] const char* name;
    };

    auto blocks() -> std::vector<Block>;

    auto block_for(char32_t) noexcept -> std::optional<Block>;

    auto block_by_id(size_t blockId) noexcept -> std::optional<Block>;
}
