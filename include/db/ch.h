#pragma once

#include "ch/basic.h"
#include "ch/bidi.h"
#include "ch/break.h"
#include "ch/casing.h"
#include "ch/indic.h"
#include "ch/join.h"
#include "ch/meta.h"
#include "ch/metax.h"
#include "ch/misc.h"
#include "ch/normalize.h"
#include "ch/numeric.h"
#include "ch/nushu.h"
#include "ch/script.h"
#include "ch/tangut.h"
#include "ch/unihan.h"

namespace unicode::db::ch {
    /**
     * Holds all Unihan properties for a single character
     */
    struct UnihanProps {
        [[maybe_unused]] const char *kAccountingNumeric;
        [[maybe_unused]] const char *kAlternateHanYu;
        [[maybe_unused]] const char *kAlternateJEF;
        [[maybe_unused]] const char *kAlternateKangXi;
        [[maybe_unused]] const char *kAlternateMorohashi;
        [[maybe_unused]] const char *kAlternateTotalStrokes;
        [[maybe_unused]] const char *kBigFive;
        [[maybe_unused]] const char *kCCCII;
        [[maybe_unused]] const char *kCNS1986;
        [[maybe_unused]] const char *kCNS1992;
        [[maybe_unused]] const char *kCangjie;
        [[maybe_unused]] const char *kCantonese;
        [[maybe_unused]] const char *kCheungBauer;
        [[maybe_unused]] const char *kCheungBauerIndex;
        [[maybe_unused]] const char *kCihaiT;
        [[maybe_unused]] const char *kCompatibilityVariant;
        [[maybe_unused]] const char *kCowles;
        [[maybe_unused]] const char *kDaeJaweon;
        [[maybe_unused]] const char *kDefinition;
        [[maybe_unused]] const char *kEACC;
        [[maybe_unused]] const char *kFenn;
        [[maybe_unused]] const char *kFennIndex;
        [[maybe_unused]] const char *kFourCornerCode;
        [[maybe_unused]] const char *kFrequency;
        [[maybe_unused]] const char *kGB0;
        [[maybe_unused]] const char *kGB1;
        [[maybe_unused]] const char *kGB3;
        [[maybe_unused]] const char *kGB5;
        [[maybe_unused]] const char *kGB7;
        [[maybe_unused]] const char *kGB8;
        [[maybe_unused]] const char *kGradeLevel;
        [[maybe_unused]] const char *kGSR;
        [[maybe_unused]] const char8_t *kHangul;
        [[maybe_unused]] const char *kHanYu;
        [[maybe_unused]] const char8_t *kHanyuPinlu;
        [[maybe_unused]] const char8_t *kHanyuPinyin;
        [[maybe_unused]] const char8_t *kHDZRadBreak;
        [[maybe_unused]] const char *kHKGlyph;
        [[maybe_unused]] const char *kHKSCS;
        [[maybe_unused]] const char *kIBMJapan;
        [[maybe_unused]] const char *kIICore;
        [[maybe_unused]] const char *kIRGDaeJaweon;
        [[maybe_unused]] const char *kIRGDaiKanwaZiten;
        [[maybe_unused]] const char *kIRGHanyuDaZidian;
        [[maybe_unused]] const char *kIRGKangXi;
        [[maybe_unused]] const char *kIRG_GSource;
        [[maybe_unused]] const char *kIRG_HSource;
        [[maybe_unused]] const char *kIRG_JSource;
        [[maybe_unused]] const char *kIRG_KPSource;
        [[maybe_unused]] const char *kIRG_KSource;
        [[maybe_unused]] const char *kIRG_MSource;
        [[maybe_unused]] const char *kIRG_SSource;
        [[maybe_unused]] const char *kIRG_TSource;
        [[maybe_unused]] const char *kIRG_USource;
        [[maybe_unused]] const char *kIRG_UKSource;
        [[maybe_unused]] const char *kIRG_VSource;
        [[maybe_unused]] const char *kJa;
        [[maybe_unused]] const char *kJHJ;
        [[maybe_unused]] const char *kJinmeiyoKanji;
        [[maybe_unused]] const char *kJoyoKanji;
        [[maybe_unused]] const char *kKoreanEducationHanja;
        [[maybe_unused]] const char *kKoreanName;
        [[maybe_unused]] const char *kTGH;
        [[maybe_unused]] const char *kJIS0213;
        [[maybe_unused]] const char *kJapaneseKun;
        [[maybe_unused]] const char *kJapaneseOn;
        [[maybe_unused]] const char *kJis0;
        [[maybe_unused]] const char *kJis1;
        [[maybe_unused]] const char *kKPS0;
        [[maybe_unused]] const char *kKPS1;
        [[maybe_unused]] const char *kKSC0;
        [[maybe_unused]] const char *kKangXi;
        [[maybe_unused]] const char *kKarlgren;
        [[maybe_unused]] const char *kKorean;
        [[maybe_unused]] const char *kLau;
        [[maybe_unused]] const char *kMainlandTelegraph;
        [[maybe_unused]] const char8_t *kMandarin;
        [[maybe_unused]] const char *kMatthews;
        [[maybe_unused]] const char *kMeyerWempe;
        [[maybe_unused]] const char *kMorohashi;
        [[maybe_unused]] const char *kNelson;
        [[maybe_unused]] const char *kOtherNumeric;
        [[maybe_unused]] const char *kPhonetic;
        [[maybe_unused]] const char *kPrimaryNumeric;
        [[maybe_unused]] const char *kPseudoGB1;
        [[maybe_unused]] const char *kRSAdobe_Japan1_6;
        [[maybe_unused]] const char *kRSJapanese;
        [[maybe_unused]] const char *kRSKanWa;
        [[maybe_unused]] const char *kRSKangXi;
        [[maybe_unused]] const char *kRSKorean;
        [[maybe_unused]] const char *kRSMerged;
        [[maybe_unused]] const char *kRSUnicode;
        [[maybe_unused]] const char *kSBGY;
        [[maybe_unused]] const char *kSemanticVariant;
        [[maybe_unused]] const char *kSimplifiedVariant;
        [[maybe_unused]] const char *kSpecializedSemanticVariant;
        [[maybe_unused]] const char *kSpoofingVariant;
        [[maybe_unused]] const char *kTaiwanTelegraph;
        [[maybe_unused]] const char8_t *kTang;
        [[maybe_unused]] const char8_t *kTGHZ2013;
        [[maybe_unused]] const char *kTotalStrokes;
        [[maybe_unused]] const char *kTraditionalVariant;
        [[maybe_unused]] const char *kUnihanCore2020;
        [[maybe_unused]] const char8_t *kVietnamese;
        [[maybe_unused]] const char8_t *kXHC1983;
        [[maybe_unused]] const char *kXerox;
        [[maybe_unused]] const char *kZVariant;
        [[maybe_unused]] const char *kStrange;
    };

    /**
     * Holds all Tangut properties for a single character
     */
    struct TangutData {
        [[maybe_unused]] const char *kRSTUnicode;
        [[maybe_unused]] const char *kTGT_MergedSrc;
    };

    /**
     * Holds all Nushu properties for a single character
     */
    struct NushuData {
        [[maybe_unused]] const char *kSrc_NushuDuben;
        [[maybe_unused]] const char *kReading;
    };

    /**
     * Holds all Unicode properties for a single character
     * Includes Unihan, Tangut, and Nushu data as well
     * **Note:** Recommended to get individual properties for performance
     */
    struct Props {
        [[maybe_unused]] const char *age;
        [[maybe_unused]] const char *name;
        [[maybe_unused]] std::vector<Alias> aliases;
        [[maybe_unused]] Block block = Block::ASCII; // blk
        [[maybe_unused]] GeneralCategory generalCategory = GeneralCategory::CO; // gc
        [[maybe_unused]] std::optional<uint8_t> combiningProperties; // ccc
        [[maybe_unused]] std::optional<BidiClass> bidiClass; // bc
        [[maybe_unused]] bool bidiMirrored; // Bidi_M
        [[maybe_unused]] char32_t bidiMirroredGlyph = U'\0'; // bmg
        [[maybe_unused]] bool bidiControl; // Bidi_C
        [[maybe_unused]] std::optional<BidiPairedBracketType> bidiPairedBracketType; // bpt
        [[maybe_unused]] char32_t bidiPairedBracketProperties; // bpb; '#' maps to itself
        [[maybe_unused]] const char32_t *decompositionMapping; // dm, '#' maps to itself
        [[maybe_unused]] bool compositionExclusion; // CE
        [[maybe_unused]] bool fullCompositionExclusion; // Comp_Ex
        [[maybe_unused]] NumericType numericType = NumericType::NONE; // nt
        [[maybe_unused]] NFC_QC nfcQuickCheck = NFC_QC::N; // NFC_QC
        [[maybe_unused]] NFD_QC nfdQuickCheck = NFD_QC::N; // NFD_QC
        [[maybe_unused]] NFKC_QC nfkcQuickCheck = NFKC_QC::N; // NFKC_QC
        [[maybe_unused]] NFKD_QC nfkdQuickCheck = NFKD_QC::N; // NFKD_QC
        [[maybe_unused]] bool expandOnNfc; // XO_NFC
        [[maybe_unused]] bool expandOnNfd; // XO_NFD
        [[maybe_unused]] bool expandOnNfkc; // XO_NFKC
        [[maybe_unused]] bool expandOnNfkd; // XO_NFKD
        [[maybe_unused]] const char32_t *fcNfkcClosure; // FC_NFKC, '#' maps to itself
        [[maybe_unused]] const char *numericValue = ""; // NaN is "no value"
        // TODO: add parsing
//            double numericValueRealized; // NaN is "no value"
        [[maybe_unused]] std::optional<JoinType> joinType; // jt
        [[maybe_unused]] std::optional<JoiningGroup> joiningGroup; // jg
        [[maybe_unused]] bool joinControl; // Join_C
        [[maybe_unused]] std::optional<LineBreak> lineBreak; // lb
        [[maybe_unused]] std::optional<EastAsianWidth> eastAsianWidth; // ea
        [[maybe_unused]] bool isUpper; // Upper
        [[maybe_unused]] bool isLower; // Lower
        [[maybe_unused]] bool isOtherUpper; // OUpper
        [[maybe_unused]] bool isOtherLower; // OLower
        [[maybe_unused]] char32_t simpleUpperCase; // suc, '#' maps to itself
        [[maybe_unused]] char32_t simpleLowerCase; // slc, '#' maps to itself
        [[maybe_unused]] char32_t simpleTitleCase; // stc, '#' maps to itself
        [[maybe_unused]] char32_t simpleCaseFolding; // sfc, '#' maps to itself
        [[maybe_unused]] const char32_t *upperCase; // uc, '#' maps to itself
        [[maybe_unused]] const char32_t *lowerCase; // lc, '#' maps to itself
        [[maybe_unused]] const char32_t *titleCase; // tc, '#' maps to itself
        [[maybe_unused]] const char32_t *caseFolding; // cf, '#' maps to itself
        [[maybe_unused]] const char32_t *nfkcCaseFold; // NFKC_CF, '#' maps to itself
        [[maybe_unused]] bool caseIgnorable; // CI
        [[maybe_unused]] bool cased; // Cased
        [[maybe_unused]] bool changesWhenCaseFolded; // CWCF
        [[maybe_unused]] bool changesWhenCaseMapped; // CWCM
        [[maybe_unused]] bool changesWhenLowerCased; // CWL
        [[maybe_unused]] bool changesWhenNfkcCaseFolded; // CWKCF
        [[maybe_unused]] bool changesWhenTitleCased; // CWT
        [[maybe_unused]] bool changesWhenUpperCased; // CWU
        [[maybe_unused]] std::optional<Script> script; // sc, set to nullptr when no script is available
        [[maybe_unused]] std::vector<Script> scriptExtensions; // scx
        [[maybe_unused]] const char *iso10646Comment; // isc
        [[maybe_unused]] std::optional<HangulSyllableType> hangulSyllableType; // hst
        [[maybe_unused]] const char *jamoShortName; // JSN
        [[maybe_unused]] std::optional<IndicSyllabicCategory> indicSyllabicCategory; // InSC
        [[maybe_unused]] std::optional<IndicMantraCategory> indicMantraCategory; // InMC
        [[maybe_unused]] std::optional<IndicPositionalCategory> indicPositionalCategory; // InPC

        // Identifier and pattern and programming language properties
        [[maybe_unused]] bool identifierStart; // IDS
        [[maybe_unused]] bool otherIdentifierStart; // OIDS
        [[maybe_unused]] bool extendedIdentifierStart; // XIDS
        [[maybe_unused]] bool identifierContinue; // IDC
        [[maybe_unused]] bool otherIdentifierContinue; // OIDC
        [[maybe_unused]] bool extendedIdentifierContinue; // XIDC
        [[maybe_unused]] bool patternSyntax; // Pat_Syn
        [[maybe_unused]] bool patternWhiteSpace; // Pat_WS
        [[maybe_unused]] bool isDash; // Dash
        [[maybe_unused]] bool isHyphen; // Hyphen
        [[maybe_unused]] bool isQuotationMark; // QMark
        [[maybe_unused]] bool isTerminalPunctuation; // Term
        [[maybe_unused]] bool isSentenceTerminal; // STerm
        [[maybe_unused]] bool isDiacritic; // Dia
        [[maybe_unused]] bool isExtender; // Ext
        [[maybe_unused]] bool isPrependedConcatenationMark; // PCM
        [[maybe_unused]] bool isSoftDotted; // SD
        [[maybe_unused]] bool isAlphabetic; // Alpha
        [[maybe_unused]] bool isOtherAlphabetic; // OAlpha
        [[maybe_unused]] bool isMath; // Math
        [[maybe_unused]] bool isOtherMath; // OMath
        [[maybe_unused]] bool isHex; // Hex
        [[maybe_unused]] bool isAsciiHex; // AHex
        [[maybe_unused]] bool defaultIgnorableCodePoint; // DI
        [[maybe_unused]] bool otherDefaultIgnorableCodePoint; // ODI
        [[maybe_unused]] bool logicalOrderException; // LOE
        [[maybe_unused]] bool isWhiteSpace; // WSpace
        [[maybe_unused]] std::optional<VerticalOrientation> verticalOrientation; // vo
        [[maybe_unused]] bool regionalIndicator; // RI
        [[maybe_unused]] bool graphemeBase; // Gr_Base
        [[maybe_unused]] bool graphemeExtend; // Gr_Ext
        [[maybe_unused]] bool otherGraphemeExtend; // OGr_Ext
        [[maybe_unused]] bool graphemeLink; // Gr_Link
        [[maybe_unused]] std::optional<GraphemeClusterBreak> graphemeClusterBreak; // GCB
        [[maybe_unused]] std::optional<WordBreak> wordBreak; // WB
        [[maybe_unused]] std::optional<SentenceBreak> sentenceBreak; // SB
        [[maybe_unused]] char32_t equivalentUnifiedIdeograph; // EqUIdeo
        [[maybe_unused]] bool ideographic; // Ideo
        [[maybe_unused]] bool unifiedIdeograph; // UIdeo
        [[maybe_unused]] bool idsBinaryOperator; // IDSB
        [[maybe_unused]] bool idsTrinaryOperator; // IDST
        [[maybe_unused]] bool radical; // Radical
        [[maybe_unused]] bool variationSelector; // VS
        [[maybe_unused]] bool nonCharacterCodePoint; // NChar
        [[maybe_unused]] bool deprecated; // Dep
        [[maybe_unused]] UnihanProps unihanProps; // nullptr if no unihan props are available
        [[maybe_unused]] TangutData tangutData;
        [[maybe_unused]] NushuData nushuData;
        [[maybe_unused]] bool emoji; // Emoji
        [[maybe_unused]] bool emojiPresentation; // EPress
        [[maybe_unused]] bool emojiModifier; // EMod
        [[maybe_unused]] bool emojiModifierBase; // EBase
        [[maybe_unused]] bool emojiComponent; // EComp
        [[maybe_unused]] bool extendedPictograph; // ExtPict
        [[maybe_unused]] std::optional<DecompositionType> decompositionType; // dt
    };

    /**
     * Gets all unihan properties for a character
     * @return
     */
    auto unihan_props(char32_t) -> UnihanProps;

    /**
     * Gets all tangut properties for a character
     * @return
     */
    auto tangut_data(char32_t) -> TangutData;

    /**
     * Gets all nushu properties for a character
     * @return
     */
    auto nushu_data(char32_t) -> NushuData;

    /**
     * Gets all unicode properties for a character
     * @return
     */
    auto props(char32_t) -> Props;
}
