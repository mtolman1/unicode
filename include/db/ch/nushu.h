#pragma once
#pragma clang diagnostic push
#pragma ide diagnostic ignored "readability-identifier-naming"

namespace unicode::db::ch {
    /**
     * Returns the kSrc_NushuDuben property, or nullptr if not present
     * @return
     */
    auto kSrc_NushuDuben(char32_t) -> const char *;

    /**
     * Returns the kReading property, or nullptr if not present
     * @return
     */
    auto kReading(char32_t) -> const char *;
}

#pragma clang diagnostic pop