#pragma once

#include "db/ch/defs.h"

#include <string_view>
#include <set>
#include <vector>

namespace unicode::db::ch {
    /**
     * Age of the character (aka. version of unicode it was introduced in)
     * @return
     */
    auto age(char32_t) -> const char *;

    /**
     * Returns all aliases for a character (order is not guaranteed)
     * @return
     */
    auto aliases(char32_t) -> std::vector<Alias>;

    /**
     * Returns the unicode block for a character
     * @return
     */
    auto block(char32_t) -> Block;

    /**
     * Returns the full, official name of a character
     * @return
     */
    auto name(char32_t) -> const char *;

    /**
     * Returns the general category of a character
     * @return
     */
    auto general_category(char32_t) -> GeneralCategory;

    /**
     * Returns all of the characters in a block
     * @return
     */
    auto chars_by_block(Block) -> std::set<char32_t>;

    /**
     * Returns all of the characters in a general category
     * @return
     */
    auto chars_by_general_category(GeneralCategory) -> std::set<char32_t>;

    /**
     * Returns a character by its name (or nullopt if there is no character by a name)
     * @return
     */
    auto char_by_name(const std::string_view &) -> std::optional<char32_t>;
}