#pragma once
#pragma clang diagnostic push
#pragma ide diagnostic ignored "readability-identifier-naming"

namespace unicode::db::ch {

    /**
     * Returns the kBigFive property, or nullptr if not present
     * @return
     */
    auto kBigFive(char32_t) -> const char *;

    /**
     * Returns the kIRGKangXi property, or nullptr if not present
     * @return
     */
    auto kIRGKangXi(char32_t) -> const char *;

    /**
     * Returns the kRSKorean property, or nullptr if not present
     * @return
     */
    auto kRSKorean(char32_t) -> const char *;

    /**
     * Returns the kAccountingNumeric property, or nullptr if not present
     * @return
     */
    auto kAccountingNumeric(char32_t) -> const char *;

    /**
     * Returns the kAlternateKangXi property, or nullptr if not present
     * @return
     */
    auto kAlternateKangXi(char32_t) -> const char *;

    /**
     * Returns the kAlternateHanYu property, or nullptr if not present
     * @return
     */
    auto kAlternateHanYu(char32_t) -> const char *;

    /**
     * Returns the kAlternateJEF property, or nullptr if not present
     * @return
     */
    auto kAlternateJEF(char32_t) -> const char *;

    /**
     * Returns the kAlternateMorohashi property, or nullptr if not present
     * @return
     */
    auto kAlternateMorohashi(char32_t) -> const char *;

    /**
     * Returns the kAlternateTotalStrokes property, or nullptr if not present
     * @return
     */
    auto kAlternateTotalStrokes(char32_t) -> const char *;

    /**
     * Returns the kCCCII property, or nullptr if not present
     * @return
     */
    auto kCCCII(char32_t) -> const char *;

    /**
     * Returns the kCNS1986 property, or nullptr if not present
     * @return
     */
    auto kCNS1986(char32_t) -> const char *;

    /**
     * Returns the kCNS1992 property, or nullptr if not present
     * @return
     */
    auto kCNS1992(char32_t) -> const char *;

    /**
     * Returns the kCangjie property, or nullptr if not present
     * @return
     */
    auto kCangjie(char32_t) -> const char *;

    /**
     * Returns the kCantonese property, or nullptr if not present
     * @return
     */
    auto kCantonese(char32_t) -> const char *;

    /**
     * Returns the kCheungBauer property, or nullptr if not present
     * @return
     */
    auto kCheungBauer(char32_t) -> const char *;

    /**
     * Returns the kCheungBauerIndex property, or nullptr if not present
     * @return
     */
    auto kCheungBauerIndex(char32_t) -> const char *;

    /**
     * Returns the kCihaiT property, or nullptr if not present
     * @return
     */
    auto kCihaiT(char32_t) -> const char *;

    /**
     * Returns the kCompatibilityVariant property, or nullptr if not present
     * @return
     */
    auto kCompatibilityVariant(char32_t) -> const char *;

    /**
     * Returns the kCowles property, or nullptr if not present
     * @return
     */
    auto kCowles(char32_t) -> const char *;

    /**
     * Returns the kDaeJaweon property, or nullptr if not present
     * @return
     */
    auto kDaeJaweon(char32_t) -> const char *;

    /**
     * Returns the kDefinition property, or nullptr if not present
     * @return
     */
    auto kDefinition(char32_t) -> const char *;

    /**
     * Returns the kEACC property, or nullptr if not present
     * @return
     */
    auto kEACC(char32_t) -> const char *;

    /**
     * Returns the kFenn property, or nullptr if not present
     * @return
     */
    auto kFenn(char32_t) -> const char *;

    /**
     * Returns the kFennIndex property, or nullptr if not present
     * @return
     */
    auto kFennIndex(char32_t) -> const char *;

    /**
     * Returns the kFourCornerCode property, or nullptr if not present
     * @return
     */
    auto kFourCornerCode(char32_t) -> const char *;

    /**
     * Returns the kFrequency property, or nullptr if not present
     * @return
     */
    auto kFrequency(char32_t) -> const char *;

    /**
     * Returns the kGB0 property, or nullptr if not present
     * @return
     */
    auto kGB0(char32_t) -> const char *;

    /**
     * Returns the kGB1 property, or nullptr if not present
     * @return
     */
    auto kGB1(char32_t) -> const char *;

    /**
     * Returns the kGB3 property, or nullptr if not present
     * @return
     */
    auto kGB3(char32_t) -> const char *;

    /**
     * Returns the kGB5 property, or nullptr if not present
     * @return
     */
    auto kGB5(char32_t) -> const char *;

    /**
     * Returns the kGB7 property, or nullptr if not present
     * @return
     */
    auto kGB7(char32_t) -> const char *;

    /**
     * Returns the kGB8 property, or nullptr if not present
     * @return
     */
    auto kGB8(char32_t) -> const char *;

    /**
     * Returns the kGSR property, or nullptr if not present
     * @return
     */
    auto kGSR(char32_t) -> const char *;

    /**
     * Returns the kGradeLevel property, or nullptr if not present
     * @return
     */
    auto kGradeLevel(char32_t) -> const char *;

    /**
     * Returns the kHDZRadBreak property, or nullptr if not present
     * @return
     */
    auto kHDZRadBreak(char32_t) -> const char8_t *;

    /**
     * Returns the kHKGlyph property, or nullptr if not present
     * @return
     */
    auto kHKGlyph(char32_t) -> const char *;

    /**
     * Returns the kHKSCS property, or nullptr if not present
     * @return
     */
    auto kHKSCS(char32_t) -> const char *;

    /**
     * Returns the kHanYu property, or nullptr if not present
     * @return
     */
    auto kHanYu(char32_t) -> const char *;

    /**
     * Returns the kHangul property, or nullptr if not present
     * @return
     */
    auto kHangul(char32_t) -> const char8_t *;

    /**
     * Returns the kHanyuPinlu property, or nullptr if not present
     * @return
     */
    auto kHanyuPinlu(char32_t) -> const char8_t *;

    /**
     * Returns the kHanyuPinyin property, or nullptr if not present
     * @return
     */
    auto kHanyuPinyin(char32_t) -> const char8_t *;

    /**
     * Returns the kIBMJapan property, or nullptr if not present
     * @return
     */
    auto kIBMJapan(char32_t) -> const char *;

    /**
     * Returns the kIICore property, or nullptr if not present
     * @return
     */
    auto kIICore(char32_t) -> const char *;

    /**
     * Returns the kIRGDaeJaweon property, or nullptr if not present
     * @return
     */
    auto kIRGDaeJaweon(char32_t) -> const char *;

    /**
     * Returns the kIRGDaiKanwaZiten property, or nullptr if not present
     * @return
     */
    auto kIRGDaiKanwaZiten(char32_t) -> const char *;

    /**
     * Returns the kIRGHanyuDaZidian property, or nullptr if not present
     * @return
     */
    auto kIRGHanyuDaZidian(char32_t) -> const char *;

    /**
     * Returns the kIRG_GSource property, or nullptr if not present
     * @return
     */
    auto kIRG_GSource(char32_t) -> const char *;

    /**
     * Returns the kIRG_HSource property, or nullptr if not present
     * @return
     */
    auto kIRG_HSource(char32_t) -> const char *;

    /**
     * Returns the kIRG_JSource property, or nullptr if not present
     * @return
     */
    auto kIRG_JSource(char32_t) -> const char *;

    /**
     * Returns the kIRG_KPSource property, or nullptr if not present
     * @return
     */
    auto kIRG_KPSource(char32_t) -> const char *;

    /**
     * Returns the kIRG_KSource property, or nullptr if not present
     * @return
     */
    auto kIRG_KSource(char32_t) -> const char *;

    /**
     * Returns the kIRG_MSource property, or nullptr if not present
     * @return
     */
    auto kIRG_MSource(char32_t) -> const char *;

    /**
     * Returns the kIRG_SSource property, or nullptr if not present
     * @return
     */
    auto kIRG_SSource(char32_t) -> const char *;

    /**
     * Returns the kIRG_TSource property, or nullptr if not present
     * @return
     */
    auto kIRG_TSource(char32_t) -> const char *;

    /**
     * Returns the kIRG_UKSource property, or nullptr if not present
     * @return
     */
    auto kIRG_UKSource(char32_t) -> const char *;

    /**
     * Returns the kIRG_USource property, or nullptr if not present
     * @return
     */
    auto kIRG_USource(char32_t) -> const char *;

    /**
     * Returns the kIRG_VSource property, or nullptr if not present
     * @return
     */
    auto kIRG_VSource(char32_t) -> const char *;

    /**
     * Returns the kJHJ property, or nullptr if not present
     * @return
     */
    auto kJHJ(char32_t) -> const char *;

    /**
     * Returns the kJIS0213 property, or nullptr if not present
     * @return
     */
    auto kJIS0213(char32_t) -> const char *;

    /**
     * Returns the kJa property, or nullptr if not present
     * @return
     */
    auto kJa(char32_t) -> const char *;

    /**
     * Returns the kJapaneseKun property, or nullptr if not present
     * @return
     */
    auto kJapaneseKun(char32_t) -> const char *;

    /**
     * Returns the kJapaneseOn property, or nullptr if not present
     * @return
     */
    auto kJapaneseOn(char32_t) -> const char *;

    /**
     * Returns the kJinmeiyoKanji property, or nullptr if not present
     * @return
     */
    auto kJinmeiyoKanji(char32_t) -> const char *;

    /**
     * Returns the kJis0 property, or nullptr if not present
     * @return
     */
    auto kJis0(char32_t) -> const char *;

    /**
     * Returns the kJis1 property, or nullptr if not present
     * @return
     */
    auto kJis1(char32_t) -> const char *;

    /**
     * Returns the kJoyoKanji property, or nullptr if not present
     * @return
     */
    auto kJoyoKanji(char32_t) -> const char *;

    /**
     * Returns the kKPS0 property, or nullptr if not present
     * @return
     */
    auto kKPS0(char32_t) -> const char *;

    /**
     * Returns the kKPS1 property, or nullptr if not present
     * @return
     */
    auto kKPS1(char32_t) -> const char *;

    /**
     * Returns the kKSC0 property, or nullptr if not present
     * @return
     */
    auto kKSC0(char32_t) -> const char *;

    /**
     * Returns the kKangXi property, or nullptr if not present
     * @return
     */
    auto kKangXi(char32_t) -> const char *;

    /**
     * Returns the kKarlgren property, or nullptr if not present
     * @return
     */
    auto kKarlgren(char32_t) -> const char *;

    /**
     * Returns the kKorean property, or nullptr if not present
     * @return
     */
    auto kKorean(char32_t) -> const char *;

    /**
     * Returns the kKoreanEducationHanja property, or nullptr if not present
     * @return
     */
    auto kKoreanEducationHanja(char32_t) -> const char *;

    /**
     * Returns the kKoreanName property, or nullptr if not present
     * @return
     */
    auto kKoreanName(char32_t) -> const char *;

    /**
     * Returns the kLau property, or nullptr if not present
     * @return
     */
    auto kLau(char32_t) -> const char *;

    /**
     * Returns the kMainlandTelegraph property, or nullptr if not present
     * @return
     */
    auto kMainlandTelegraph(char32_t) -> const char *;

    /**
     * Returns the kMandarin property, or nullptr if not present
     * @return
     */
    auto kMandarin(char32_t) -> const char8_t *;

    /**
     * Returns the kMatthews property, or nullptr if not present
     * @return
     */
    auto kMatthews(char32_t) -> const char *;

    /**
     * Returns the kMeyerWempe property, or nullptr if not present
     * @return
     */
    auto kMeyerWempe(char32_t) -> const char *;

    /**
     * Returns the kMorohashi property, or nullptr if not present
     * @return
     */
    auto kMorohashi(char32_t) -> const char *;

    /**
     * Returns the kNelson property, or nullptr if not present
     * @return
     */
    auto kNelson(char32_t) -> const char *;

    /**
     * Returns the kOtherNumeric property, or nullptr if not present
     * @return
     */
    auto kOtherNumeric(char32_t) -> const char *;

    /**
     * Returns the kPhonetic property, or nullptr if not present
     * @return
     */
    auto kPhonetic(char32_t) -> const char *;

    /**
     * Returns the kPrimaryNumeric property, or nullptr if not present
     * @return
     */
    auto kPrimaryNumeric(char32_t) -> const char *;

    /**
     * Returns the kPseudoGB1 property, or nullptr if not present
     * @return
     */
    auto kPseudoGB1(char32_t) -> const char *;

    /**
     * Returns the kRSAdobe_Japan1_6 property, or nullptr if not present
     * @return
     */
    auto kRSAdobe_Japan1_6(char32_t) -> const char *;

    /**
     * Returns the kRSJapanese property, or nullptr if not present
     * @return
     */
    auto kRSJapanese(char32_t) -> const char *;

    /**
     * Returns the kRSKanWa property, or nullptr if not present
     * @return
     */
    auto kRSKanWa(char32_t) -> const char *;

    /**
     * Returns the kRSKangXi property, or nullptr if not present
     * @return
     */
    auto kRSKangXi(char32_t) -> const char *;

    /**
     * Returns the kRSMerged property, or nullptr if not present
     * @return
     */
    auto kRSMerged(char32_t) -> const char *;

    /**
     * Returns the kRSUnicode property, or nullptr if not present
     * @return
     */
    auto kRSUnicode(char32_t) -> const char *;

    /**
     * Returns the kSBGY property, or nullptr if not present
     * @return
     */
    auto kSBGY(char32_t) -> const char *;

    /**
     * Returns the kSpecializedSemanticVariant property, or nullptr if not present
     * @return
     */
    auto kSpecializedSemanticVariant(char32_t) -> const char *;

    /**
     * Returns the kSemanticVariant property, or nullptr if not present
     * @return
     */
    auto kSemanticVariant(char32_t) -> const char *;

    /**
     * Returns the kSimplifiedVariant property, or nullptr if not present
     * @return
     */
    auto kSimplifiedVariant(char32_t) -> const char *;

    /**
     * Returns the kSpoofingVariant property, or nullptr if not present
     * @return
     */
    auto kSpoofingVariant(char32_t) -> const char *;

    /**
     * Returns the kStrange property, or nullptr if not present
     * @return
     */
    auto kStrange(char32_t) -> const char *;

    /**
     * Returns the kTGH property, or nullptr if not present
     * @return
     */
    auto kTGH(char32_t) -> const char *;

    /**
     * Returns the kTGHZ2013 property, or nullptr if not present
     * @return
     */
    auto kTGHZ2013(char32_t) -> const char8_t *;

    /**
     * Returns the kTaiwanTelegraph property, or nullptr if not present
     * @return
     */
    auto kTaiwanTelegraph(char32_t) -> const char *;

    /**
     * Returns the kTang property, or nullptr if not present
     * @return
     */
    auto kTang(char32_t) -> const char8_t *;

    /**
     * Returns the kTotalStrokes property, or nullptr if not present
     * @return
     */
    auto kTotalStrokes(char32_t) -> const char *;

    /**
     * Returns the kTraditionalVariant property, or nullptr if not present
     * @return
     */
    auto kTraditionalVariant(char32_t) -> const char *;

    /**
     * Returns the kUnihanCore2020 property, or nullptr if not present
     * @return
     */
    auto kUnihanCore2020(char32_t) -> const char *;

    /**
     * Returns the kVietnamese property, or nullptr if not present
     * @return
     */
    auto kVietnamese(char32_t) -> const char8_t *;

    /**
     * Returns the kXHC1983 property, or nullptr if not present
     * @return
     */
    auto kXHC1983(char32_t) -> const char8_t *;

    /**
     * Returns the kXerox property, or nullptr if not present
     * @return
     */
    auto kXerox(char32_t) -> const char *;

    /**
     * Returns the kZVariant property, or nullptr if not present
     * @return
     */
    auto kZVariant(char32_t) -> const char *;
}

#pragma clang diagnostic pop