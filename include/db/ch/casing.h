#pragma once

namespace unicode::db::ch {
    /**
     * Returns simple case folded version of a character
     * @return
     */
    auto simple_case_folding(char32_t) -> char32_t;

    /**
     * Returns simple lower cased version of a character
     * @return
     */
    auto simple_lower_case(char32_t) -> char32_t;

    /**
     * Returns simple title cased version of a character
     * @return
     */
    auto simple_title_case(char32_t) -> char32_t;

    /**
     * Returns simple upper cased version of a character
     * @return
     */
    auto simple_upper_case(char32_t) -> char32_t;

    /**
     * Returns upper cased version of a character (may be multiple characters)
     * @return
     */
    auto upper_case(char32_t) -> const char32_t *;

    /**
     * Returns lower cased version of a character (may be multiple characters)
     * @return
     */
    auto lower_case(char32_t) -> const char32_t *;

    /**
     * Returns title cased version of a character (may be multiple characters)
     * @return
     */
    auto title_case(char32_t) -> const char32_t *;

    /**
     * Returns case folded version of a character (may be multiple characters)
     * @return
     */
    auto case_folding(char32_t) -> const char32_t *;
}
