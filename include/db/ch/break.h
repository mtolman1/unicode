#pragma once

#include <optional>
#include "db/ch/defs.h"

namespace unicode::db::ch {

    /**
     * Returns grapheme cluster break class (if available)
     * @return
     */
    auto grapheme_cluster_break(char32_t) -> std::optional<GraphemeClusterBreak>;

    /**
     * Returns line break class (if available)
     * @return
     */
    auto line_break(char32_t) -> std::optional<LineBreak>;

    /**
     * Returns sentence break class (if available)
     * @return
     */
    auto sentence_break(char32_t) -> std::optional<SentenceBreak>;

    /**
     * Returns word break class (if available)
     * @return
     */
    auto word_break(char32_t) -> std::optional<WordBreak>;
}
