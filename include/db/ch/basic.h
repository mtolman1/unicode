#pragma once

#include <optional>
#include "db/ch/defs.h"

namespace unicode::db::ch {

    /**
     * Returns whether a character is an alphabetic unicode character
     * @return
     */
    auto is_alphabetic(char32_t) -> bool;

    /**
     * Returns whether a character is an ascii hexadecimal unicode character
     * @return
     */
    auto is_ascii_hex(char32_t) -> bool;

    /**
     * Returns whether a character is an bidirectional control unicode character
     * @return
     */
    auto is_bidi_control(char32_t) -> bool;

    /**
     * Returns whether a character is an bidirectionally mirrored unicode character
     * @return
     */
    auto is_bidi_mirrored(char32_t) -> bool;

    /**
     * Returns whether a character is an case ignorable
     * @return
     */
    auto is_case_ignorable(char32_t) -> bool;

    /**
     * Returns whether a character is an cased
     * @return
     */
    auto is_cased(char32_t) -> bool;

    /**
     * Returns whether a character is an changed when case folded
     * @return
     */
    auto is_changed_when_case_folded(char32_t) -> bool;

    /**
     * Returns whether a character is an changed when case mapped
     * @return
     */
    auto is_changed_when_case_mapped(char32_t) -> bool;

    /**
     * Returns whether a character is an changed when lower cased
     * @return
     */
    auto is_changed_when_lower_cased(char32_t) -> bool;

    /**
     * Returns whether a character is an changed when nfkc case folded
     * @return
     */
    auto is_changed_when_nfkc_case_folded(char32_t) -> bool;

    /**
     * Returns whether a character is an changed when title cased
     * @return
     */
    auto is_changed_when_title_cased(char32_t) -> bool;

    /**
     * Returns whether a character is an changed when upper cased
     * @return
     */
    auto is_changed_when_upper_cased(char32_t) -> bool;

    /**
     * Returns whether a character is composition exclusion
     * @return
     */
    auto is_composition_exclusion(char32_t) -> bool;

    /**
     * Returns whether a character is a dash
     * @return
     */
    auto is_dash(char32_t) -> bool;

    /**
     * Returns whether a character is a default ignorable code point
     * @return
     */
    auto is_default_ignorable_code_point(char32_t) -> bool;

    /**
     * Returns whether a character is deprecated
     * @return
     */
    auto is_deprecated(char32_t) -> bool;

    /**
     * Returns whether a character is a diacritic
     * @return
     */
    auto is_diacritic(char32_t) -> bool;

    /**
     * Returns whether a character is an emoji
     * @return
     */
    auto is_emoji(char32_t) -> bool;

    /**
     * Returns whether a character is an emoji component
     * @return
     */
    auto is_emoji_component(char32_t) -> bool;

    /**
     * Returns whether a character is an emoji modifier
     * @return
     */
    auto is_emoji_modifier(char32_t) -> bool;

    /**
     * Returns whether a character is an emoji modifier base
     * @return
     */
    auto is_emoji_modifier_base(char32_t) -> bool;

    /**
     * Returns whether a character is an emoji modifier presentation
     * @return
     */
    auto is_emoji_presentation(char32_t) -> bool;

    /**
     * Returns whether a character expands on nfc
     * @return
     */
    auto is_expand_on_nfc(char32_t) -> bool;

    /**
     * Returns whether a character expands on nfd
     * @return
     */
    auto is_expand_on_nfd(char32_t) -> bool;

    /**
     * Returns whether a character expands on nfkc
     * @return
     */
    auto is_expand_on_nfkc(char32_t) -> bool;

    /**
     * Returns whether a character expands on nfkd
     * @return
     */
    auto is_expand_on_nfkd(char32_t) -> bool;

    /**
     * Returns whether a character is an extended identifier continue
     * @return
     */
    auto is_extended_identifier_continue(char32_t) -> bool;

    /**
     * Returns whether a character is an extended identifier start
     * @return
     */
    auto is_extended_identifier_start(char32_t) -> bool;

    /**
     * Returns whether a character is an extended pictograph
     * @return
     */
    auto is_extended_pictograph(char32_t) -> bool;

    /**
     * Returns whether a character is an extender
     * @return
     */
    auto is_extender(char32_t) -> bool;

    /**
     * Returns whether a character is a full composition exclusion
     * @return
     */
    auto is_full_composition_exclusion(char32_t) -> bool;

    /**
     * Returns whether a character is a grapheme base
     * @return
     */
    auto is_grapheme_base(char32_t) -> bool;

    /**
     * Returns whether a character is a grapheme extend
     * @return
     */
    auto is_grapheme_extend(char32_t) -> bool;

    /**
     * Returns whether a character is a grapheme link
     * @return
     */
    auto is_grapheme_link(char32_t) -> bool;

    /**
     * Returns whether a character is a hex character
     * @return
     */
    auto is_hex(char32_t) -> bool;

    /**
     * Returns whether a character is a hyphen
     * @return
     */
    auto is_hyphen(char32_t) -> bool;

    /**
     * Returns whether a character is an identifier continue
     * @return
     */
    auto is_identifier_continue(char32_t) -> bool;

    /**
     * Returns whether a character is an identifier start
     * @return
     */
    auto is_identifier_start(char32_t) -> bool;

    /**
     * Returns whether a character is an ideographic
     * @return
     */
    auto is_ideographic(char32_t) -> bool;

    /**
     * Returns whether a character is an ids binary operator
     * @return
     */
    auto is_ids_binary_operator(char32_t) -> bool;

    /**
     * Returns whether a character is an ids trinary operator
     * @return
     */
    auto is_ids_trinary_operator(char32_t) -> bool;

    /**
     * Returns whether a character is a join control
     * @return
     */
    auto is_join_control(char32_t) -> bool;

    /**
     * Returns whether a character is a logical order exception
     * @return
     */
    auto is_logical_order_exception(char32_t) -> bool;

    /**
     * Returns whether a character is lower cased
     * @return
     */
    auto is_lower(char32_t) -> bool;

    /**
     * Returns whether a character is a math symbol
     * @return
     */
    auto is_math(char32_t) -> bool;

    /**
     * Returns whether a character is a non character code point
     * @return
     */
    auto is_non_character_code_point(char32_t) -> bool;

    /**
     * Returns whether a character is other alphabetic
     * @return
     */
    auto is_other_alphabetic(char32_t) -> bool;

    /**
     * Returns whether a character is an other non character code point
     * @return
     */
    auto is_other_default_ignorable_code_point(char32_t) -> bool;

    /**
     * Returns whether a character is an other grapheme extend
     * @return
     */
    auto is_other_grapheme_extend(char32_t) -> bool;

    /**
     * Is an other identifier continue
     * @return
     */
    auto is_other_identifier_continue(char32_t) -> bool;

    /**
     * Is an other identifier start
     * @return
     */
    auto is_other_identifier_start(char32_t) -> bool;

    /**
     * Is an other lower cased
     * @return
     */
    auto is_other_lower(char32_t) -> bool;

    /**
     * Is an other math symbol
     * @return
     */
    auto is_other_math(char32_t) -> bool;

    /**
     * Is an other upper cased
     * @return
     */
    auto is_other_upper(char32_t) -> bool;

    /**
     * Is a pattern syntax
     * @return
     */
    auto is_pattern_syntax(char32_t) -> bool;

    /**
     * Is a pattern whitespace
     * @return
     */
    auto is_pattern_whitespace(char32_t) -> bool;

    /**
     * Is a prepended concatenation mark
     * @return
     */
    auto is_prepended_concatenation_mark(char32_t) -> bool;

    /**
     * Is a quotation mark
     * @return
     */
    auto is_quotation_mark(char32_t) -> bool;

    /**
     * Is a radical
     * @return
     */
    auto is_radical(char32_t) -> bool;

    /**
     * Is a regional indicator
     * @return
     */
    auto is_regional_indicator(char32_t) -> bool;

    /**
     * Is a sentence terminal
     * @return
     */
    auto is_sentence_terminal(char32_t) -> bool;

    /**
     * Is soft dotted
     * @return
     */
    auto is_soft_dotted(char32_t) -> bool;

    /**
     * Is a terminal punctuation mark
     * @return
     */
    auto is_terminal_punctuation(char32_t) -> bool;

    /**
     * Is a unified ideograph
     * @return
     */
    auto is_unified_ideograph(char32_t) -> bool;

    /**
     * Is upper cased
     * @return
     */
    auto is_upper(char32_t) -> bool;

    /**
     * Is a variation selector
     * @return
     */
    auto is_variation_selector(char32_t) -> bool;

    /**
     * Is white space
     * @return
     */
    auto is_white_space(char32_t) -> bool;

    /**
     * Returns vertical orientation of a character
     * @return
     */
    auto vertical_orientation(char32_t) -> std::optional<db::ch::VerticalOrientation>;
}
