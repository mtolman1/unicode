#pragma once
#pragma clang diagnostic push
#pragma ide diagnostic ignored "readability-identifier-naming"

namespace unicode::db::ch {
    /**
     * Returns the kTGT_MergedSrc property, or nullptr if not present
     * @return
     */
    auto kRSTUnicode(char32_t) -> const char *;

    /**
     * Returns the kTGT_MergedSrc property, or nullptr if not present
     * @return
     */
    auto kTGT_MergedSrc(char32_t) -> const char *;
}

#pragma clang diagnostic pop