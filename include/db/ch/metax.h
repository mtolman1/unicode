#pragma once

#include <string_view>
#include <optional>

namespace unicode::db::ch {
    /**
     * Returns the legacy name of a character
     * @return
     */
    auto legacy_name(char32_t) -> const char *;

    /**
     * Returns a character by it's legacy name (if there is one by the provided name)
     * @return
     */
    auto char_by_legacy_name(const std::string_view &) -> std::optional<char32_t>;

    /**
     * Returns the iso comment tied to a character
     * @return
     */
    auto iso10646_comment(char32_t) -> const char *;
}
