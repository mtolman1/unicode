#pragma once

#include "db/ch/defs.h"
#include <optional>

namespace unicode::db::ch {
    /**
     * Returns the east asian width of a character (if available)
     * @return
     */
    auto east_asian_width(char32_t) -> std::optional<EastAsianWidth>;

    /**
     * Returns the hangul syllable type of a character (if available)
     * @return
     */
    auto hangul_syllable_type(char32_t) -> std::optional<HangulSyllableType>;

    /**
     * Returns the jamo short name of a character, or nullptr if not available
     * @return
     */
    auto jamo_short_name(char32_t ch) -> const char *;

    /**
     * Returns the equivalent unified ideograph of a character or the same character if one is not available
     * @return
     */
    auto equivalent_unified_ideograph(char32_t) -> char32_t;
}
