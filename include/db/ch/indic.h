#pragma once

#include <optional>
#include "db/ch/defs.h"

namespace unicode::db::ch {

    /**
     * Returns indic mantra category (if available)
     */
    auto indic_mantra_category(char32_t) -> std::optional<IndicMantraCategory>;

    /**
     * Returns indic positional category (if available)
     */
    auto indic_positional_category(char32_t) -> std::optional<IndicPositionalCategory>;

    /**
     * Returns indic syllabic category (if available)
     */
    auto indic_syllabic_category(char32_t) -> std::optional<IndicSyllabicCategory>;
}