#pragma once

#include "db/ch/defs.h"

#include <optional>
#include <vector>

namespace unicode::db::ch {

    /**
     * Returns the unicode script for a character (if available)
     * @return
     */
    auto script(char32_t) -> std::optional<Script>;

    /**
     * Returns the unicode script extensions for a character (no particular order)
     * @return
     */
    auto script_extensions(char32_t) -> std::vector<Script>;
}
