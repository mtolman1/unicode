#pragma once

#include <optional>
#include "db/ch/defs.h"

namespace unicode::db::ch {

    /**
     * Get bidirectional class (if available)
     * @return
     */
    auto bidi_class(char32_t) -> std::optional<BidiClass>;

    /**
     * Get bidirectional mirrored glpyh
     * <p>
     * **Note:** This is just for completeness. However, bidi mirroring is application and font specific.
     * If bidi mirroring is needed, please use unicode layers as this will always return the character passed in.
     * </p>
     * @return
     */
    auto bidi_mirrored_glyph(char32_t) -> char32_t;

    /**
     * Returns the other bracket paired with a specific character, or returns the current character if there is no pair.
     * @return
     */
    auto bidi_paired_bracket(char32_t) -> char32_t;

    /**
     * Returns the bidi paired bracket type (if available)
     * @return
     */
    auto bidi_paired_bracket_type(char32_t) -> std::optional<BidiPairedBracketType>;
}
