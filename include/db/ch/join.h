#pragma once

#include <optional>
#include "db/ch/defs.h"

namespace unicode::db::ch {

    /**
     * Returns join type for a character (if available)
     * @return
     */
    auto join_type(char32_t) -> std::optional<JoinType>;

    /**
     * Returns joining type for a character (if available)
     * @return
     */
    auto joining_group(char32_t) -> std::optional<JoiningGroup>;

    /**
     * Returns combining properties
     * @return
     */
    auto combining_properties(char32_t) -> uint8_t;
}
