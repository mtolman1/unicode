#pragma once

#include "db/ch/defs.h"

#include <optional>

namespace unicode::db::ch {
    /**
     * Returns decomposition mapping for a character, or nullptr if one is not available
     * @return
     */
    auto decomposition_mapping(char32_t) -> const char32_t *;

    /**
     * Returns decomposition type for a character (if available)
     * @return
     */
    auto decomposition_type(char32_t) -> std::optional<DecompositionType>;

    /**
     * Returns the FC NFKC Closure for a character, or nullptr if one is not available
     * @return
     */
    auto fc_nfkc_closure(char32_t) -> const char32_t *;

    /**
     * Returns the NFKC case fold for a character, or nullptr if one is not available
     * @return
     */
    auto nfkc_case_fold(char32_t) -> const char32_t *;

    /**
     * Returns the NFC quick check for a character
     * @return
     */
    auto nfc_quick_check(char32_t) -> NFC_QC;

    /**
     * Returns the NFD quick check for a character
     * @return
     */
    auto nfd_quick_check(char32_t) -> NFD_QC;

    /**
     * Returns the NFKC quick check for a character
     * @return
     */
    auto nfkc_quick_check(char32_t) -> NFKC_QC;

    /**
     * Returns the NFKD quick check for a character
     * @return
     */
    auto nfkd_quick_check(char32_t) -> NFKD_QC;
}
