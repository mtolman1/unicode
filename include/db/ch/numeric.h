#pragma once

#include "db/ch/defs.h"
#include <string>
#include <set>

namespace unicode::db::ch {
    /**
     * Returns the numeric type for a character
     * @return
     */
    auto numeric_type(char32_t) -> NumericType;

    /**
     * Returns a string representing the numeric value of a character, or nullptr if one is not present
     * @return
     */
    auto numeric_value(char32_t) -> const char *;

    /**
     * Returns all characters associated with a number (e.g. 1, 9, 1/2)
     * @param number String representation of a number
     * @return
     */
    auto chars_by_numeric_value(const std::string_view & number) -> std::set<char32_t>;

    /**
     * Returns all characters associated with a number (e.g. 1, 9, 1/2)
     * @param number String representation of a number
     * @return
     */
    auto chars_by_numeric_value(const std::u16string_view & number) -> std::set<char32_t>;

    /**
     * Returns all characters associated with a number (e.g. 1, 9, 1/2)
     * @param number String representation of a number
     * @return
     */
    auto chars_by_numeric_value(const std::u32string_view & number) -> std::set<char32_t>;

    /**
     * Returns all characters associated with a number (e.g. 1, 9, 1/2)
     * @param number String representation of a number
     * @return
     */
    auto chars_by_numeric_value(const std::u8string_view & number) -> std::set<char32_t>;

    /**
     * Returns all characters associated with a number (e.g. 1, 9, 1/2)
     * @param number Double representation of a number (e.g. 1, 0.5)
     * @return
     */
    auto chars_by_numeric_value(double number) -> std::set<char32_t>;
}
