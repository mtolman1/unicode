#pragma once

#include <set>

namespace unicode::db::ch {
    /**
     * Returns all alphabetic characters as a set
     * @return
     */
    [[maybe_unused]] auto all_of_alphabetic() -> std::set<char32_t>;

    /**
     * Returns all ascii characters as a set
     * @return
     */
    [[maybe_unused]] auto all_of_ascii_hex() -> std::set<char32_t>;

    /**
     * Returns all bidi control characters as a set
     * @return
     */
    [[maybe_unused]] auto all_of_bidi_control() -> std::set<char32_t>;

    /**
     * Returns all bidi mirrored characters as a set
     * @return
     */
    [[maybe_unused]] auto all_of_bidi_mirrored() -> std::set<char32_t>;

    /**
     * Returns all case ignorable characters as a set
     * @return
     */
    [[maybe_unused]] auto all_of_case_ignorable() -> std::set<char32_t>;

    /**
     * Returns all cased characters as a set
     * @return
     */
    [[maybe_unused]] auto all_of_cased() -> std::set<char32_t>;

    /**
     * Returns all characters that change when case folded
     * @return
     */
    [[maybe_unused]] auto all_of_changed_when_case_folded() -> std::set<char32_t>;

    /**
     * Returns all characters that change when case mapped
     * @return
     */
    [[maybe_unused]] auto all_of_changed_when_case_mapped() -> std::set<char32_t>;

    /**
     * Returns all characters that change when lower cased
     * @return
     */
    [[maybe_unused]] auto all_of_changed_when_lower_cased() -> std::set<char32_t>;

    /**
     * Returns all characters that change when nfkc case folded
     * @return
     */
    [[maybe_unused]] auto all_of_changed_when_nfkc_case_folded() -> std::set<char32_t>;

    /**
     * Returns all characters that change when title cased
     * @return
     */
    [[maybe_unused]] auto all_of_changed_when_title_cased() -> std::set<char32_t>;

    /**
     * Returns all characters that change when upper cased
     * @return
     */
    [[maybe_unused]] auto all_of_changed_when_upper_cased() -> std::set<char32_t>;

    /**
     * Returns all composition exclusion characters
     * @return
     */
    [[maybe_unused]] auto all_of_composition_exclusion() -> std::set<char32_t>;

    /**
     * Returns all dash characters
     * @return
     */
    [[maybe_unused]] auto all_of_dash() -> std::set<char32_t>;

    /**
     * Returns all default ignorable code point characters
     * @return
     */
    [[maybe_unused]] auto all_of_default_ignorable_code_point() -> std::set<char32_t>;

    /**
     * Returns all deprecated characters
     * @return
     */
    [[maybe_unused]] auto all_of_deprecated() -> std::set<char32_t>;

    /**
     * Returns all diacritic characters
     * @return
     */
    [[maybe_unused]] auto all_of_diacritic() -> std::set<char32_t>;

    /**
     * Returns all emoji characters
     * @return
     */
    [[maybe_unused]] auto all_of_emoji() -> std::set<char32_t>;

    /**
     * Returns all emoji component characters
     * @return
     */
    [[maybe_unused]] auto all_of_emoji_component() -> std::set<char32_t>;

    /**
     * Returns all emoji modifier characters
     * @return
     */
    [[maybe_unused]] auto all_of_emoji_modifier() -> std::set<char32_t>;

    /**
     * Returns all emoji modifier base characters
     * @return
     */
    [[maybe_unused]] auto all_of_emoji_modifier_base() -> std::set<char32_t>;

    /**
     * Returns all emoji presentation characters
     * @return
     */
    [[maybe_unused]] auto all_of_emoji_presentation() -> std::set<char32_t>;

    /**
     * Returns all characters that expand on nfc
     * @return
     */
    [[maybe_unused]] auto all_of_expand_on_nfc() -> std::set<char32_t>;

    /**
     * Returns all characters that expand on nfd
     * @return
     */
    [[maybe_unused]] auto all_of_expand_on_nfd() -> std::set<char32_t>;

    /**
     * Returns all characters that expand on nfkc
     * @return
     */
    [[maybe_unused]] auto all_of_expand_on_nfkc() -> std::set<char32_t>;

    /**
     * Returns all characters that expand on nfkd
     * @return
     */
    [[maybe_unused]] auto all_of_expand_on_nfkd() -> std::set<char32_t>;

    /**
     * Returns all extended identifier continue characters
     * @return
     */
    [[maybe_unused]] auto all_of_extended_identifier_continue() -> std::set<char32_t>;

    /**
     * Returns all extended identifier start characters
     * @return
     */
    [[maybe_unused]] auto all_of_extended_identifier_start() -> std::set<char32_t>;

    /**
     * Returns all extended pictograph characters
     * @return
     */
    [[maybe_unused]] auto all_of_extended_pictograph() -> std::set<char32_t>;

    /**
     * Returns all extender characters
     * @return
     */
    [[maybe_unused]] auto all_of_extender() -> std::set<char32_t>;

    /**
     * Returns all full composition exclusion characters
     * @return
     */
    [[maybe_unused]] auto all_of_full_composition_exclusion() -> std::set<char32_t>;

    /**
     * Returns all grapheme base characters
     * @return
     */
    [[maybe_unused]] auto all_of_grapheme_base() -> std::set<char32_t>;

    /**
     * Returns all grapheme extend  characters
     * @return
     */
    [[maybe_unused]] auto all_of_grapheme_extend() -> std::set<char32_t>;

    /**
     * Returns all grapheme link characters
     * @return
     */
    [[maybe_unused]] auto all_of_grapheme_link() -> std::set<char32_t>;

    /**
     * Returns all hex characters
     * @return
     */
    [[maybe_unused]] auto all_of_hex() -> std::set<char32_t>;

    /**
     * Returns all hyphen characters
     * @return
     */
    [[maybe_unused]] auto all_of_hyphen() -> std::set<char32_t>;

    /**
     * Returns all identifier continue characters
     * @return
     */
    [[maybe_unused]] auto all_of_identifier_continue() -> std::set<char32_t>;

    /**
     * Returns all identifier start characters
     * @return
     */
    [[maybe_unused]] auto all_of_identifier_start() -> std::set<char32_t>;

    /**
     * Returns all ideographic characters
     * @return
     */
    [[maybe_unused]] auto all_of_ideographic() -> std::set<char32_t>;

    /**
     * Returns all ids binary operator characters
     * @return
     */
    [[maybe_unused]] auto all_of_ids_binary_operator() -> std::set<char32_t>;

    /**
     * Returns all ids trinary operator characters
     * @return
     */
    [[maybe_unused]] auto all_of_ids_trinary_operator() -> std::set<char32_t>;

    /**
     * Returns all join control characters
     * @return
     */
    [[maybe_unused]] auto all_of_join_control() -> std::set<char32_t>;

    /**
     * Returns all logical order exception characters
     * @return
     */
    [[maybe_unused]] auto all_of_logical_order_exception() -> std::set<char32_t>;

    /**
     * Returns all lower cased characters
     * @return
     */
    [[maybe_unused]] auto all_of_lower() -> std::set<char32_t>;

    /**
     * Returns all math characters
     * @return
     */
    [[maybe_unused]] auto all_of_math() -> std::set<char32_t>;

    /**
     * Returns all non character code point characters
     * @return
     */
    [[maybe_unused]] auto all_of_non_character_code_point() -> std::set<char32_t>;

    /**
     * Returns all other alphabetic characters
     * @return
     */
    [[maybe_unused]] auto all_of_other_alphabetic() -> std::set<char32_t>;

    /**
     * Returns all other default ignorable code point characters
     * @return
     */
    [[maybe_unused]] auto all_of_other_default_ignorable_code_point() -> std::set<char32_t>;

    /**
     * Returns all other grapheme extend characters
     * @return
     */
    [[maybe_unused]] auto all_of_other_grapheme_extend() -> std::set<char32_t>;

    /**
     * Returns all other identifier continue characters
     * @return
     */
    [[maybe_unused]] auto all_of_other_identifier_continue() -> std::set<char32_t>;

    /**
     * Returns all other identifier start characters
     * @return
     */
    [[maybe_unused]] auto all_of_other_identifier_start() -> std::set<char32_t>;

    /**
     * Returns all other lower cased characters
     * @return
     */
    [[maybe_unused]] auto all_of_other_lower() -> std::set<char32_t>;

    /**
     * Returns all other math characters
     * @return
     */
    [[maybe_unused]] auto all_of_other_math() -> std::set<char32_t>;

    /**
     * Returns all other upper cased characters
     * @return
     */
    [[maybe_unused]] auto all_of_other_upper() -> std::set<char32_t>;

    /**
     * Returns all pattern syntax characters
     * @return
     */
    [[maybe_unused]] auto all_of_pattern_syntax() -> std::set<char32_t>;

    /**
     * Returns all pattern whitespace characters
     * @return
     */
    [[maybe_unused]] auto all_of_pattern_whitespace() -> std::set<char32_t>;

    /**
     * Returns all prepended concatenation mark characters
     * @return
     */
    [[maybe_unused]] auto all_of_prepended_concatenation_mark() -> std::set<char32_t>;

    /**
     * Returns all quotation mark characters
     * @return
     */
    [[maybe_unused]] auto all_of_quotation_mark() -> std::set<char32_t>;

    /**
     * Returns all radical characters
     * @return
     */
    [[maybe_unused]] auto all_of_radical() -> std::set<char32_t>;

    /**
     * Returns all regional indicator characters
     * @return
     */
    [[maybe_unused]] auto all_of_regional_indicator() -> std::set<char32_t>;

    /**
     * Returns all sentence terminal characters
     * @return
     */
    [[maybe_unused]] auto all_of_sentence_terminal() -> std::set<char32_t>;

    /**
     * Returns all soft dotted characters
     * @return
     */
    [[maybe_unused]] auto all_of_soft_dotted() -> std::set<char32_t>;

    /**
     * Returns all terminal punctuation characters
     * @return
     */
    [[maybe_unused]] auto all_of_terminal_punctuation() -> std::set<char32_t>;

    /**
     * Returns all unified ideograph characters
     * @return
     */
    [[maybe_unused]] auto all_of_unified_ideograph() -> std::set<char32_t>;

    /**
     * Returns all upper cased characters
     * @return
     */
    [[maybe_unused]] auto all_of_upper() -> std::set<char32_t>;

    /**
     * Returns all variation selector characters
     * @return
     */
    [[maybe_unused]] auto all_of_variation_selector() -> std::set<char32_t>;

    /**
     * Returns all white space characters
     * @return
     */
    [[maybe_unused]] auto all_of_white_space() -> std::set<char32_t>;
}
