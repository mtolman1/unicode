#pragma once

#include "layers/character.h"

namespace unicode::layers::base {
    auto base_unicode_layer() -> std::shared_ptr<CharacterLayer>;
}
