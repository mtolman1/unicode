#pragma once

#include <map>
#include <vector>
#include <optional>
#include <memory>
#include <functional>
#include <string>
#include "db/ch/defs.h"

namespace unicode::layers {
    /**
     * Represents a layer in getting unicode data.
     * By using layers, developers and applications can create application-specific and/or locale-specific "overrides".
     * This is useful since some unicode properties are reserved to be application specific and are not provided
     * machine-readable data by ICU.
     *
     * The layering approach also allows locale-specific data to be used for different algorithms
     *
     * Additionally, it also allows developers to customize how data is loaded for an application, as well as which
     *  pieces of data are loaded. This can be important when the default static linking methodology doesn't fit an
     *  application's needs.
     */
    class CharacterLayer {
    public:
        /**
         * Default constructor. Creates an empty layer that returns std::nullopt for everything
         */
        CharacterLayer() = default;

        /**
         * Creates a character layer that will fallback to the specified layer
         * @param fallback Layer to fallback to
         */
        explicit CharacterLayer(std::shared_ptr<CharacterLayer> fallback) : child(std::move(fallback)) {}

        /**
         * Merges overrides from this layer with another layer.
         * The other layer's overrides will overwrite the current layer's overrides.
         * @return reference to this
         */
        [[maybe_unused]] auto merge(const CharacterLayer&) -> CharacterLayer&;

        /**
         * Change the fallback layer
         * @return reference to this
         */
        [[maybe_unused]] auto set_fallback_layer(std::shared_ptr<CharacterLayer>) -> CharacterLayer&;

        [[maybe_unused]] [[nodiscard]] auto age(char32_t) const -> std::optional<std::string>;
        [[maybe_unused]] [[nodiscard]] auto aliases(char32_t) const -> std::optional<std::map<db::ch::AliasType, std::vector<std::string>>>;
        [[maybe_unused]] [[nodiscard]] auto bidi_class(char32_t) const -> std::optional<db::ch::BidiClass>;
        [[maybe_unused]] [[nodiscard]] auto bidi_mirrored_glyph(char32_t) const -> std::optional<char32_t>;
        [[maybe_unused]] [[nodiscard]] auto bidi_paired_bracket(char32_t) const -> std::optional<char32_t>;
        [[maybe_unused]] [[nodiscard]] auto bidi_paired_bracket_type(char32_t) const -> std::optional<db::ch::BidiPairedBracketType>;
        [[maybe_unused]] [[nodiscard]] auto block(char32_t) const -> std::optional<db::ch::Block>;
        [[maybe_unused]] [[nodiscard]] auto case_folding(char32_t) const -> std::optional<std::u32string>;
        [[maybe_unused]] [[nodiscard]] auto combining_properties(char32_t) const -> std::optional<uint8_t>;
        [[maybe_unused]] [[nodiscard]] auto decomposition_mapping(char32_t) const -> std::optional<std::u32string>;
        [[maybe_unused]] [[nodiscard]] auto decomposition_type(char32_t) const -> std::optional<db::ch::DecompositionType>;
        [[maybe_unused]] [[nodiscard]] auto east_asian_width(char32_t) const -> std::optional<db::ch::EastAsianWidth>;
        [[maybe_unused]] [[nodiscard]] auto equivalent_unified_ideograph(char32_t) const -> std::optional<char32_t>;
        [[maybe_unused]] [[nodiscard]] auto fc_nfkc_closure(char32_t) const -> std::optional<std::u32string>;
        [[maybe_unused]] [[nodiscard]] auto general_category(char32_t) const -> std::optional<db::ch::GeneralCategory>;
        [[maybe_unused]] [[nodiscard]] auto grapheme_cluster_break(char32_t) const -> std::optional<db::ch::GraphemeClusterBreak>;
        [[maybe_unused]] [[nodiscard]] auto hangul_syllable_type(char32_t) const -> std::optional<db::ch::HangulSyllableType>;
        [[maybe_unused]] [[nodiscard]] auto indic_mantra_category(char32_t) const -> std::optional<db::ch::IndicMantraCategory>;
        [[maybe_unused]] [[nodiscard]] auto indic_positional_category(char32_t) const -> std::optional<db::ch::IndicPositionalCategory>;
        [[maybe_unused]] [[nodiscard]] auto indic_syllabic_category(char32_t) const -> std::optional<db::ch::IndicSyllabicCategory>;
        [[maybe_unused]] [[nodiscard]] auto is_alphabetic(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_ascii_hex(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_bidi_control(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_bidi_mirrored(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_case_ignorable(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_cased(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_changed_when_case_folded(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_changed_when_case_mapped(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_changed_when_lower_cased(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_changed_when_title_cased(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_changed_when_nfkc_case_folded(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_changed_when_upper_cased(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_composition_exclusion(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_dash(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_default_ignorable_code_point(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_deprecated(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_diacritic(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_emoji(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_emoji_component(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_emoji_modifier(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_emoji_modifier_base(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_emoji_presentation(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_expand_on_nfc(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_expand_on_nfd(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_expand_on_nfkc(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_expand_on_nfkd(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_extended_identifier_continue(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_extended_identifier_start(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_extended_pictograph(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_extender(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_full_composition_exclusion(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_grapheme_base(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_grapheme_extend(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_grapheme_link(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_hex(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_hyphen(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_identifier_continue(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_identifier_start(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_ideographic(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_ids_binary_operator(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_ids_trinary_operator(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_join_control(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_logical_order_exception(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_lower(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_math(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_non_character_code_point(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_other_alphabetic(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_other_default_ignorable_code_point(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_other_grapheme_extend(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_other_identifier_continue(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_other_identifier_start(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_other_lower(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_other_math(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_other_upper(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_pattern_syntax(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_pattern_whitespace(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_prepended_concatenation_mark(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_quotation_mark(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_radical(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_regional_indicator(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_sentence_terminal(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_soft_dotted(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_terminal_punctuation(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_unified_ideograph(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_upper(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_variation_selector(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto is_white_space(char32_t) const -> std::optional<bool>;
        [[maybe_unused]] [[nodiscard]] auto iso10646_comment(char32_t) const -> std::optional<std::string>;
        [[maybe_unused]] [[nodiscard]] auto jamo_short_name(char32_t) const -> std::optional<std::string>;
        [[maybe_unused]] [[nodiscard]] auto join_type(char32_t) const -> std::optional<db::ch::JoinType>;
        [[maybe_unused]] [[nodiscard]] auto joining_group(char32_t) const -> std::optional<db::ch::JoiningGroup>;
        [[maybe_unused]] [[nodiscard]] auto kAccountingNumeric(char32_t) const -> std::optional<std::string>; // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kAlternateHanYu(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kAlternateJEF(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kAlternateKangXi(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kAlternateMorohashi(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kAlternateTotalStrokes(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kBigFive(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCCCII(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCNS1986(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCNS1992(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCangjie(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCantonese(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCheungBauer(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCheungBauerIndex(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCihaiT(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCompatibilityVariant(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kCowles(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kDaeJaweon(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kDefinition(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kEACC(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kFenn(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kFennIndex(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kFourCornerCode(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kFrequency(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kGB0(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kGB1(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kGB3(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kGB5(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kGB7(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kGB8(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kGSR(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kGradeLevel(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kHDZRadBreak(char32_t) const -> std::optional<std::u8string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kHKGlyph(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kHKSCS(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kHangul(char32_t) const -> std::optional<std::u8string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kHanyuPinlu(char32_t) const -> std::optional<std::u8string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kHanyuPinyin(char32_t) const -> std::optional<std::u8string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIBMJapan(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIICore(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRGDaeJaweon(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRGDaiKanwaZiten(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRGHanyuDaZidian(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRGKangXi(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_GSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_HSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_JSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_KPSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_KSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_MSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_SSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_TSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_UKSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_USource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kIRG_VSource(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kJHJ(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kJIS0213(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kJa(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kJapaneseKun(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kJapaneseOn(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kJinmeiyoKanji(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kJis0(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kJis1(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kJoyoKanji(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kKPS0(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kKPS1(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kKSC0(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kKangXi(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kKarlgren(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kKorean(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kKoreanEducationHanja(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kKoreanName(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kLau(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kMainlandTelegraph(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kMandarin(char32_t) const -> std::optional<std::u8string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kMatthews(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kMeyerWempe(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kMorohashi(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kNelson(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kOtherNumeric(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kPhonetic(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kPrimaryNumeric(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kPseudoGB1(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kRSAdobe_Japan1_6(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kRSJapanese(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kRSKanWa(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kRSKangXi(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kRSKorean(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kRSMerged(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kRSTUnicode(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kRSUnicode(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kReading(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kSBGY(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kSemanticVariant(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kSimplifiedVariant(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kSpecializedSemanticVariant(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kSpoofingVariant(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kSrc_NushuDuben(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kStrange(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kTGH(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kTGHZ2013(char32_t) const -> std::optional<std::u8string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kTGT_MergedSrc(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kTaiwanTelegraph(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kTang(char32_t) const -> std::optional<std::u8string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kTotalStrokes(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kTraditionalVariant(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kUnihanCore2020(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kVietnamese(char32_t) const -> std::optional<std::u8string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kXHC1983(char32_t) const -> std::optional<std::u8string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kXerox(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto kZVariant(char32_t) const -> std::optional<std::string>;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] [[nodiscard]] auto legacy_name(char32_t) const -> std::optional<std::string>;
        [[maybe_unused]] [[nodiscard]] auto line_break(char32_t) const -> std::optional<db::ch::LineBreak>;
        [[maybe_unused]] [[nodiscard]] auto lower_case(char32_t) const -> std::optional<std::u32string>;
        [[maybe_unused]] [[nodiscard]] auto name(char32_t) const -> std::optional<std::string>;
        [[maybe_unused]] [[nodiscard]] auto nfc_quick_check(char32_t) const -> std::optional<db::ch::NFC_QC>;
        [[maybe_unused]] [[nodiscard]] auto nfd_quick_check(char32_t) const -> std::optional<db::ch::NFD_QC>;
        [[maybe_unused]] [[nodiscard]] auto nfkc_case_fold(char32_t) const -> std::optional<std::u32string>;
        [[maybe_unused]] [[nodiscard]] auto nfkc_quick_check(char32_t) const -> std::optional<db::ch::NFKC_QC>;
        [[maybe_unused]] [[nodiscard]] auto nfkd_quick_check(char32_t) const -> std::optional<db::ch::NFKD_QC>;
        [[maybe_unused]] [[nodiscard]] auto numeric_type(char32_t) const -> std::optional<db::ch::NumericType>;
        [[maybe_unused]] [[nodiscard]] auto numeric_value(char32_t) const -> std::optional<std::string>;
        [[maybe_unused]] [[nodiscard]] auto script(char32_t) const -> std::optional<db::ch::Script>;
        [[maybe_unused]] [[nodiscard]] auto script_extensions(char32_t) const -> std::optional<std::vector<db::ch::Script>>;
        [[maybe_unused]] [[nodiscard]] auto sentence_break(char32_t) const -> std::optional<db::ch::SentenceBreak>;
        [[maybe_unused]] [[nodiscard]] auto simple_case_folding(char32_t) const -> std::optional<char32_t>;
        [[maybe_unused]] [[nodiscard]] auto simple_lower_case(char32_t) const -> std::optional<char32_t>;
        [[maybe_unused]] [[nodiscard]] auto simple_title_case(char32_t) const -> std::optional<char32_t>;
        [[maybe_unused]] [[nodiscard]] auto simple_upper_case(char32_t) const -> std::optional<char32_t>;
        [[maybe_unused]] [[nodiscard]] auto title_case(char32_t) const -> std::optional<std::u32string>;
        [[maybe_unused]] [[nodiscard]] auto upper_case(char32_t) const -> std::optional<std::u32string>;
        [[maybe_unused]] [[nodiscard]] auto vertical_orientation(char32_t) const -> std::optional<db::ch::VerticalOrientation>;
        [[maybe_unused]] [[nodiscard]] auto word_break(char32_t) const -> std::optional<db::ch::WordBreak>;
        
        [[maybe_unused]] auto override_age(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_aliases(std::function<auto (char32_t) -> std::optional<std::map<db::ch::AliasType, std::vector<std::string>>>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_bidi_class(std::function<auto (char32_t) -> std::optional<db::ch::BidiClass>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_bidi_mirrored_glyph(std::function<auto (char32_t) -> std::optional<char32_t>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_bidi_paired_bracket(std::function<auto (char32_t) -> std::optional<char32_t>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_bidi_paired_bracket_type(std::function<auto (char32_t) -> std::optional<db::ch::BidiPairedBracketType>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_block(std::function<auto (char32_t) -> std::optional<db::ch::Block>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_case_folding(std::function<auto (char32_t) -> std::optional<std::u32string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_combining_properties(std::function<auto (char32_t) -> std::optional<uint8_t>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_decomposition_mapping(std::function<auto (char32_t) -> std::optional<std::u32string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_decomposition_type(std::function<auto (char32_t) -> std::optional<db::ch::DecompositionType>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_east_asian_width(std::function<auto (char32_t) -> std::optional<db::ch::EastAsianWidth>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_equivalent_unified_ideograph(std::function<auto (char32_t) -> std::optional<char32_t>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_fc_nfkc_closure(std::function<auto (char32_t) -> std::optional<std::u32string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_general_category(std::function<auto (char32_t) -> std::optional<db::ch::GeneralCategory>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_grapheme_cluster_break(std::function<auto (char32_t) -> std::optional<db::ch::GraphemeClusterBreak>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_hangul_syllable_type(std::function<auto (char32_t) -> std::optional<db::ch::HangulSyllableType>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_indic_mantra_category(std::function<auto (char32_t) -> std::optional<db::ch::IndicMantraCategory>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_indic_positional_category(std::function<auto (char32_t) -> std::optional<db::ch::IndicPositionalCategory>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_indic_syllabic_category(std::function<auto (char32_t) -> std::optional<db::ch::IndicSyllabicCategory>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_alphabetic(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_ascii_hex(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_bidi_control(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_bidi_mirrored(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_case_ignorable(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_cased(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_changed_when_case_folded(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_changed_when_case_mapped(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_changed_when_lower_cased(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_changed_when_title_cased(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_changed_when_nfkc_case_folded(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_changed_when_upper_cased(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_composition_exclusion(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_dash(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_default_ignorable_code_point(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_deprecated(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_diacritic(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_emoji(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_emoji_component(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_emoji_modifier(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_emoji_modifier_base(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_emoji_presentation(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_expand_on_nfc(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_expand_on_nfd(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_expand_on_nfkc(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_expand_on_nfkd(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_extended_identifier_continue(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_extended_identifier_start(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_extended_pictograph(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_extender(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_full_composition_exclusion(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_grapheme_base(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_grapheme_extend(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_grapheme_link(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_hex(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_hyphen(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_identifier_continue(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_identifier_start(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_ideographic(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_ids_binary_operator(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_ids_trinary_operator(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_join_control(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_logical_order_exception(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_lower(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_math(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_non_character_code_point(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_other_alphabetic(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_other_default_ignorable_code_point(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_other_grapheme_extend(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_other_identifier_continue(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_other_identifier_start(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_other_lower(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_other_math(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_other_upper(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_pattern_syntax(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_pattern_whitespace(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_prepended_concatenation_mark(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_quotation_mark(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_radical(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_regional_indicator(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_sentence_terminal(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_soft_dotted(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_terminal_punctuation(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_unified_ideograph(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_upper(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_variation_selector(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_is_white_space(std::function<auto (char32_t) -> std::optional<bool>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_iso10646_comment(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_jamo_short_name(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_join_type(std::function<auto (char32_t) -> std::optional<db::ch::JoinType>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_joining_group(std::function<auto (char32_t) -> std::optional<db::ch::JoiningGroup>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_kAccountingNumeric(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kAlternateHanYu(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kAlternateJEF(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kAlternateKangXi(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kAlternateMorohashi(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kAlternateTotalStrokes(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kBigFive(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCCCII(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCNS1986(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCNS1992(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCangjie(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCantonese(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCheungBauer(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCheungBauerIndex(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCihaiT(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCompatibilityVariant(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kCowles(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kDaeJaweon(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kDefinition(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kEACC(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kFenn(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kFennIndex(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kFourCornerCode(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kFrequency(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kGB0(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kGB1(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kGB3(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kGB5(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kGB7(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kGB8(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kGSR(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kGradeLevel(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kHDZRadBreak(std::function<auto (char32_t) -> std::optional<std::u8string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kHKGlyph(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kHKSCS(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kHangul(std::function<auto (char32_t) -> std::optional<std::u8string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kHanyuPinlu(std::function<auto (char32_t) -> std::optional<std::u8string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kHanyuPinyin(std::function<auto (char32_t) -> std::optional<std::u8string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIBMJapan(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIICore(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRGDaeJaweon(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRGDaiKanwaZiten(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRGHanyuDaZidian(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRGKangXi(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_GSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_HSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_JSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_KPSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_KSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_MSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_SSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_TSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_UKSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_USource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kIRG_VSource(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kJHJ(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kJIS0213(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kJa(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kJapaneseKun(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kJapaneseOn(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kJinmeiyoKanji(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kJis0(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kJis1(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kJoyoKanji(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kKPS0(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kKPS1(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kKSC0(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kKangXi(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kKarlgren(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kKorean(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kKoreanEducationHanja(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kKoreanName(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kLau(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kMainlandTelegraph(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kMandarin(std::function<auto (char32_t) -> std::optional<std::u8string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kMatthews(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kMeyerWempe(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kMorohashi(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kNelson(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kOtherNumeric(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kPhonetic(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kPrimaryNumeric(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kPseudoGB1(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kRSAdobe_Japan1_6(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kRSJapanese(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kRSKanWa(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kRSKangXi(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kRSKorean(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kRSMerged(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kRSTUnicode(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kRSUnicode(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kReading(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kSBGY(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kSemanticVariant(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kSimplifiedVariant(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kSpecializedSemanticVariant(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kSpoofingVariant(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kSrc_NushuDuben(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kStrange(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kTGH(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kTGHZ2013(std::function<auto (char32_t) -> std::optional<std::u8string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kTGT_MergedSrc(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kTaiwanTelegraph(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kTang(std::function<auto (char32_t) -> std::optional<std::u8string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kTotalStrokes(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kTraditionalVariant(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kUnihanCore2020(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kVietnamese(std::function<auto (char32_t) -> std::optional<std::u8string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kXHC1983(std::function<auto (char32_t) -> std::optional<std::u8string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kXerox(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_kZVariant(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;  // NOLINT(readability-identifier-naming)
        [[maybe_unused]] auto override_legacy_name(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_line_break(std::function<auto (char32_t) -> std::optional<db::ch::LineBreak>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_lower_case(std::function<auto (char32_t) -> std::optional<std::u32string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_name(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_nfc_quick_check(std::function<auto (char32_t) -> std::optional<db::ch::NFC_QC>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_nfd_quick_check(std::function<auto (char32_t) -> std::optional<db::ch::NFD_QC>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_nfkc_case_fold(std::function<auto (char32_t) -> std::optional<std::u32string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_nfkc_quick_check(std::function<auto (char32_t) -> std::optional<db::ch::NFKC_QC>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_nfkd_quick_check(std::function<auto (char32_t) -> std::optional<db::ch::NFKD_QC>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_numeric_type(std::function<auto (char32_t) -> std::optional<db::ch::NumericType>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_numeric_value(std::function<auto (char32_t) -> std::optional<std::string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_script(std::function<auto (char32_t) -> std::optional<db::ch::Script>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_script_extensions(std::function<auto (char32_t) -> std::optional<std::vector<db::ch::Script>>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_sentence_break(std::function<auto (char32_t) -> std::optional<db::ch::SentenceBreak>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_simple_case_folding(std::function<auto (char32_t) -> std::optional<char32_t>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_simple_lower_case(std::function<auto (char32_t) -> std::optional<char32_t>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_simple_title_case(std::function<auto (char32_t) -> std::optional<char32_t>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_simple_upper_case(std::function<auto (char32_t) -> std::optional<char32_t>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_title_case(std::function<auto (char32_t) -> std::optional<std::u32string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_upper_case(std::function<auto (char32_t) -> std::optional<std::u32string>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_vertical_orientation(std::function<auto (char32_t) -> std::optional<db::ch::VerticalOrientation>>) -> CharacterLayer&;
        [[maybe_unused]] auto override_word_break(std::function<auto (char32_t) -> std::optional<db::ch::WordBreak>>) -> CharacterLayer&;

    private:
        std::shared_ptr<CharacterLayer> child = nullptr;

        std::map<std::string, std::function<auto (char32_t) -> std::optional<bool>>> flagOverrides;
        std::map<std::string, std::function<auto (char32_t) -> std::optional<char32_t>>> charOverrides;
        std::map<std::string, std::function<auto (char32_t) -> std::optional<std::string>>> strOverrides;
        std::map<std::string, std::function<auto (char32_t) -> std::optional<std::u32string>>> utf32Overrides;
        std::map<std::string, std::function<auto (char32_t) -> std::optional<std::u8string>>> utf8Overrides;

        std::optional<std::function<auto (char32_t) -> std::optional<std::map<db::ch::AliasType, std::vector<std::string>>>>> aliasesOverride;
        std::optional<std::function<auto (char16_t) -> std::optional<bool>>> isBomRev16BitOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::BidiClass>>> bidiClassOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::BidiPairedBracketType>>> bidiPairedBracketOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::GraphemeClusterBreak>>> graphemeClusterBreakOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::IndicMantraCategory>>> indicMantraCategoryOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::IndicPositionalCategory>>> indicPositionalCategoryOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::IndicSyllabicCategory>>> indicSyllabicCategoryOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::JoinType>>> joinTypeOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::JoiningGroup>>> joiningGroupOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::LineBreak>>> lineBreakOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::SentenceBreak>>> sentenceBreakOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::WordBreak>>> wordBreakOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::Block>>> blockOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::GeneralCategory>>> generalCategoryOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::EastAsianWidth>>> eastAsianWidthOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::HangulSyllableType>>> hangulSyllableTypeOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::DecompositionType>>> decompositionTypeOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::NFC_QC>>> nfcQcOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::NFD_QC>>> nfdQcOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::NFKC_QC>>> nfkcQcOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::NFKD_QC>>> nfkdQcOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::NumericType>>> numericTypeOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::Script>>> scriptOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<db::ch::VerticalOrientation>>> verticalOrientationOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<std::vector<db::ch::Script>>>> scriptExtensionsOverride;
        std::optional<std::function<auto (char32_t) -> std::optional<uint8_t>>> combiningPropertiesOverride;
    };
}
